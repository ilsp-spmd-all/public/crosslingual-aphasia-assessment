# Code to manipulate json files with transcript and feature data

from time import time
import tqdm

from utils import save_dict,load_dict, timestamp_to_ints

def create_speaker_subset(source_data_dict_path,speaker_list,target_dict_path):
    source=load_dict(source_data_dict_path)
    target={}
    not_in_source=[]
    for speaker in speaker_list:
        if speaker in source:
            target[speaker]=source[speaker]
        else:
            not_in_source.append(speaker)
    save_dict(target,target_dict_path)
    if len(not_in_source)>0:
        print("The following users were not in the source data_dict: ")
        print(not_in_source)

def print_lines_to_file(data_dict_path,line_name,target_file_path):
    source=load_dict(data_dict_path)
    lines=[]
    for speaker in source:
        for story in source[speaker]["stories"]:
            for utterance in source[speaker]["stories"][story]["utterances"]:
                if line_name in source[speaker]["stories"][story]["utterances"][utterance]:
                    lines.append(source[speaker]["stories"][story]["utterances"][utterance][line_name])
    with open(target_file_path, 'w') as f:
        for line in lines:
            f.write(f"{line}\n")



def copy_ASR_to_data_dict(source_data_dict_path,target_data_dict_path, result_data_dict_path):
    source=load_dict(source_data_dict_path)
    target=load_dict(target_data_dict_path)
    for speaker in tqdm.tqdm(source):
        for story in source[speaker]["stories"]:
            for utterance in source[speaker]["stories"][story]["utterances"]:
                try:
                    tmp_asr_transcript=source[speaker]["stories"][story]["utterances"][utterance]["asr_prediction"]
                except:
                    tmp_asr_transcript=f"NO_ASR for {speaker}"
                #now write the asr transcript to the target dict
                #maybe need an exception for when there is no such speaker?
                target[speaker]["stories"][story]["utterances"][utterance]["asr_prediction"]=tmp_asr_transcript
                

    save_dict(target,result_data_dict_path)

def add_ducle_transcript(data_dict_path,duc_le_folder_path,result_dict_path):
    # maybe split this function to one that appends duc_le ids and one for whole transcripts?
    data_dict=load_dict(data_dict_path)
    segments_path=duc_le_folder_path+"/segments.txt"
    transcripts_path=duc_le_folder_path+"/transcript.txt"
    speakers_missing=[]
    utterances_missing=[]
    with open(segments_path, "r") as f:
        segments=f.readlines()
    segments_dict={}
    for l in segments:
        l=l.rstrip()
        utterance,speaker,start,end=l.split(" ")
        if speaker in segments_dict:
            segments_dict[speaker][str(start)+","+str(end)]=utterance
        else:
            segments_dict[speaker]={}
            segments_dict[speaker][str(start)+","+str(end)]=utterance
    with open(transcripts_path, "r") as f:
        transcripts=f.readlines()
    transcripts_dict={}
    for l in transcripts:
        l=l.rstrip()
        utterance=l.split(" ")[0]
        transcript=" ".join(l.split(" ")[1:])
        transcripts_dict[utterance]=transcript
    for speaker in data_dict:
        speaker_name,speaker_group=speaker.split("-")
        if speaker_name not in segments_dict:
            speakers_missing.append(speaker)
            continue
        else:
            for story in data_dict[speaker]["stories"]:
                for utterance in data_dict[speaker]["stories"][story]["utterances"]:
                    timestamp=data_dict[speaker]["stories"][story]["utterances"][utterance]["timestamp"]
                    start,end=timestamp_to_ints(timestamp)
                    if str(start/1000)+","+str(end/1000) in segments_dict[speaker_name]:
                        duc_le_name=segments_dict[speaker_name][str(start/1000)+","+str(end/1000)]
                        data_dict[speaker]["stories"][story]["utterances"][utterance]["duc_le_name"]=segments_dict[speaker_name][str(start/1000)+","+str(end/1000)]
                        data_dict[speaker]["stories"][story]["utterances"][utterance]["duc_le_transcript"]=transcripts_dict[duc_le_name]
                    else:
                        print(f"utterance {utterance} not in duc le")

    save_dict(data_dict,result_dict_path)


def keep_duc_le_utterances(data_dict_path,new_dict_path):
    # doesnt work yet
    data_dict=load_dict(data_dict_path)
    
    # we use list(dict) because we modify the dictionary inside the loop
    for speaker in list(data_dict):
        for story in list(data_dict[speaker]["stories"]):
            for utterance in list(data_dict[speaker]["stories"][story]["utterances"]):
                if "duc_le_transcript" not in data_dict[speaker]["stories"][story]["utterances"][utterance]:
                    del data_dict[speaker]["stories"][story]["utterances"][utterance]
            if len(data_dict[speaker]["stories"][story]["utterances"])==0:
                del data_dict[speaker]["stories"][story]
        if len(data_dict[speaker]["stories"])==0:
            del data_dict[speaker]
            print(f"speaker {speaker} deleted!")
            
    save_dict(data_dict,new_dict_path)


if __name__=="__main__":
    source_data_dict_path="json_files/english_data_ducle_subset.json"
    target_dict_path="json_files/ACWT01a_data_ducle.json"
    speaker_list=["ACWT01a-Aphasia"]
    create_speaker_subset(source_data_dict_path,speaker_list,target_dict_path)

    # data_dict_path="testt.json"
    # print_lines_to_file(data_dict_path,"target_transcript","target_transcripts_ducle_subset.txt")

    # data_dict_path="json_files/english_data.json"
    # duc_le_folder_path="/home/user/temp/planv/ducle"
    # result_dict_path="test.json"
    # add_ducle_transcript(data_dict_path,duc_le_folder_path,result_dict_path)

    # data_dict_path="test.json"
    # new_dict_path="testt.json"
    # keep_duc_le_utterances(data_dict_path,new_dict_path)
