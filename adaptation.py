import os
import yaml
from utils import load_dict, save_dict
from sklearn.preprocessing import StandardScaler
from joblib import dump, load
from feature_extraction import DatasetFeatureExtractor, SpeakerFeatureExtractor
from dependency_parser import (
    GreekTEDxConlluParser,
    EnglishTEDxConlluParser,
    SpacyConlluParser,
)
from pprint import pprint
import numpy as np
import ot
import matplotlib.pylab as plt
import pandas as pd
from feature_names import FEATURES_NAMES

class TEDxGreekDataset(object):
    # Donwload from https://www.openslr.org/100
    # wget https://www.openslr.org/resources/100/mtedx_el.tgz
    # TEDx transcripts
    def __init__(
        self,
        stack_utterances_durations=True,
        flag_extract_conllu=False,
    ):
        self.path = "/home/jerrychatz/mtedx_el"
        self.info_train, self.transcripts_train, self.unique_wavs = self.fetch_data()
        self.conllu_extractor = GreekTEDxConlluParser()
        self.feature_extractor = StoryLevelFeatureExtractor(
            language="greek",
            stack_utterances_durations=stack_utterances_durations,
        )
        if flag_extract_conllu:
            self.data_dict = self.conllu_extractor.calculate_conllu(
                self.create_custom_data_dict()
            )
            save_dict(self.data_dict, "tedx-greek.json")
        else:
            self.data_dict = load_dict("tedx-greek.json")

    def fetch_data(self):
        train_path = os.path.join(self.path, "data", "train", "txt")
        with open(os.path.join(train_path, "train.yaml"), "r") as stream:
            try:
                info_train = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print("YAMLError", exc)
        unique_wavs = []
        for item in info_train:
            if item["wav"] not in unique_wavs:
                unique_wavs.append(item["wav"])
        with open(os.path.join(train_path, "train.el"), "r") as f:
            transcripts_train = f.readlines()
        transcripts_train = [
            transcript.rstrip("\n") for transcript in transcripts_train
        ]
        return info_train, transcripts_train, unique_wavs

    def create_custom_data_dict(self):
        data_dict = {}
        for idx, info in enumerate(self.info_train):
            # initialization
            if info["wav"] not in data_dict:
                data_dict[info["wav"]] = {"raw_transcriptions": {}}
            transcription = self.transcripts_train[idx]
            duration = info["duration"]
            data_dict[info["wav"]]["raw_transcriptions"][transcription] = {
                "duration": duration
            }
        return data_dict

    def get_features(self):
        X = []
        for wav in self.data_dict.keys():
            features, _ = self.feature_extractor.feature_extraction_story_level(
                self.data_dict[wav]["raw_transcriptions"]
            )
            X.append(features)
        return np.array(X, dtype=float)

    def get_scaler(self):
        sc = StandardScaler()
        X = self.get_features()
        print(X.shape)
        sc.fit_transform(X)
        return sc


class TEDxMultilingualDatasetEnEl(object):
    def __init__(self, stack_utterances_durations=True, load_data_csv=True):
        self.path = "/home/plitsis/planv/datasets/tedx/el-en/data"
        languages = ["el", "en"]
        self.conllu_extractor_el = GreekTEDxConlluParser()
        self.conllu_extractor_en = EnglishTEDxConlluParser()
        self.stack_utterances_durations = stack_utterances_durations
        if load_data_csv:
            self.df = pd.read_csv(self.path + "/all_data.csv")
        else:
            (
                self.transcripts_el,
                self.transcripts_en,
                self.info,
            ) = self.fetch_data(splits=["train", "valid", "test"])
            self.df = self.create_df(save=True)
            self.df = self.df_add_conllu(self.df, save=True)

        self.df_feats_en, self.df_feats_el = self.create_df_feats_by_n_utterances(self.df,25)
        

    def get_features(self):
        X_el, X_en = [], []
        for wav in self.data_dict_el.keys():
            (
                features_el,
                _,
            ) = self.feature_extractor_greek.feature_extraction_story_level(
                self.data_dict_el[wav]["raw_transcriptions"]
            )
            (
                features_en,
                _,
            ) = self.feature_extractor_english.feature_extraction_story_level(
                self.data_dict_en[wav]["raw_transcriptions"]
            )
            X_el.append(features_el)
            X_en.append(features_en)
        return np.array(X_el, dtype=float), np.array(X_en, dtype=float)

    def create_df(self, save=False):
        df = pd.DataFrame().from_dict(self.info)
        df["transcripts_en"] = self.transcripts_en
        df["transcripts_el"] = self.transcripts_el
        if save:
            df.to_csv(self.path + "/all_data.csv", index=False)
        return df

    def df_add_conllu(self, df, save=False):
        spacy_model_name_el = "el_core_news_lg"
        spacy_model_name_en = "en_core_web_trf"
        spacy_parser_en = SpacyConlluParser(spacy_model_name=spacy_model_name_el)
        spacy_parser_el = SpacyConlluParser(spacy_model_name=spacy_model_name_en)
        df["transcripts_en_conllu"] = spacy_parser_en.calculate_conllus_from_list(
            list(df["transcripts_en"])
        )
        df["transcripts_el_conllu"] = spacy_parser_en.calculate_conllus_from_list(
            list(df["transcripts_el"])
        )
        if save:
            df.to_csv(self.path + "/all_data.csv", index=False)
        return df
        # self.df.to_csv(csv_path,index=False)

    def create_df_feats(self, df):
        df = df.dropna()
        fe_en = SpeakerFeatureExtractor(
            "en", "transcripts_en", "transcripts_en_conllu", "duration"
        )
        fe_el = SpeakerFeatureExtractor(
            "el", "transcripts_el", "transcripts_el_conllu", "duration"
        )
        df_feats_en=pd.DataFrame()
        df_feats_el=pd.DataFrame()

        for speaker in df["speaker_id"].unique():
            dff = df[df["speaker_id"] == speaker]
            feats_en, helpers_en = fe_en.feature_extraction_from_df(dff)
            feats_el, helpers_el = fe_el.feature_extraction_from_df(dff)

            feats_en = {k: [v] for k, v in zip(feats_en.keys(), feats_en.values())}
            feats_en = pd.DataFrame.from_dict(feats_en)
            df_feats_en=pd.concat([df_feats_en,feats_en])
            feats_el = {k: [v] for k, v in zip(feats_el.keys(), feats_el.values())}
            feats_el = pd.DataFrame.from_dict(feats_el)
            df_feats_el=pd.concat([df_feats_el,feats_el])
        return df_feats_en, df_feats_el

    def create_df_feats_by_n_utterances(self, df,no_utterances):
        df = df.dropna()
        fe_en = SpeakerFeatureExtractor(
            "en", "transcripts_en", "transcripts_en_conllu", "duration"
        )
        fe_el = SpeakerFeatureExtractor(
            "el", "transcripts_el", "transcripts_el_conllu", "duration"
        )
        df_feats_en=pd.DataFrame()
        df_feats_el=pd.DataFrame()
        def split_dataframe(df, chunk_size = 25,reject_smaller=True): 
            chunks = list()
            num_chunks = len(df) // chunk_size + 1
            for i in range(num_chunks):
                if len(df[i*chunk_size:(i+1)*chunk_size])==chunk_size:
                    chunks.append(df[i*chunk_size:(i+1)*chunk_size])
            return chunks

        for speaker in df["speaker_id"].unique():
            
            
            dff = df[df["speaker_id"] == speaker]
            chunks=split_dataframe(dff,25)
            for chunk in chunks:
                feats_en, helpers_en = fe_en.feature_extraction_from_df(chunk)
                feats_el, helpers_el = fe_el.feature_extraction_from_df(chunk)

                feats_en = {k: [v] for k, v in zip(feats_en.keys(), feats_en.values())}
                feats_en = pd.DataFrame.from_dict(feats_en)
                df_feats_en=pd.concat([df_feats_en,feats_en])
                feats_el = {k: [v] for k, v in zip(feats_el.keys(), feats_el.values())}
                feats_el = pd.DataFrame.from_dict(feats_el)
                df_feats_el=pd.concat([df_feats_el,feats_el])
        return df_feats_en, df_feats_el

    def create_custom_data_dict(self):
        data_dict_el = {}
        data_dict_en = {}
        for idx, info in enumerate(self.info):
            # initialization
            if info["wav"] not in data_dict_el:
                data_dict_el[info["wav"]] = {"raw_transcriptions": {}}
                data_dict_en[info["wav"]] = {"raw_transcriptions": {}}
            transcription_el = self.transcripts_el[idx]
            transcription_en = self.transcripts_en[idx]
            duration = info["duration"]
            data_dict_el[info["wav"]]["raw_transcriptions"][transcription_el] = {
                "duration": duration
            }
            data_dict_en[info["wav"]]["raw_transcriptions"][transcription_en] = {
                "duration": duration
            }
        return data_dict_el, data_dict_en

    def fetch_data(self, splits=["train", "valid", "test"]):
        transcripts_el = []
        transcripts_en = []
        info = []
        for split in splits:
            (
                transcripts_el_split,
                transcripts_en_split,
                info_split,
            ) = self.fetch_data_split(split=split)
            transcripts_el += transcripts_el_split
            transcripts_en += transcripts_en_split
            info += info_split
        return transcripts_el, transcripts_en, info

    def fetch_data_split(self, split="test"):
        datapath = os.path.join(self.path, split, "txt")
        with open(os.path.join(datapath, f"{split}.yaml"), "r") as stream:
            try:
                info = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print("YAMLError", exc)

            with open(os.path.join(datapath, f"{split}.el"), "r") as f:
                transcripts_greek = f.readlines()
            with open(os.path.join(datapath, f"{split}.en"), "r") as f:
                transcripts_english = f.readlines()
        transcripts_greek = [
            transcript.rstrip("\n") for transcript in transcripts_greek
        ]
        transcripts_english = [
            transcript.rstrip("\n") for transcript in transcripts_english
        ]
        return transcripts_greek, transcripts_english, info


class OptimalAdaptation(object):
    # Domain Adaptation Examples > Linear OT mapping estimation
    def __init__(self, algorithm="gaussian"):
        self.algorithm = algorithm
        if algorithm == "linear":
            self.ot = ot.da.OT_mapping_linear
        elif algorithm == "emd":
            self.ot = ot.da.EMDTransport()
        elif algorithm == "sinkhorn":
            self.ot = ot.da.SinkhornTransport(reg_e=1e-1)
        elif algorithm == "gaussian":
            self.ot = ot.da.MappingTransport(
                kernel="gaussian",
                eta=1e-5,
                mu=1e-1,
                bias=True,
                sigma=1,
                max_iter=20,
                verbose=True,
            )
        self.Ae, self.be = None, None

    def fit_and_transform(self, X_el, X_en):
        if self.algorithm == "linear":
            self.Ae, self.be = self.ot(X_el, X_en)
            X_el_transformed = X_el.dot(self.Ae) + self.be
        else:
            self.ot.fit(Xs=X_el, Xt=X_en)
            X_el_transformed = self.ot.transform(Xs=X_el)
        return X_el_transformed

    def transform(self, X_el):
        if self.algorithm == "linear":
            X_el_transformed = X_el.dot(self.Ae) + self.be
        else:
            X_el_transformed = self.ot.transform(Xs=X_el)
        return X_el_transformed


if __name__ == "__main__":
    # import numpy as np
    # import matplotlib.pylab as plt
    # import ot
    # import ot.plot
    # from ot.datasets import make_1D_gauss as gauss

    # n_source_samples = 150
    # n_target_samples = 150

    # Xs, ys = ot.datasets.make_data_classif("3gauss", n_source_samples)
    # Xt, yt = ot.datasets.make_data_classif("3gauss2", n_target_samples)

    # print(Xs, ys)

    data = TEDxMultilingualDatasetEnEl()
    # X_el, X_en = data.get_features()
    X_el = data.df_feats_el.to_numpy()
    X_en = data.df_feats_en.to_numpy()
    optimal_transport = OptimalAdaptation()

    X_el_transformed = optimal_transport.fit_and_transform(X_el, X_en)
    

    utterance_column_name="clean_transcription_v1"
    csv_english="/home/plitsis/planv/datasets/ab_english/all_data.csv"
    df_en=pd.read_csv(csv_english)

    fe_en = DatasetFeatureExtractor(
        df_en,
        language="en",
        utterance_column=utterance_column_name,
        conllu_column=utterance_column_name+"_conllu",
        extract_conllu=False,
    )
    df_feats_en = fe_en.calculate_features_all_speakers()
    X_en_ab=df_feats_en[FEATURES_NAMES].to_numpy()
    csv_greek="/home/plitsis/planv/datasets/ab_english/all_data.csv"
    df_el=pd.read_csv(csv_greek)

    fe_el = DatasetFeatureExtractor(
        df_el,
        language="el",
        utterance_column=utterance_column_name,
        conllu_column=utterance_column_name+"_conllu",
        extract_conllu=False,
    )
    df_feats_el = fe_el.calculate_features_all_speakers()
    X_el_planv=df_feats_el[FEATURES_NAMES].to_numpy()

    X_el_transformed = optimal_transport.transform(X_el_planv)

    def give_cdf(x):
        x=np.sort(x)
        y=np.arange(len(x))/float(len(x))
        return x,y

    feat_to_plot=2
    x_el,y_el = give_cdf(X_el[:,feat_to_plot])
    x_en,y_en = give_cdf(X_en[:,feat_to_plot])
    x_el_t,y_el_t=give_cdf(X_el_transformed[:,feat_to_plot])
    x_en_ab,y_en_ab = give_cdf(X_en_ab[:,feat_to_plot])
    
    plt.figure()
    plt.plot(x_el,y_el, "b", label="Source distribution")
    plt.plot(x_en,y_en, "r", label="Target distribution")
    plt.legend()
    plt.savefig("el-en-train-ot.png")

    plt.figure()
    plt.plot(x_el_t,y_el_t, "b", label="Source distribution")
    plt.plot(x_en_ab,y_en_ab, "r", label="Target distribution")
    plt.legend()
    plt.savefig("el-en-after.png")

    plt.figure()
    plt.plot(x_en,y_en, "b", label="English TEDX")
    plt.plot(x_en_ab,y_en_ab, "r", label="English AB")
    plt.legend()
    plt.savefig("el-en-compare_eng.png")

    plt.figure()
    plt.plot(x_el,y_el, "b", label="Greek TEDX")
    plt.plot(x_el_t,y_el_t, "r", label="Greek AB")
    plt.legend()
    plt.savefig("el-en-compare_el.png")