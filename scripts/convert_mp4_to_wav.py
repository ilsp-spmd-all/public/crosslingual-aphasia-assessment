''' 
    data_conversion.py
        Convert mp4 files of AphasiaBank dataset to wav files         
'''

import os 
import argparse 

# DATA_MP4_DIR = '/data/projects/planv/media.talkbank.org/aphasia/English/Aphasia/'
# DATA_WAV_DIR = '/data/projects/planv/aphasic-dataset'

def convert_mp4_dir_to_wav(mp4_dir_path,wav_dir_path, sample_rate=16000, channels=1):
    for folder in os.listdir(args.DATA_MP4_DIR):
        
        mp4_folder_path = os.path.join(args.DATA_MP4_DIR, folder)
        wav_folder_path = os.path.join(args.DATA_WAV_DIR, folder)

        # Create directory if inexistent 
        if not os.path.isdir(wav_folder_path):
            print(f'wav folder for {folder} is created')
            os.makedirs(wav_folder_path, exist_ok=True)
        
        for mp4_file in os.listdir(mp4_folder_path):
            filename = mp4_file.split('.')[0] # keep filename from mp4_file = filename.mp4
            mp4_file_path = os.path.join(mp4_folder_path, mp4_file)
            wav_file_path = os.path.join(wav_folder_path, filename+'.wav')

            os.system(f'ffmpeg -i {mp4_file_path} -vn -acodec pcm_s16le -ar {sample_rate} -ac {channels} {wav_file_path}')
            print(f'File {filename} is converted successfully to wav')


if __name__ == '__main__':

    # Parse arguments
    parser = argparse.ArgumentParser(description='Data Conversion ParserL from mp4 to wav')
    parser.add_argument('--DATA_MP4_DIR', default='/data/projects/planv/media.talkbank.org/aphasia/English/Aphasia/')
    parser.add_argument('--DATA_WAV_DIR', default='/data/projects/planv/aphasic-dataset')
    args = parser.parse_args()

    convert_mp4_dir_to_wav(args.DATA_MP4_DIR, args.DATA_WAV_DIR)