import os
from time import time
import librosa
import sys
import re
from pydub import AudioSegment
# print(os.getcwd())
# sys.path.append("..")
# from utils import fetch_transcriptions_from_elan_file


def merge_chat_files_jerry(folder="/data/projects/planv/jerry/elanchat", durations=[0, 0]):
    chat_filenames = [
        os.path.join(folder, fn) for fn in os.listdir(folder) if fn.endswith(".cha")
    ]
    wav_dict = {
        "stroke": "/data/projects/planv/jerry/dataset_planv/010/010_stroke_1619173048977.wav",
        "cat": "/data/projects/planv/jerry/dataset_planv/010/010_cat_1619173048977.wav",
        "umbrella":"/data/projects/planv/jerry/dataset_planv/010/010_umbrella_1619173048977.wav",
        "cinderella":"/data/projects/planv/jerry/dataset_planv/010/010_cinderella_1619173048977.wav",
    }
    total_duration = 0
    with open("output.cha", "w") as f_out:
        for idx, chat_filename in enumerate(chat_filenames):
            story_name = chat_filename.split("/")[-1].split(".")[0].split("_")[-1]
            passed_intro_info = False
            previous_line_first_character = ""
            with open(chat_filename, "r") as f_in:
                lines = f_in.readlines()
            for line in lines:
                if line.startswith("@"):
                    if idx == 0 and not passed_intro_info:
                        f_out.write(line)
                    if idx == len(chat_filenames) - 1 and passed_intro_info:
                        f_out.write(line)
                else:
                    if previous_line_first_character == "@":
                        passed_intro_info = True
                        f_out.write(f"@G: {story_name.capitalize()}\n")

                    if idx > 0:
                        time_segment = re.findall("\(.*?)\", line)[0]
                        # re.sub('<.*?>', '', string)
                        t = time_segment.split('"')
                        timestamps = t[2].split("_")[1::]
                        new_segment = f'{t[0]}"{t[1]}"_{int(timestamps[0])+total_duration}_{int(timestamps[1])+total_duration}'
                        line_no_time=str.replace(line,time_segment,"")
                        line_no_time=line_no_time.rstrip()
                        print("lnt:",line_no_time)
                        if line_no_time.endswith(";"):
                            line_no_time[-1]="?"
                        line=line_no_time+" "+time_segment
                        # line = str.replace(line, time_segment, new_segment)
                    f_out.write(line)
                previous_line_first_character = line[0]
            duration = librosa.get_duration(filename=wav_dict[story_name])
            total_duration += int(duration * 1000)

def merge_chat_files(folder,stories,wav_dict,output_file,new_wav_name,replace_greek_question_marks=True):
    chat_filenames = [
        os.path.join(folder, fn) for fn in os.listdir(folder) if fn.endswith(".cha")
    ]
    print(chat_filenames)
    print(stories)
    
    

    total_duration = 0
    with open(output_file, "w") as f_out:
        idx=0
        stories_used=[]
        for story_name in stories:
            for chat_filename in chat_filenames:
                if story_name in chat_filename:
                    # chat_fp=os.path.basename(chat_filename)
                    passed_intro_info = False
                    previous_line_first_character = ""
                    with open(chat_filename, "r") as f_in:
                        lines = f_in.readlines()
                    for line in lines:
                        if line.startswith("@"):
                            if idx == 0 and not passed_intro_info:
                                f_out.write(line)
                            if idx == len(chat_filenames) - 1 and passed_intro_info:
                                f_out.write(line)
                        else:
                            if previous_line_first_character == "@":
                                passed_intro_info = True
                                f_out.write(f"@G: {story_name.capitalize()}\n")
                            if idx >= 0:
                                time_segment = re.findall("\(.*?)\", line)[0]
                                
                                print(time_segment)
                                # re.sub('<.*?>', '', string)
                                t = time_segment.split('"')
                                print(t)
                                timestamps = t[2].split("_")[1::]
                                print(timestamps)
                                # new_segment = f'{t[0]}"{t[1]}"_{int(timestamps[0])+total_duration}_{int(timestamps[1])+total_duration}'
                                new_segment=f'{int(timestamps[0])+total_duration}_{int(timestamps[1])+total_duration}'
                                print(new_segment)
                                if replace_greek_question_marks:
                                    time_ind=line.find("")
                                    line_no_time=line[:time_ind]

                                    line_no_time=line_no_time.rstrip()
                                    print("lnt:",line_no_time)
                                    if line_no_time.endswith(";"):
                                        line_no_time=line_no_time[:-1]+ "?"
                                    line=line_no_time+" "+line[time_ind:]
                                line = str.replace(line, time_segment, new_segment)
                                print(line)
                            f_out.write(line)
                        previous_line_first_character = line[0]
                    duration = librosa.get_duration(filename=wav_dict[story_name])
                    total_duration += int(duration * 1000)
                    idx+=1
                    stories_used.append(story_name)
                    continue
    sounds=[]
    for story in stories_used:
        sounds.append(AudioSegment.from_wav(wav_dict[story]))
    new_wav=sum(sounds)
    new_wav.export(os.path.dirname(output_file)+f"/{new_wav_name}.wav", format="wav")


        
if __name__ == "__main__":
    # chat_folder="datasets/planv-aphasia/chat/023"
    # wav_dict = {
    #     "stroke": "datasets/planv-aphasia/all_files/audio/aphasia/023/023_stroke_1637151076961.wav",
    #     "cat": "datasets/planv-aphasia/all_files/audio/aphasia/023/023_cat_1637151076961.wav",
    #     "umbrella":"datasets/planv-aphasia/all_files/audio/aphasia/023/023_umbrella_1637151076961.wav",
    #     "cinderella":"datasets/planv-aphasia/all_files/audio/aphasia/023/023_cinderella_1637151076961.wav",
    # }
    chat_folder="datasets/planv-aphasia/split_chat_files/505"
    wav_dict = {
        "stroke": "datasets/planv-aphasia/split_chat_files/audio_files_for_teams/505/aud/505_stroke_1641202383951 (1)_aud.wav",
        "cat": "datasets/planv-aphasia/split_chat_files/audio_files_for_teams/505/aud/505_cat_1641202383951 (1) aud.wav",
        "umbrella":"datasets/planv-aphasia/split_chat_files/audio_files_for_teams/505/aud/505_umbrella_1641202383951 (1)_aud.wav",
        "cinderella":"datasets/planv-aphasia/split_chat_files/audio_files_for_teams/505/aud/505_cinderella_1641202383951 (1)_aud.wav",
    }
    stories=["stroke","cat","umbrella","cinderella"]
    new_wav_name="PLANV505a"
    output_file=chat_folder+f"/{new_wav_name}.cha"

    merge_chat_files(chat_folder,stories,wav_dict,output_file,new_wav_name=new_wav_name)
    