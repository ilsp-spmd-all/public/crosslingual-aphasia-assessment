from pyannote.audio import Pipeline

def chat_to_rttm(chat_file_path,rttm_path):
    with open(chat_file_path, "r") as f:
        lines=f.readlines()



pipeline = Pipeline.from_pretrained("pyannote/speaker-diarization")

audio_path="datasets/ab_english/all_files/audio/Aphasia/ACWT/ACWT01a.wav"

# DEMO_FILE = {'uri': 'blabal', 'audio': 'audio.wav'}
dz = pipeline(audio_path, num_speakers=2)  

with open("diarization.txt", "w") as text_file:
    text_file.write(str(dz))