from asyncore import read
from email.mime import audio
import os

import re
import pandas as pd

# import argparse
# import pandas as pd
# import numpy as np
# from sklearn import preprocessing
import torch

# import torchaudio
# from torch.utils.data import Dataset
# from utils import filename2folder, inst2foldername_dict
# import json
import string
import tqdm
from pydub import AudioSegment
# import pyannote
import glob
from utils import (
    save_dict,
    load_dict,
    fetch_transcriptions_from_elan_file,
    reformat_sentence,
    timestamp_to_ints,
    timestamp_to_duration_conversion,
    cut_audio_file,
    resample_audio,
)
from dataclasses import dataclass, field
from typing import Any, Dict, List, Optional, Union
from transformers import Wav2Vec2Processor

from transcript_cleaning import all_cleaning, clean_f_l, normalize_whisper,clean_greek
from run_asr import run_whisper_csv
from feature_extraction import DatasetFeatureExtractor


@dataclass
class DataCollatorCTCWithPadding:
    """
    Data collator that will dynamically pad the inputs received.
    Args:
        processor (:class:`~transformers.Wav2Vec2Processor`)
            The processor used for proccessing the data.
        padding (:obj:`bool`, :obj:`str` or :class:`~transformers.tokenization_utils_base.PaddingStrategy`, `optional`, defaults to :obj:`True`):
            Select a strategy to pad the returned sequences (according to the model's padding side and padding index)
            among:
            * :obj:`True` or :obj:`'longest'`: Pad to the longest sequence in the batch (or no padding if only a single
              sequence if provided).
            * :obj:`'max_length'`: Pad to a maximum length specified with the argument :obj:`max_length` or to the
              maximum acceptable input length for the model if that argument is not provided.
            * :obj:`False` or :obj:`'do_not_pad'` (default): No padding (i.e., can output a batch with sequences of
              different lengths).
        max_length (:obj:`int`, `optional`):
            Maximum length of the ``input_values`` of the returned list and optionally padding length (see above).
        max_length_labels (:obj:`int`, `optional`):
            Maximum length of the ``labels`` returned list and optionally padding length (see above).
        pad_to_multiple_of (:obj:`int`, `optional`):
            If set will pad the sequence to a multiple of the provided value.
            This is especially useful to enable the use of Tensor Cores on NVIDIA hardware with compute capability >=
            7.5 (Volta).
    """

    processor: Wav2Vec2Processor
    padding: Union[bool, str] = True
    max_length: Optional[int] = None
    max_length_labels: Optional[int] = None
    pad_to_multiple_of: Optional[int] = None
    pad_to_multiple_of_labels: Optional[int] = None

    def __call__(
        self, features: List[Dict[str, Union[List[int], torch.Tensor]]]
    ) -> Dict[str, torch.Tensor]:
        # split inputs and labels since they have to be of different lenghts and need
        # different padding methods
        input_features = [
            {"input_values": feature["input_values"]} for feature in features
        ]
        label_features = [{"input_ids": feature["labels"]} for feature in features]

        batch = self.processor.pad(
            input_features,
            padding=self.padding,
            max_length=self.max_length,
            pad_to_multiple_of=self.pad_to_multiple_of,
            return_tensors="pt",
        )
        with self.processor.as_target_processor():
            labels_batch = self.processor.pad(
                label_features,
                padding=self.padding,
                max_length=self.max_length_labels,
                pad_to_multiple_of=self.pad_to_multiple_of_labels,
                return_tensors="pt",
            )

        # replace padding with -100 to ignore loss correctly
        labels = labels_batch["input_ids"].masked_fill(
            labels_batch.attention_mask.ne(1), -100
        )

        batch["labels"] = labels

        return batch


# Greek Dataset Functions


def create_planv_data(eaf_dir, group, save_dict_name="planv_data.json", audio_folder=""):
    eaf_files = glob.glob(f"{eaf_dir}/**/*.eaf", recursive=True)
    speaker_ids = []
    wav_paths = []
    data_dict = {}
    if audio_folder:
        audio_root = audio_folder
    else:
        audio_root = eaf_dir

    for eaf in eaf_files:
        tmp = eaf.split("/")[-1].split("_")
        speaker_id = tmp[0]
        story = tmp[1].split(".")[0]
        speaker_ids.append(speaker_id)
        if speaker_id not in data_dict.keys():
            data_dict[speaker_id] = {"user_info": {"group": group}, "stories": {}}
        wav_files = [
            f
            for f in os.listdir(os.path.join(audio_root, speaker_id))
            if (
                (speaker_id in f)
                and (story in f)
                # and (not ("_aud" in f))
                and (f.endswith(".wav"))
            )
        ]
        if len(wav_files) == 0:
            print(speaker_id, story)
        wav_file = os.path.join(audio_root, speaker_id, wav_files[0])
        wav_paths.append(wav_file)
        data_dict[speaker_id]["stories"][story] = {
            "story_info": {"transcription_path": eaf, "audio_path": wav_file},
            "utterances": {},
        }
    save_dict(data_dict, save_dict_name)


def get_planv_oracle_transcriptions(
    data_dict,
    save_dict_name="planv_data.json",
    tier='TIER_ID="*PAR"',
):
    for speaker in data_dict:
        for story in data_dict[speaker]["stories"]:
            eaf_fp = data_dict[speaker]["stories"][story]["story_info"][
                "transcription_path"
            ]
            (
                utterances,
                t_start,
                t_end,
                durations,
            ) = fetch_transcriptions_from_elan_file(eaf_fp, tier)
            if len(t_start) == 0:
                print(speaker, story)
            for idx, utterance in enumerate(utterances):
                data_dict[speaker]["stories"][story]["utterances"][
                    f"{speaker}-{story}-{idx}"
                ] = {
                    "raw_transcript": utterance,
                    "timestamp": f"{t_start[idx]}_{t_end[idx]}",
                    "duration": durations[idx],
                }
    save_dict(data_dict, save_dict_name)


def resample_move_audio_files(
    data_dict_path, target_rate, save_folder, save_new_path_to_dict=True
):
    data_dict = load_dict(data_dict_path)
    for speaker in tqdm.tqdm(data_dict):
        for story in data_dict[speaker]["stories"]:
            wav_fp = data_dict[speaker]["stories"][story]["story_info"]["audio_path"]
            if not wav_fp:
                continue
            else:
                group = data_dict[speaker]["user_info"]["group"]
                save_path = (
                    save_folder
                    + "/"
                    + group
                    + "/"
                    + speaker
                    + "/"
                    + wav_fp.split("/")[-1][:-4]
                    + ".wav"
                )
                if save_new_path_to_dict:
                    data_dict[speaker]["stories"][story]["story_info"][
                        "audio_path"
                    ] = save_path
                resample_audio(wav_fp, target_rate, save_path)

    save_dict(data_dict, data_dict_path)


def split_audio_files_stories(data_dict_path, save_folder):
    # for each utterance, create an audio file with the audio
    # the title should be <uttterance_name>_start_duration.wav
    data_dict = load_dict(data_dict_path)
    for speaker in tqdm.tqdm(data_dict):
        for story in data_dict[speaker]["stories"]:
            wav_fp = data_dict[speaker]["stories"][story]["story_info"]["audio_path"]
            if not wav_fp:
                continue
            for idx, utterance_name in enumerate(
                data_dict[speaker]["stories"][story]["utterances"]
            ):
                ts = data_dict[speaker]["stories"][story]["utterances"][utterance_name][
                    "timestamp"
                ]
                start, end = timestamp_to_ints(ts)
                duration = timestamp_to_duration_conversion(ts)
                cut_file_path = (
                    save_folder
                    + "/"
                    + utterance_name
                    + "_"
                    + str(start / 1000)
                    + "_"
                    + str(duration / 1000)
                    + ".wav"
                )
                if not os.path.exists(cut_file_path):
                    cut_audio_file(wav_fp, start, end, cut_file_path)
                data_dict[speaker]["stories"][story]["utterances"][utterance_name][
                    "audio_segment_path"
                ] = cut_file_path
    save_dict(data_dict, data_dict_path)


def create_segments_greek(datapath, save_dict_name):
    from pyannote.audio.pipelines import VoiceActivityDetection

    pipeline = VoiceActivityDetection(segmentation="pyannote/segmentation")
    HYPER_PARAMETERS = {
        # onset/offset activation thresholds
        "onset": 0.5,
        "offset": 0.5,
        # remove speech regions shorter than that many seconds.
        "min_duration_on": 0.0,
        # fill non-speech regions shorter than that many seconds.
        "min_duration_off": 0.0,
    }
    pipeline.instantiate(HYPER_PARAMETERS)
    new_dict = {}
    for speaker in os.listdir(datapath):
        new_dict[speaker] = {"user_info": {"group": "aphasia", "aq_score": None}}
        for audio_fn in os.listdir(os.path.join(datapath, speaker)):
            full_audio_fn = os.path.join(datapath, speaker, audio_fn)
            story_name = audio_fn.split("_")[1]
            new_dict[speaker][story_name] = {
                "story_info": {"audio_path": full_audio_fn},
                "utterances": {},
            }

            vad = pipeline(full_audio_fn)
            for idx, (turn, speaker_id, type_sound) in enumerate(
                vad.itertracks(yield_label=True)
            ):
                new_dict[speaker][story_name]["utterances"][f"{speaker}-{idx}"] = {
                    "timestamp": f"{int(turn.start * 1000)}_{int(turn.end * 1000)}",
                    "duration": int(turn.end * 1000) - int(turn.start * 1000),
                }
    save_dict(new_dict, save_dict_name)


def create_thales_data_dict(
    data_folder,
    save_dict_name,
    audio_folder="",
    tier='TIER_ID="Processed Transcription"',
):
    groups = os.listdir(data_folder)
    data_dict = {}
    eaf_filepaths = []
    t_start, t_end, durations = [], [], []
    for group in groups:
        group_path = os.path.join(data_folder, group)
        group_speakers = os.listdir(group_path)
        for speaker in group_speakers:
            data_dict[speaker] = {"user_info": {"group": group}, "stories": {}}
            speaker_path = os.path.join(group_path, speaker)
            speaker_stories = os.listdir(speaker_path)
            for speaker_story in speaker_stories:
                story_path = os.path.join(speaker_path, speaker_story)
                eaf_file = glob.glob(f"{story_path}/*.eaf", recursive=True)[0]

                wav_file = [
                    f for f in glob.glob(f"{story_path}/*") if not f.endswith("eaf")
                ]

                if wav_file:
                    wav_file = wav_file[0]
                    wav_name = wav_file.split("/")[-1][:-4]
                    if audio_folder:
                        wav_file = f"{audio_folder}/{group}/{speaker}/{wav_name}.wav"
                else:
                    wav_file = None

                data_dict[speaker]["stories"][speaker_story] = {
                    "story_info": {
                        "transcription_path": eaf_file,
                        "audio_path": wav_file,
                    },
                    "utterances": {},
                }

                (
                    transcriptions_list,
                    t_start,
                    t_end,
                    durations,
                ) = fetch_transcriptions_from_elan_file(eaf_file, tier)
                transcriptions_list = reformat_sentence(transcriptions_list)
                eaf_filepaths.append(eaf_file)
                for idx, transcription in enumerate(transcriptions_list):

                    data_dict[speaker]["stories"][speaker_story]["utterances"][
                        f"{speaker}-{speaker_story}-{idx}"
                    ] = {
                        "raw_transcript": transcription,
                        "timestamp": f"{t_start[idx]}_{t_end[idx]}",
                        "duration": durations[idx],
                        "conllu": "",
                    }

    save_dict(data_dict, save_dict_name)
    return data_dict, eaf_filepaths, list(data_dict.keys())


def fix_greek_helper_lowercase():
    data_dict = load_dict("www_greek_data_asr.json")
    for speaker in data_dict.keys():
        for story in data_dict[speaker]["stories"]:
            for utt_id in data_dict[speaker]["stories"][story]["utterances"].keys():
                if (
                    "asr_prediction"
                    not in data_dict[speaker]["stories"][story]["utterances"][
                        utt_id
                    ].keys()
                ):
                    continue
                data_dict[speaker]["stories"][story]["utterances"][utt_id][
                    "asr_prediction"
                ] = data_dict[speaker]["stories"][story]["utterances"][utt_id][
                    "asr_prediction"
                ].lower()
        save_dict(data_dict, "www_greek_data_asr.json")


def merge_greek_dicts(dict_1, dict_2, save_dict_name):
    dict_1.update(dict_2)
    print(len(dict_1))
    save_dict(dict_1, save_dict_name)


def calculate_conllus_ilsp(
    data_dict,
    key_target="processed_transcript",
    conllu_name="conllu",
    save_dict_name="planv_data.json",
):
    import requests

    for speaker in data_dict:
        for story in data_dict[speaker]["stories"]:
            utterances, utt_names = [], []

            for utt_name in data_dict[speaker]["stories"][story]["utterances"]:
                if (
                    key_target
                    not in data_dict[speaker]["stories"][story]["utterances"][utt_name]
                ):
                    continue
                t = data_dict[speaker]["stories"][story]["utterances"][utt_name][
                    key_target
                ]
                if t == "":
                    continue

                def preprocess_text(t):
                    t = t.translate(str.maketrans("", "", string.punctuation))
                    t = t.capitalize()
                    return t + "."

                t = preprocess_text(t)
                utt_names.append(utt_name)
                utterances.append(t)
            if utterances == []:
                continue
            text = "\n\n".join(utterances)
            response = requests.post(
                "http://nlp.ilsp.gr/nws/api/", data={"text": text}, timeout=5
            ).json()
            sentences = text.split("\n\n")
            conllus = response["conllu"].split("\n\n")
            if conllus[-1] == "":
                conllus = conllus[0:-1]
            for idx, (utt_name, utterance, conllu) in enumerate(
                zip(utt_names, utterances, conllus)
            ):
                data_dict[speaker]["stories"][story]["utterances"][utt_name][
                    conllu_name
                ] = conllu
    save_dict(data_dict, save_dict_name)


def calculate_conllus_ilsp_df(df, text_column_name="asr_transcription_whisper"):
    conllu_column_name = (text_column_name + "_conllu",)
    import requests

    # df_parse=df[["Utterance ID",text_column_name]]
    utterances = list(df[text_column_name])
    utt_names = list(df["Utterance ID"])

    text = "\n\n".join(utterances)
    response = requests.post(
        "http://nlp.ilsp.gr/nws/api/", data={"text": text}, timeout=5
    ).json()
    sentences = text.split("\n\n")
    conllus = response["conllu"].split("\n\n")
    if conllus[-1] == "":
        conllus = conllus[0:-1]
    df[conllu_column_name] = conllus
    # for idx, (utt_name, utterance, conllu) in enumerate(
    #     zip(utt_names, utterances, conllus)
    # ):
    #     df = pd.merge(df, df_asr, on=["Utterance ID"])

    return df


def create_csv_from_json(data_dict_path, csv_path):
    data_dict = load_dict(data_dict_path)
    # each row contains a tuple: utterance id,participant id, raw_transcript, mark_start,mark_end, file (transcript_path),audio_path,group,file_cut,duration_seconds
    row = []
    for speaker in data_dict.keys():
        for story in data_dict[speaker]["stories"]:
            if not data_dict[speaker]["stories"][story]["story_info"]["audio_path"]:
                continue
            for utt_id in data_dict[speaker]["stories"][story]["utterances"].keys():
                raw_transcript = data_dict[speaker]["stories"][story]["utterances"][
                    utt_id
                ]["raw_transcript"]
                timestamp = data_dict[speaker]["stories"][story]["utterances"][utt_id][
                    "timestamp"
                ]
                duration_seconds = (
                    data_dict[speaker]["stories"][story]["utterances"][utt_id][
                        "duration"
                    ]
                    / 1000
                )
                file_cut = data_dict[speaker]["stories"][story]["utterances"][utt_id][
                    "audio_segment_path"
                ]
                mark_start, mark_end = timestamp_to_ints(timestamp)
                file = data_dict[speaker]["stories"][story]["story_info"][
                    "transcription_path"
                ]
                audio_path = data_dict[speaker]["stories"][story]["story_info"][
                    "audio_path"
                ]
                group = data_dict[speaker]["user_info"]["group"]
                if group == "aphasia":
                    control = False
                else:
                    control = True
                row.append(
                    (
                        utt_id,
                        speaker,
                        raw_transcript,
                        mark_start,
                        mark_end,
                        file,
                        audio_path,
                        group,
                        file_cut,
                        duration_seconds,
                        control,
                    )
                )
    columns = (
        "Utterance ID",
        "Participant ID",
        "raw_transcriptions",
        "mark_start",
        "mark_end",
        "file",
        "audio_path",
        "group",
        "file_cut",
        "duration_seconds",
        "control",
    )
    df = pd.DataFrame(row, columns=columns)
    # df["duration_seconds"]=df.apply(lambda row:((pd.to_numeric(row['mark_end']))-(pd.to_numeric(row['mark_start'])))/1000, axis=1)

    df.to_csv(csv_path, index=False)


def load_aq_from_excel_planv(csv_path,excel_file_path="datasets/planv-aphasia/scores_data.xlsx"):
    df=pd.read_csv(csv_path)
    df_excel = pd.read_excel(excel_file_path, sheet_name='BDAE-WAB')
    df_excel=df_excel[["participant_ID","AQ score"]]
    df_excel=df_excel.dropna()
    # print(df_excel)
    # print(df["Participant ID"].unique())
    # print(df_excel[df_excel['participant_ID'].isin(df["Participant ID"].unique())])
    # df=df.drop(["WAB_AQ"],axis=1)
    df_excel=df_excel.rename({"participant_ID":"Participant ID", "AQ score" : "WAB_AQ"},axis=1)
    df_merged = pd.merge(df, df_excel, on=["Participant ID"])
    # print(df_merged)
    df_merged.to_csv(csv_path,index=False)
    
def load_aq_from_excel_planv(csv_path,excel_file_path="datasets/planv-aphasia/scores_data.xlsx"):
    df=pd.read_csv(csv_path)
    df_excel = pd.read_excel(excel_file_path, sheet_name='BDAE-WAB')
    df_excel=df_excel[["participant_ID","AQ score"]]
    df_excel=df_excel.dropna()
    # print(df_excel)
    # print(df["Participant ID"].unique())
    # print(df_excel[df_excel['participant_ID'].isin(df["Participant ID"].unique())])
    # df=df.drop(["WAB_AQ"],axis=1)
    df_excel=df_excel.rename({"participant_ID":"Participant ID", "AQ score" : "WAB_AQ"},axis=1)
    df_merged = pd.merge(df, df_excel, on=["Participant ID"])
    # print(df_merged)
    df_merged.to_csv(csv_path,index=False)

def silence_utterances_in_data_dict(data_dict_path):

    data_dict = load_dict(data_dict_path)
    for speaker in tqdm.tqdm(data_dict):
        for story in data_dict[speaker]["stories"]:
            wav_fp = data_dict[speaker]["stories"][story]["story_info"]["audio_path"]
            if not wav_fp:
                continue
            audio = AudioSegment.from_wav(wav_fp)
            for idx, utterance_name in enumerate(
                data_dict[speaker]["stories"][story]["utterances"]
            ):
                ts = data_dict[speaker]["stories"][story]["utterances"][utterance_name][
                    "timestamp"
                ]
                start, end = timestamp_to_ints(ts)
                duration = timestamp_to_duration_conversion(ts)
                silence = AudioSegment.silent(duration)
                audio=audio[:start]+silence+audio[end:]
            audio.export(wav_fp,format='wav')
            
    # save_dict(data_dict, data_dict_path)

def get_dataset_silenced_inv():
    create_planv_data(
        root="datasets/planv-aphasia/unprocessed",
        group="control",
        save_dict_name="planv_aphasia_data_no_inv.json",
        # audio_folder="datasets/planv-aphasia/all_files/audio",
    )
    resample_move_audio_files("planv_aphasia_data_no_inv.json",16000,"datasets/planv-aphasia/all_files/audio_silenced_inv")
    
    get_planv_oracle_transcriptions(
        data_dict=load_dict("planv_aphasia_data_no_inv.json"),
        save_dict_name="planv_aphasia_data_no_inv.json",
        tier='TIER_ID="*INV"',
    )

    silence_utterances_in_data_dict("planv_aphasia_data_no_inv.json")


# get_dataset_silenced_inv()

# PLANV FRONTIERS DATA

# create_planv_data(
#     root="datasets/planv_gr/planv-frontiers/unprocessed",
#     group="control",
#     save_dict_name="planv_frontiers_data.json",
#     # audio_folder="datasets/planv_gr/planv-frontiers/all_files/audio",
# )

# resample_move_audio_files("planv_frontiers_data.json",16000,"datasets/planv-frontiers/all_files/audio")

# get_planv_oracle_transcriptions(
#     data_dict=load_dict("planv_frontiers_data.json"),
#     save_dict_name="planv_frontiers_data.json",
#     tier='TIER_ID="*PAR"',
# )
# split_audio_files_stories(
#     "planv_frontiers_data.json",
#     "datasets/planv-frontiers/all_files/audio_segments",
# )
# csv_path_planv_frontiers="datasets/planv-frontiers/all_data.csv"

# create_csv_from_json(
#     data_dict_path="planv_frontiers_data.json",
#     csv_path=csv_path_planv_frontiers,
# )

# df_planv_frontiers=pd.read_csv(csv_path_planv_frontiers)
# df_planv_frontiers["duration_seconds"]=df_planv_frontiers.apply(lambda row:((pd.to_numeric(row['mark_end']))-(pd.to_numeric(row['mark_start'])))/1000, axis=1)
# df_planv_frontiers=df_planv_frontiers[df_planv_frontiers["duration_seconds"]>0.3]

# df_planv_frontiers=clean_greek(df_planv_frontiers)
# df_planv_frontiers.to_csv(csv_path_planv_frontiers,index=False)


# THALES DATA

# resample_move_audio_files("thales_data.json",16000,"datasets/thales-data-with-audio/all_files/audio")

# create_thales_data_dict(
#     data_folder="datasets/thales-data-with-audio/unprocessed",
#     save_dict_name="thales_data.json",
#     audio_folder="datasets/thales-data-with-audio/all_files/audio",
#     tier='TIER_ID="Processed Transcription"',
# )
# split_audio_files_stories(
#     "thales_data.json",
#     "datasets/thales-data-with-audio/all_files/audio_segments",
# )

# create_csv_from_json(
#     data_dict_path="thales_data.json",
#     csv_path="datasets/thales-data-with-audio/all_data.csv",
# )

# PLANV PWA DATA

# create_planv_data(
#     group="aphasia",
#     root="datasets/planv-aphasia/unprocessed",
#     save_dict_name="planv_pwa_data.json",
# )

# resample_move_audio_files("planv_pwa_data.json",16000,"datasets/planv-aphasia/all_files/audio")

# get_planv_oracle_transcriptions(
#     data_dict=load_dict("planv_pwa_data.json"),
#     save_dict_name="planv_pwa_data.json",
#     tier='TIER_ID="*PAR"',
# )
# split_audio_files_stories(
#     "planv_pwa_data.json",
#     "datasets/planv-aphasia/all_files/audio_segments",
# )

# csv_path_planv_aphasia="datasets/planv-aphasia/all_data.csv"
# create_csv_from_json(
#     data_dict_path="planv_pwa_data.json",
#     csv_path=csv_path_planv_aphasia,
# )

# df_planv_aphasia=pd.read_csv(csv_path_planv_aphasia)
# df_planv_aphasia["duration_seconds"]=df_planv_aphasia.apply(lambda row:((pd.to_numeric(row['mark_end']))-(pd.to_numeric(row['mark_start'])))/1000, axis=1)
# df_planv_aphasia=df_planv_aphasia[df_planv_aphasia["duration_seconds"]>0.3]
# df_planv_aphasia=clean_greek(df_planv_aphasia)
# df_planv_aphasia.to_csv(csv_path_planv_aphasia,index=False)
# load_aq_from_excel_planv(csv_path_planv_aphasia,excel_file_path="datasets/planv-aphasia/scores_data.xlsx")

# # MERGE PLANV DATASETS
# csv_path_planv_frontiers="datasets/planv_gr/planv-frontiers/all_data.csv"
# df_planv_frontiers=pd.read_csv(csv_path_planv_frontiers)
# df_planv_frontiers["WAB_AQ"]=None
# csv_path_planv_aphasia="datasets/planv_gr/planv-aphasia/all_data.csv"
# df_planv_aphasia=pd.read_csv(csv_path_planv_aphasia)
# df_planv_aphasia["Participant ID"]=df_planv_aphasia["Participant ID"].astype(str)
# df_planv_frontiers["Participant ID"]=df_planv_frontiers["Participant ID"].astype(str)
# for par in df_planv_aphasia["Participant ID"].unique():
#     if par in df_planv_frontiers["Participant ID"].unique():
#         df_planv_aphasia["Participant ID"]=df_planv_aphasia["Participant ID"].replace({str(par):str(par)+"_pwa"},regex=True)
#         df_planv_aphasia["Utterance ID"]=df_planv_aphasia["Utterance ID"].replace({str(par):str(par)+"_pwa"},regex=True)
#         # df_planv_aphasia[df_planv_aphasia["Participant ID"]==par].replace({str(par):str(par)+"_pwa"},regex=True,inplace=True)
# # df_planv_aphasia["control"]=False
# df_gr=pd.concat([df_planv_aphasia,df_planv_frontiers],axis=0)

# df_gr[
#     [
#         "sex",
#         "age",
#         "aphasia_type",
#         "WAB_AQ_category",
        
#     ]
# ]=[None,None,None,None]

# df_gr.to_csv("datasets/planv_gr/planv_all.csv",index=False)

# csv_path_planv_frontiers = "datasets/planv-frontiers/all_data.csv"

# df_frontiers = pd.read_csv(csv_path_planv_frontiers)
# df_frontiers["control"] = True
# df_frontiers[
#     [
#         "sex",
#         "age",
#         "WAB_AQ",
#         "aphasia_type",
#         "WAB_AQ_category",
        
#     ]
# ]=[None,None,None,None,None]




# df_gr=pd.concat([df_planv_aphasia,df_frontiers],axis=0)
# # print(len(df_frontiers)+len(df_planv_aphasia))
# # print(len(df_gr))
# # print(df_gr["control"].unique())
# df_gr.to_csv("datasets/planv_all.csv",index=False)

# df=run_whisper_greek(df,)
# df.to_csv(csv_path_planv_aphasia,index=False)


# print(df)
# df["duration_seconds"]=df.apply(lambda row:((pd.to_numeric(row['mark_end']))-(pd.to_numeric(row['mark_start'])))/1000, axis=1)
# df.to_csv(csv_path, index=False)



# feature_extractor = DatasetFeatureExtractor(
#         df,
#         language="gr",
#         utterance_column="asr_transcription_whisper",
#         conllu_column="asr_transcription_whisper"+"_conllu",
#         extract_conllu=True,
#     )

# merge_greek_dicts(
#     dict_1=load_dict("thales_data.json"),
#     dict_2=load_dict("planv_frontiers_data.json"),
#     save_dict_name="greek_data.json",
# )
# merge_greek_dicts(
#     dict_1=load_dict("www_greek_data.json"),
#     dict_2=load_dict("www_planv_aphasic_data.json"),
#     save_dict_name="www_greek_data_extended.json",
# )


# calculate_conllus_ilsp(
#     data_dict=load_dict("www_greek_data.json"),
#     key_target="processed_transcript",
#     conllu_name="conllu",
#     save_dict_name="www_greek_data_conllu.json",
# )


# csv_path = "datasets/planv-frontiers/all_data.csv"

# df = pd.read_csv(csv_path)
# df["control"] = True
# df[
#     [
#         "sex",
#         "age",
#         "WAB_AQ",
#         "aphasia_type",
#         "WAB_AQ_category",
        
#     ]
# ]=[None,None,None,None,None]
# print(df)
# # df["duration_seconds"]=df.apply(lambda row:((pd.to_numeric(row['mark_end']))-(pd.to_numeric(row['mark_start'])))/1000, axis=1)
# df.to_csv(csv_path, index=False)

# csv_path = "datasets/planv-aphasia/all_data.csv"
# df1 = pd.read_csv(csv_path)
# print(len(df1))
# csv_path = "datasets/planv-frontiers/all_data.csv"

# df2 = pd.read_csv(csv_path)
# print(len(df2))

# df=calculate_conllus_ilsp_df(df)
# print(df.head())
# print(df["asr_transcription_whisper"])
# create_features_dict(
#     data_dict=load_dict("www_greek_data_conllu.json"),
#     use_asr_transcripts=False,
#     save_dict_name="www_greek_data_conllu_features.json",
#     language="greek",
# )

# OTHER 

# create_segments_greek(
#     datapath="/home/jerrychatz/projects/aphasia-severity-detection/language-agnostic/dataset_planv",
#     save_dict_name="www_planv_aphasic_data.json",
# )

# get_greek_asr_transcriptions(
#     data_dict=load_dict("www_planv_aphasic_data.json"),
#     save_dict_name="www_planv_aphasic_data.json",
#     logits_path=None,
#     model_id="jonatasgrosman/wav2vec2-large-xlsr-53-greek",
#     add_language_model=False,
#     kenlm_model_path=None,
# )