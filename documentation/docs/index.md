## Documentation

Welcome to Plan-V Documentation page :computer:	:brain:		
This page serves as a means of providing some of the work's valuable links, files, code and resources. Our goal through this page is to give some insights into

- [x] our thought process :thought_balloon:
- [x] our work :construction_worker:
- [x] our machine learning algorithms :robot:

we deployed to work towards aphasia severity detection and classification. 

<!-- - [Workflows](tagifai/main.md): main workflows.
- [tagifai](tagifai/data.md): documentation of functionality. -->

## Some useful links

To get an insight about aphasia, we strongly recommend to access the content from the

- National Aphasia Association: [https://www.aphasia.org/](https://www.aphasia.org/)
- AphasiaBank Teaching Section: [education/examples/](https://aphasia.talkbank.org/education/examples/)

## Quick Navigation

- [Research outcomes](outcomes/main.md): brief summary of our current research outcomes.
- Experimental Workflows
    - [Datasets](workflows/data.md): download and create a multi-lingual dataset.
    - [Preprocessing](workflows/preprocessing.md): data preprocessing steps.
    - [Dependency Parsing](workflows/parsing.md): showcase dependency parsers available and used.
    - [Feature Extraction](workflows/features.md): feature extraction pipelines.
    - [Wav2Vec2-XLSR](workflows/asr.md): automatic speech recognition models.
    - [Single-Language Experiments](workflows/loso.md): english LOSO experiment.
    - [Zero-Shot Cross-lingual experiments](workflows/cross.md): transfer learning from english to french and greek.
- Personal Notes: 
    - [On Aphasia](notes/main.md): unofficial personal notes while watching AphasiaBank's teaching content and research progress.
    - [On research progress](notes/research.md): research progress notes.
    - [On random paper and ideas](notes/alltogether.md): various documents and notes.
- [Web app aplications](apps/main.md): links to repos of our web apps.
- [Miro Designs](designs/main.md): initial investigation of how the miro page will look like.

<p align="center">
<img src="https://ih1.redbubble.net/image.1815154837.9405/st,small,507x507-pad,600x600,f8f8f8.jpg" alt="drawing" width="400"/>
</p>