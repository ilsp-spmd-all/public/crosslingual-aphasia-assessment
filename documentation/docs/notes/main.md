# Notes

<div class="grid cards" markdown>

- [__Personal Notes__ made from AphasiaBank's Educational Content]()
- [__Research Progress Notes__]()



</div>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><title>AphasiaBank: Data Inspection</title><style>
/* cspell:disable-file */
/* webkit printing magic: print all background colors */
html {
	-webkit-print-color-adjust: exact;
}
* {
	box-sizing: border-box;
	-webkit-print-color-adjust: exact;
}

html,
body {
	margin: 0;
	padding: 0;
}
@media only screen {
	body {
		margin: 2em auto;
		max-width: 900px;
		color: rgb(55, 53, 47);
	}
}

body {
	line-height: 1.5;
	white-space: pre-wrap;
}

a,
a.visited {
	color: inherit;
	text-decoration: underline;
}

.pdf-relative-link-path {
	font-size: 80%;
	color: #444;
}

h1,
h2,
h3 {
	letter-spacing: -0.01em;
	line-height: 1.2;
	font-weight: 600;
	margin-bottom: 0;
}

.page-title {
	font-size: 2.5rem;
	font-weight: 700;
	margin-top: 0;
	margin-bottom: 0.75em;
}

h1 {
	font-size: 1.875rem;
	margin-top: 1.875rem;
}

h2 {
	font-size: 1.5rem;
	margin-top: 1.5rem;
}

h3 {
	font-size: 1.25rem;
	margin-top: 1.25rem;
}

.source {
	border: 1px solid #ddd;
	border-radius: 3px;
	padding: 1.5em;
	word-break: break-all;
}

.callout {
	border-radius: 3px;
	padding: 1rem;
}

figure {
	margin: 1.25em 0;
	page-break-inside: avoid;
}

figcaption {
	opacity: 0.5;
	font-size: 85%;
	margin-top: 0.5em;
}

mark {
	background-color: transparent;
}

.indented {
	padding-left: 1.5em;
}

hr {
	background: transparent;
	display: block;
	width: 100%;
	height: 1px;
	visibility: visible;
	border: none;
	border-bottom: 1px solid rgba(55, 53, 47, 0.09);
}

img {
	max-width: 100%;
}

@media only print {
	img {
		max-height: 100vh;
		object-fit: contain;
	}
}

@page {
	margin: 1in;
}

.collection-content {
	font-size: 0.875rem;
}

.column-list {
	display: flex;
	justify-content: space-between;
}

.column {
	padding: 0 1em;
}

.column:first-child {
	padding-left: 0;
}

.column:last-child {
	padding-right: 0;
}

.table_of_contents-item {
	display: block;
	font-size: 0.875rem;
	line-height: 1.3;
	padding: 0.125rem;
}

.table_of_contents-indent-1 {
	margin-left: 1.5rem;
}

.table_of_contents-indent-2 {
	margin-left: 3rem;
}

.table_of_contents-indent-3 {
	margin-left: 4.5rem;
}

.table_of_contents-link {
	text-decoration: none;
	opacity: 0.7;
	border-bottom: 1px solid rgba(55, 53, 47, 0.18);
}

table,
th,
td {
	border: 1px solid rgba(55, 53, 47, 0.09);
	border-collapse: collapse;
}

table {
	border-left: none;
	border-right: none;
}

th,
td {
	font-weight: normal;
	padding: 0.25em 0.5em;
	line-height: 1.5;
	min-height: 1.5em;
	text-align: left;
}

th {
	color: rgba(55, 53, 47, 0.6);
}

ol,
ul {
	margin: 0;
	margin-block-start: 0.6em;
	margin-block-end: 0.6em;
}

li > ol:first-child,
li > ul:first-child {
	margin-block-start: 0.6em;
}

ul > li {
	list-style: disc;
}

ul.to-do-list {
	text-indent: -1.7em;
}

ul.to-do-list > li {
	list-style: none;
}

.to-do-children-checked {
	text-decoration: line-through;
	opacity: 0.375;
}

ul.toggle > li {
	list-style: none;
}

ul {
	padding-inline-start: 1.7em;
}

ul > li {
	padding-left: 0.1em;
}

ol {
	padding-inline-start: 1.6em;
}

ol > li {
	padding-left: 0.2em;
}

.mono ol {
	padding-inline-start: 2em;
}

.mono ol > li {
	text-indent: -0.4em;
}

.toggle {
	padding-inline-start: 0em;
	list-style-type: none;
}

/* Indent toggle children */
.toggle > li > details {
	padding-left: 1.7em;
}

.toggle > li > details > summary {
	margin-left: -1.1em;
}

.selected-value {
	display: inline-block;
	padding: 0 0.5em;
	background: rgba(206, 205, 202, 0.5);
	border-radius: 3px;
	margin-right: 0.5em;
	margin-top: 0.3em;
	margin-bottom: 0.3em;
	white-space: nowrap;
}

.collection-title {
	display: inline-block;
	margin-right: 1em;
}

.simple-table {
	margin-top: 1em;
	font-size: 0.875rem;
	empty-cells: show;
}
.simple-table td {
	height: 29px;
	min-width: 120px;
}

.simple-table th {
	height: 29px;
	min-width: 120px;
}

.simple-table-header-color {
	background: rgb(247, 246, 243);
	color: black;
}
.simple-table-header {
	font-weight: 500;
}

time {
	opacity: 0.5;
}

.icon {
	display: inline-block;
	max-width: 1.2em;
	max-height: 1.2em;
	text-decoration: none;
	vertical-align: text-bottom;
	margin-right: 0.5em;
}

img.icon {
	border-radius: 3px;
}

.user-icon {
	width: 1.5em;
	height: 1.5em;
	border-radius: 100%;
	margin-right: 0.5rem;
}

.user-icon-inner {
	font-size: 0.8em;
}

.text-icon {
	border: 1px solid #000;
	text-align: center;
}

.page-cover-image {
	display: block;
	object-fit: cover;
	width: 100%;
	max-height: 30vh;
}

.page-header-icon {
	font-size: 3rem;
	margin-bottom: 1rem;
}

.page-header-icon-with-cover {
	margin-top: -0.72em;
	margin-left: 0.07em;
}

.page-header-icon img {
	border-radius: 3px;
}

.link-to-page {
	margin: 1em 0;
	padding: 0;
	border: none;
	font-weight: 500;
}

p > .user {
	opacity: 0.5;
}

td > .user,
td > time {
	white-space: nowrap;
}

input[type="checkbox"] {
	transform: scale(1.5);
	margin-right: 0.6em;
	vertical-align: middle;
}

p {
	margin-top: 0.5em;
	margin-bottom: 0.5em;
}

.image {
	border: none;
	margin: 1.5em 0;
	padding: 0;
	border-radius: 0;
	text-align: center;
}

.code,
code {
	background: rgba(135, 131, 120, 0.15);
	border-radius: 3px;
	padding: 0.2em 0.4em;
	border-radius: 3px;
	font-size: 85%;
	tab-size: 2;
}

code {
	color: #eb5757;
}

.code {
	padding: 1.5em 1em;
}

.code-wrap {
	white-space: pre-wrap;
	word-break: break-all;
}

.code > code {
	background: none;
	padding: 0;
	font-size: 100%;
	color: inherit;
}

blockquote {
	font-size: 1.25em;
	margin: 1em 0;
	padding-left: 1em;
	border-left: 3px solid rgb(55, 53, 47);
}

.bookmark {
	text-decoration: none;
	max-height: 8em;
	padding: 0;
	display: flex;
	width: 100%;
	align-items: stretch;
}

.bookmark-title {
	font-size: 0.85em;
	overflow: hidden;
	text-overflow: ellipsis;
	height: 1.75em;
	white-space: nowrap;
}

.bookmark-text {
	display: flex;
	flex-direction: column;
}

.bookmark-info {
	flex: 4 1 180px;
	padding: 12px 14px 14px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
}

.bookmark-image {
	width: 33%;
	flex: 1 1 180px;
	display: block;
	position: relative;
	object-fit: cover;
	border-radius: 1px;
}

.bookmark-description {
	color: rgba(55, 53, 47, 0.6);
	font-size: 0.75em;
	overflow: hidden;
	max-height: 4.5em;
	word-break: break-word;
}

.bookmark-href {
	font-size: 0.75em;
	margin-top: 0.25em;
}

.sans { font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol"; }
.code { font-family: "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace; }
.serif { font-family: Lyon-Text, Georgia, ui-serif, serif; }
.mono { font-family: iawriter-mono, Nitti, Menlo, Courier, monospace; }
.pdf .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK JP'; }
.pdf:lang(zh-CN) .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK SC'; }
.pdf:lang(zh-TW) .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK TC'; }
.pdf:lang(ko-KR) .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK KR'; }
.pdf .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK JP'; }
.pdf:lang(zh-CN) .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK SC'; }
.pdf:lang(zh-TW) .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK TC'; }
.pdf:lang(ko-KR) .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK KR'; }
.pdf .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK JP'; }
.pdf:lang(zh-CN) .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK SC'; }
.pdf:lang(zh-TW) .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK TC'; }
.pdf:lang(ko-KR) .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK KR'; }
.pdf .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK JP'; }
.pdf:lang(zh-CN) .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK SC'; }
.pdf:lang(zh-TW) .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK TC'; }
.pdf:lang(ko-KR) .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK KR'; }
.highlight-default {
	color: rgba(55, 53, 47, 1);
}
.highlight-gray {
	color: rgba(120, 119, 116, 1);
	fill: rgba(120, 119, 116, 1);
}
.highlight-brown {
	color: rgba(159, 107, 83, 1);
	fill: rgba(159, 107, 83, 1);
}
.highlight-orange {
	color: rgba(217, 115, 13, 1);
	fill: rgba(217, 115, 13, 1);
}
.highlight-yellow {
	color: rgba(203, 145, 47, 1);
	fill: rgba(203, 145, 47, 1);
}
.highlight-teal {
	color: rgba(68, 131, 97, 1);
	fill: rgba(68, 131, 97, 1);
}
.highlight-blue {
	color: rgba(51, 126, 169, 1);
	fill: rgba(51, 126, 169, 1);
}
.highlight-purple {
	color: rgba(144, 101, 176, 1);
	fill: rgba(144, 101, 176, 1);
}
.highlight-pink {
	color: rgba(193, 76, 138, 1);
	fill: rgba(193, 76, 138, 1);
}
.highlight-red {
	color: rgba(212, 76, 71, 1);
	fill: rgba(212, 76, 71, 1);
}
.highlight-gray_background {
	background: rgba(241, 241, 239, 1);
}
.highlight-brown_background {
	background: rgba(244, 238, 238, 1);
}
.highlight-orange_background {
	background: rgba(251, 236, 221, 1);
}
.highlight-yellow_background {
	background: rgba(251, 243, 219, 1);
}
.highlight-teal_background {
	background: rgba(237, 243, 236, 1);
}
.highlight-blue_background {
	background: rgba(231, 243, 248, 1);
}
.highlight-purple_background {
	background: rgba(244, 240, 247, 0.8);
}
.highlight-pink_background {
	background: rgba(249, 238, 243, 0.8);
}
.highlight-red_background {
	background: rgba(253, 235, 236, 1);
}
.block-color-default {
	color: inherit;
	fill: inherit;
}
.block-color-gray {
	color: rgba(120, 119, 116, 1);
	fill: rgba(120, 119, 116, 1);
}
.block-color-brown {
	color: rgba(159, 107, 83, 1);
	fill: rgba(159, 107, 83, 1);
}
.block-color-orange {
	color: rgba(217, 115, 13, 1);
	fill: rgba(217, 115, 13, 1);
}
.block-color-yellow {
	color: rgba(203, 145, 47, 1);
	fill: rgba(203, 145, 47, 1);
}
.block-color-teal {
	color: rgba(68, 131, 97, 1);
	fill: rgba(68, 131, 97, 1);
}
.block-color-blue {
	color: rgba(51, 126, 169, 1);
	fill: rgba(51, 126, 169, 1);
}
.block-color-purple {
	color: rgba(144, 101, 176, 1);
	fill: rgba(144, 101, 176, 1);
}
.block-color-pink {
	color: rgba(193, 76, 138, 1);
	fill: rgba(193, 76, 138, 1);
}
.block-color-red {
	color: rgba(212, 76, 71, 1);
	fill: rgba(212, 76, 71, 1);
}
.block-color-gray_background {
	background: rgba(241, 241, 239, 1);
}
.block-color-brown_background {
	background: rgba(244, 238, 238, 1);
}
.block-color-orange_background {
	background: rgba(251, 236, 221, 1);
}
.block-color-yellow_background {
	background: rgba(251, 243, 219, 1);
}
.block-color-teal_background {
	background: rgba(237, 243, 236, 1);
}
.block-color-blue_background {
	background: rgba(231, 243, 248, 1);
}
.block-color-purple_background {
	background: rgba(244, 240, 247, 0.8);
}
.block-color-pink_background {
	background: rgba(249, 238, 243, 0.8);
}
.block-color-red_background {
	background: rgba(253, 235, 236, 1);
}
.select-value-color-pink { background-color: rgba(245, 224, 233, 1); }
.select-value-color-purple { background-color: rgba(232, 222, 238, 1); }
.select-value-color-green { background-color: rgba(219, 237, 219, 1); }
.select-value-color-gray { background-color: rgba(227, 226, 224, 1); }
.select-value-color-opaquegray { background-color: rgba(255, 255, 255, 0.0375); }
.select-value-color-orange { background-color: rgba(250, 222, 201, 1); }
.select-value-color-brown { background-color: rgba(238, 224, 218, 1); }
.select-value-color-red { background-color: rgba(255, 226, 221, 1); }
.select-value-color-yellow { background-color: rgba(253, 236, 200, 1); }
.select-value-color-blue { background-color: rgba(211, 229, 239, 1); }

.checkbox {
	display: inline-flex;
	vertical-align: text-bottom;
	width: 16;
	height: 16;
	background-size: 16px;
	margin-left: 2px;
	margin-right: 5px;
}

.checkbox-on {
	background-image: url("data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2216%22%20height%3D%2216%22%20viewBox%3D%220%200%2016%2016%22%20fill%3D%22none%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0A%3Crect%20width%3D%2216%22%20height%3D%2216%22%20fill%3D%22%2358A9D7%22%2F%3E%0A%3Cpath%20d%3D%22M6.71429%2012.2852L14%204.9995L12.7143%203.71436L6.71429%209.71378L3.28571%206.2831L2%207.57092L6.71429%2012.2852Z%22%20fill%3D%22white%22%2F%3E%0A%3C%2Fsvg%3E");
}

.checkbox-off {
	background-image: url("data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2216%22%20height%3D%2216%22%20viewBox%3D%220%200%2016%2016%22%20fill%3D%22none%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0A%3Crect%20x%3D%220.75%22%20y%3D%220.75%22%20width%3D%2214.5%22%20height%3D%2214.5%22%20fill%3D%22white%22%20stroke%3D%22%2336352F%22%20stroke-width%3D%221.5%22%2F%3E%0A%3C%2Fsvg%3E");
}
	
</style></head><body><article id="7167c455-cf46-429e-981c-5ca00584c557" class="page sans"><header><h1 class="page-title">AphasiaBank: Data Inspection</h1></header><div class="page-body"><figure id="f9e721b1-f114-4109-82f3-87606f0aea3c" class="image"><a href="AphasiaBank%20Data%20Inspection%20f9e721b1f114410982f387606f0aea3c/Untitled.png"><img style="width:467px" src="AphasiaBank%20Data%20Inspection%20f9e721b1f114410982f387606f0aea3c/Untitled.png"/></a></figure><nav id="0b5c436c-3062-476d-88c9-28a538b71e54" class="block-color-gray table_of_contents"><div class="table_of_contents-item table_of_contents-indent-0"><a class="table_of_contents-link" href="#140d2cf1-3156-4c4a-ab2e-a7caa2e375f2">Broca</a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#d83c2afd-470e-492c-b86c-d553c4599a7a">Εισαγωγή</a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#e6816e51-bd9f-4b30-a6aa-28c66f83a7fb">Προσωπικές Σημειώσεις </a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#895a28ee-e722-4366-b3f4-8a31c5bae11b"><strong>ACWT01a [ Broca | AQ: 63.9 | female | age 69 ]</strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#d87a6cea-24b1-4c1a-9903-7f616cc40391"><strong>ACWT05a [ Broca | AQ: 57.7 | male | age 75 ]</strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#15b07259-8a18-4abc-b426-8d80f3f7b31e"><strong>ACWT08a [ Broca | AQ: 40.5 | female | age 39 ]</strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#f3b1aba3-e6ca-4f09-9cb5-f91da7984fa8"><strong>Adler10a [ Broca | AQ: 51.2 | male | age 44 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#e78437a5-094e-4c55-9599-ee226576a5b9"><strong>Adler11a [ Broca | AQ: 17.0 | male | age 80 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#82cec9f6-9a1b-48be-8b81-41268786514a"><strong>Adler13a [ Broca | AQ: 55.8 | male | age 52 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#8d0d6b59-7e7a-46f4-87f6-69120c2f823b">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#629d03c6-ba38-4188-8b2b-42c1aa1aaa03">Σημειώσεις από το Teaching Section - Students του AphasiaBank - Δεν με αφήνει να μπω !</a></div><div class="table_of_contents-item table_of_contents-indent-0"><a class="table_of_contents-link" href="#c51eff8a-79bd-4480-a0ee-5168933bea8f">Wernicke</a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#d23af953-30e1-4281-a728-5eef6be7b0d7">Εισαγωγή</a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#04a23306-ac4c-4b3e-808e-d085b4e59bea">Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#da722cb6-0562-447f-ac95-699b09adf248"><strong>ACWT10a [ Wernicke | AQ: 57.4 | female | age 48 ] - Πρέπει να έχουν λάθος τα scripts !!!!!!!</strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#21c8ee0b-afc9-4058-9dea-9fc8aa5668c2"><strong>ACWT11a [ Wernicke | AQ: 48.9 | male | age 61 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#7e1ff665-f116-49e9-b379-c71e54f56bf5"><strong>Adler06a [ Wernicke | AQ: 28.2 | male | age 70 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#de971270-b536-41c1-8872-b7cfbd2cd78d"><strong>Adler23a [ Wernicke | AQ: 46.8 | male | age 81 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#20b02f32-8474-452b-8b00-96ad3783e549">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#14581265-d5d5-48a5-8de4-9a86f02bc320">Σημειώσεις από το Teaching Section - Students του AphasiaBank - Δεν με αφήνει να μπω !</a></div><div class="table_of_contents-item table_of_contents-indent-0"><a class="table_of_contents-link" href="#6351bc58-0dd9-4856-b499-be36018c5b23">TransMotor </a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#d0168527-2c9f-4410-805a-95002e4d3ded">Εισαγωγή </a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#7aef74d4-67aa-4bd4-9ec6-8f4d58a1f12f">Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#5886770b-8e6b-434c-af52-183cb8c91bb5">ACWT02a [ TransMotor | <strong>AQ: 74.6 | female | age 53 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#1cef5fb7-b5ab-4370-9595-01ccf6533eb9">ACWT03a [ TransMotor | <strong>AQ: 69.3 | male | age 68] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#31cc86a5-4f78-4eef-b17c-f47fa2f4766d">Adler04a [ TransMotor | <strong>AQ: 72.6 | male | age 75] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#13c121d5-0178-49a8-8131-f74c52115ba6">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#884b2fb2-597a-4524-af0b-33e84c9639f4">Σημειώσεις από το Teaching Section - Students του AphasiaBank - Δεν με αφήνει να μπω !</a></div><div class="table_of_contents-item table_of_contents-indent-0"><a class="table_of_contents-link" href="#2f790d2a-9eda-4c58-a9c9-c4c40d8cb90e">Anomic </a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#e299db5d-f94d-4ee1-8760-097f699068d6">Εισαγωγή </a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#d5036375-f3cb-4c9a-ab89-eceeee92366f">Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#fcf45484-6661-444b-a4df-f52567017268">Adler01a [ Anomic | <strong>AQ: 86.8 | male | age 58 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#4e86f86d-7ced-44d8-8c4a-b79d1fb89bda">Adler02a [ Anomic | xx<strong> | xx | xx ]  → SOS εχουν κάνει λάθος, έχουν άλλα transcripts </strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#3a82304b-a258-4aa1-9386-a0dcc9925ab6">Adler08a [ Anomic | <strong>AQ: 78.5 | male | age 56 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#787260a4-5319-49af-8fc3-ebae632b4b42">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-0"><a class="table_of_contents-link" href="#4888fd41-08fc-4fe2-bac6-c55ab61cdd94">Conduction</a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#ba413b08-b358-4e61-b116-1dc0a64a1314">Εισαγωγή </a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#1dddfb0a-9cc9-481f-a225-45b15a6c6611">Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#f2494332-0a41-46f3-bbdd-da35e452a9e3">ACWT09a [ Conduction | <strong>AQ: 80.1 | female | age 56 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#74dd0848-50fe-4a9a-a285-58a36f3d0736">Adler05a [ Conduction | <strong>AQ: 65.5 | female | age 68 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#2a5d649f-2d36-47d3-9ed2-c2f33b57b20a">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-0"><a class="table_of_contents-link" href="#30bdc22d-3fa2-42b4-84ae-0acdbbd9271e">Global Aphasia</a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#52bda80b-90f6-4d90-91de-c8bd3780bf37">Εισαγωγή </a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#73b880bc-3def-4522-a20f-0ac04c5597f6">Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#d0ac51a1-3ea9-427e-b4a7-425eef2a61d6">tap09a [ Global | <strong>AQ: 20.5 | male | age 70 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#7ad86873-9be9-4add-8e92-f631ea1a95c9">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-0"><a class="table_of_contents-link" href="#5bb54e40-b1e0-4c2c-8210-e5ea5c100508">TransSensory </a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#aabc7e7e-2d7f-49df-8ad2-6aba2dfa4f6e">Εισαγωγή </a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#ce8995cd-8052-4c30-99ba-5d477a57c753">Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#37766be7-ecff-4427-9f6e-0613e04a090f">scale12a [ TransSensory | <strong>AQ: 54.1 | male | age 57 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#c41bcd8a-4bc9-4a56-890f-4de66b13246e">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-0"><a class="table_of_contents-link" href="#546b9321-dd72-4e9a-a6ce-fc326d729270">Not Aphasic </a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#dee7ffbe-1565-4e52-8822-996d8652a3fa">Εισαγωγή </a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#0705224a-69d3-4ca3-8944-7ecd4a8b8693">Προσωπικές Σημειώσεις</a></div><div class="table_of_contents-item table_of_contents-indent-2"><a class="table_of_contents-link" href="#6293c64d-782f-44ce-8bc6-6f9ae1265ed6">ACWT04a [ NotAphasicByWAB | <strong>AQ: 96.0 | female | age 26 ] </strong></a></div><div class="table_of_contents-item table_of_contents-indent-1"><a class="table_of_contents-link" href="#5cc385cd-5d42-4ec0-a470-a9de11d8f731">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</a></div></nav><h1 id="140d2cf1-3156-4c4a-ab2e-a7caa2e375f2" class="">Broca</h1><h2 id="d83c2afd-470e-492c-b86c-d553c4599a7a" class="">Εισαγωγή</h2><p id="de1d62c2-55bb-44c6-b4cf-b6a94728e569" class="">Ξεκινάω να μελετώ σε πρώτη φάση τους αφασικούς ασθενείς που πάσχουν από αφασία τύπου Broca. Οι ασθενείς αυτοί αναμένεται να έχουν προβλήματα στην ομιλία, όχι όμως στην κατανόηση</p><h2 id="e6816e51-bd9f-4b30-a6aa-28c66f83a7fb" class="">Προσωπικές Σημειώσεις </h2><p id="711d46d1-95c1-4dcb-8f5e-f48739f2230c" class="">
</p><h3 id="895a28ee-e722-4366-b3f4-8a31c5bae11b" class=""><strong>ACWT01a [ Broca | AQ: 63.9 | female | age 69 ]</strong></h3><p id="cfb2caa7-42d8-4f33-933b-3e06545fafac" class="">Μπορεί να κατανοήσει γεγονός το οποίο δικαιολογείται από τις γρήγορες αποκρίσεις στις υποδείξεις και τις ερωτήσεις της εξετάστριας. Χρησιμοποεί νοήματα με το αριστερό της χέρι διότι το δεξί είναι παράλυτο. Χρησιμοποιεί σημαντικές λέξεις για την περιγραφή της ιστορίας αλλά δεν μπορεί να αρθρώσει ολοκληρωμένες προτάσεις. </p><p id="62cce2e1-6e11-412a-89a1-a153142c3ed3" class="">
</p><h3 id="d87a6cea-24b1-4c1a-9903-7f616cc40391" class=""><strong>ACWT05a [ Broca | AQ: 57.7 | male | age 75 ]</strong></h3><p id="0adc2cbe-ceb5-4804-b28c-c3eeeefc022d" class="">Καταλαβαίνει και αποκρίνεται άμεσα. Χρησιμοποιεί σκόρπιες λέξεις, σημαντικές για την ιστορία που εξιστορεί. Κοιτάζει εξετάστρια για βοήθεια και χρησιμοποιεί νοήματα. Το δεξί του χέρι είναι παράλυτο. Πολύ σημαντικό είναι οτι στην ιστορία της σταχτοπούτας μολονότι καταλαβαίνει, δεν μπορεί και δεν χρησιμοποεί κάποιες σημαντικές λέξεις. Παρόλα αυτά, όταν του κάνει ερωτήσεις η εξετάστρια, κατανοεί άμεσα και λέει &#x27;yes&#x27; όπου χρειάζεται. Ίσως αυτό να είναι μια ένδειξη οτι θα πρέπει να δώσουμε και μια δόση διαλογικού χαρακτήρα στην εφαρμογή μας. </p><p id="3d071bba-8a4b-4793-899a-4b1f17f485bb" class="">
</p><h3 id="15b07259-8a18-4abc-b426-8d80f3f7b31e" class=""><strong>ACWT08a [ Broca | AQ: 40.5 | female | age 39 ]</strong></h3><p id="14ec46c9-996d-4ee9-8153-f293219c50b9" class="">Κατανοεί. Παρόλα αυτά, δεν βρίσκει λέξεις σε σύγκριση με τους προηγούμενουυς. Επαναλαμβάνει συγκεκριμένες λέξεις (&#x27;yes&#x27;, &#x27;no&#x27;, &#x27;i don&#x27;t know&#x27;) και εκφράζει τη δυσαρέσκειά της που αδυνατεί να μιλήσει (πχ &#x27;oh my goodness&#x27;). Χρησιμοποιεί νοήματα. Δεξί χέρι παράλυτο και σε αυτή την περίπτωση. </p><p id="ad75a2a2-1595-41bc-b03e-73cb70694a3b" class="">
</p><h3 id="f3b1aba3-e6ca-4f09-9cb5-f91da7984fa8" class=""><strong>Adler10a [ Broca | AQ: 51.2 | male | age 44 ] </strong></h3><p id="675b5897-33f7-4445-8972-064dafcf6270" class=""><strong>@Speech:  </strong>Κατανοεί. Απαντά slow. Εκφέρει λέξεις (speech, how, about). Δεν μπορεί να διατυπώσει προτάσεις. </p><p id="908da810-5430-4f69-8a61-6b7fef41554b" class=""><strong>@Stroke: </strong>Κατανοεί. Sleeping. Παρατήρηση πως χρησιμοποεί νοήματα. Δεξί χέρι παράλυτο. Μολονότι δυσκολεύεται να διατυπώσει ολοκληρωμένες προτάσεις, φαίνεται πως γνωρίζει καλά τι θέλει να πει. Θυμάται επίσης και τι έκανε για αποκετάσταση (exercise, swimming). Δυσκολεύεται να βρει λέξη και κάνει πάλι νοήματα με το ένα χέρι. Έως τώρα έχει σχηματίσει τις περισσότερες λέξεις από όσους έχω ακούσει. <strong> </strong></p><p id="42703e56-5432-49ca-815c-0cb0ab39e22f" class=""><strong>@Important Event: </strong>Κατανοεί και ανταποκρίνεται (okay). Σχηματίζει λέξεις, ξεχνάει (μου φάνηκε προς στιγμήν) αλλά κάτι έψαχνε που δεν τον άφηνε η εξετάστρια να χρησιμοποιήσεις (εικάζω κινητό) (λέει hold on και ψάχνει - τέτοια γεγονότα θα δυσκολέψουν το task μας καθώς απαιτείται κατανόηση της περίστασης). Μετά γίνεται ακατανόητος και συνειδητοποεί πως δεν μπορεί να εξηγήσει το γεγονός).</p><p id="f673fb32-7771-4ac2-89de-6e8c77d91ca2" class=""><mark class="highlight-red">Note: Στα δεδομένα μας πρέπει να έχουμε υπόψιν μας οτι υπάρχουν πολλά fillers (e.g. hmm) ή κομμάτια σιωπής (www)</mark></p><p id="daa99b56-abc7-472c-be05-f777fae82bf0" class=""><strong>@Window:  </strong>Κατανοέι (okay μετά από ερώτηση)</p><p id="bcfbf8de-c9a7-442d-99d5-cd4af6430d48" class=""><mark class="highlight-red">Note: Εδώ ίσως μπορούμε να χρησιμοποιήσουμε τα δευτερόλεπτα της απόκρισης ύστερα από ερώτηση της εξετάστριας δλδ να καταλαβαίνουμε ακουστικά πότε γίνεται ερώτηση (μπορούμε να δούμε και τα πρωτόκολλα - χρησιμοποιούν συνήθως συγκεκριμένες φράσεις και να μετράμε την απόκριση ή να χωρίζουμε το σήμα σε speakers να βλέπουμε πόσα δευτερόλεπτα χρειάζεται ο ασθενής προτού απαντήσει στην εξετάστρια). </mark></p><p id="2dc2b367-b490-4dfa-87f9-f25bac18d7b3" class="">Ball, sleep, window και δείχνει (this one, this, this ...). </p><p id="2ff11b77-6731-4097-8762-9f3ef19fc6e9" class=""><strong>@Umbrella: </strong></p><p id="672d1047-687d-4f5b-b297-b50fb06505d1" class=""><mark class="highlight-red">Note: Αυτό με την ταχύτητα απόκρισης θέλει προσοχή όπως στη συγκεκεριμένη περίσταση καθώς παίρνει λίγο χρόνο να κοιτάξει τις εικόνες</mark></p><p id="39bd837a-24fa-4281-8257-050f547f85ad" class="">Rain, gear, sleeping. </p><p id="fc3a843a-9297-4b4c-9d8a-3d9bbdf5a762" class=""><mark class="highlight-red">Note: Έχω την εντύπωση πως πολλές φορές οι ασθενείς χρησιμοποιούν συγκεκριμένες λέξεις στο λεξιλόγιό τους. Αυτές οι λέξεις είναι λέξεις που τους έχουν καρφωθεί και τις χρησιμοποιούν συνέχεια νομίζω γιατί μπορούν να γίνουν κατανοητοί με αυτές. Άλλοι επαναλαμβάνουν διαρκώς Yes-yes-yes, this-this. Αυτός παρατηρώ πως χρησιμοποιεί αρκετές φορές τη λέξη sleep.</mark></p><p id="6d1840f4-2214-4245-8c30-e3507eb7d4fe" class=""><strong>@Cat: </strong>Fire (αποκρίνεται κατευθείαν). Fire mens, tree, sleep (ας πούμε πάλι επαναλαμβάνει λέξη άσχετη), kitty. Λέει dike και το διορθώνει σε bike γρήγορα. </p><p id="ad115e02-ed57-4811-bb30-590c46299224" class=""><strong>@Flood: </strong>Hold on (το έχει ξαναχρησιμοποιήσει). Αργεί και δυσκολεύεται. Firemens, machine, this. Δεν τα πήγε καλά. </p><p id="07820e9d-f35f-4656-8711-add5d5a69fd6" class=""><strong>@Cinderella Intro: </strong>Ανταποκρίνεται γρήγορα. Εδώ υπάρχει αρκετή σιγή καθώς διαβάζει την ιστορία. <strong> </strong></p><p id="1f159cdb-d39f-4c4f-ac26-7bb70b6e6790" class=""><strong>@Cinderella: </strong>Cinderella. I know and I don&#x27;t know. way way down.</p><p id="0516d451-bc46-40e7-8d5d-0cd954103b19" class=""><mark class="highlight-red">Note: Νομίζω πως όταν θέλει να πει κάποια ώρα ή έναν αριθμό, μετράει με τα δάχτυλα και φτάνει μέχρι τη μία παλάμη νομίζω αν φτάσει στο 5. </mark></p><p id="ffac32b3-a807-470f-bef7-9af192ed6d68" class=""><strong>@Sandwich: </strong>Κατανοεί τι του ζητείται αλλά δεν μπορεί. Κάνει νοήματα για να το εξηγήσει. </p><p id="f39f10ec-e71d-4971-8c86-0a72cb86b8b9" class="">
</p><p id="1f52f2cc-6c2c-4612-8425-29da0c725cf9" class=""><strong>Summing Up: </strong>Κατανοεί. Αποκρίνεται άμεσα. Χρησιμοποιεί νοήματα όταν δεν μπορεί να βρει κάποιες λέξεις. Δεξί χέρι παράλυτο. Λέει λέξεις αλλά αδυνατεί να διατυπώσει προτάσεις. Σε tasks με κάποια ιστορία δυσκολεύται πάρα πολύ. Νομίζω επαναλαμβάνει κάποιες συγκεκριμένες λέξεις.</p><p id="eb841ae7-198a-4d4e-8675-36f463f7790a" class="">
</p><h3 id="e78437a5-094e-4c55-9599-ee226576a5b9" class=""><strong>Adler11a [ Broca | AQ: 17.0 | male | age 80 ] </strong></h3><p id="6d2452bc-ebcd-4e39-b105-3f0882d4283d" class=""><strong>@Speech: </strong></p><p id="691c4015-fce8-4b12-bdd0-824ae87af071" class=""><strong>@Stroke: </strong>Καταλαβαίνει. Ζωγραφίζει και δεν μπορεί να μιλήσει. One. </p><p id="6e99091f-057f-4f01-91fd-e3fc8a32356c" class=""><strong>@Important Event: </strong>Καταλαβαίνει. Ζωγραφίζει τις απαντήσεις του. Δεν μιλάει. Δεξί χέρι παράλυτο. Yes/No/yeah, ζωγραφιές, νοήματα, ήχοι, μουσική. Φαίνεται να μπορεί να διαβάσει. </p><p id="86cb9148-9177-4032-b04a-e026a80bdca7" class=""><strong>@Window: </strong>Καταλαβαίνει. Κάνει ήχους και νοήματα. Παντομίμα όπως και στα προηγούμενα. </p><p id="a320ec44-b7b1-4aea-b14d-b78a32cdbe93" class=""><strong>@Umbrella: </strong>Ομοίως.</p><p id="bfe1cdd9-06cd-464a-8897-9002054f0c28" class=""><strong>@Cat: </strong>Ομοίως.</p><p id="c8b16a73-39a6-4ff7-8ca3-6839023cb6e8" class=""><strong>@Flood: </strong>Ομοίως.</p><p id="9aa40f30-737e-4911-b6d2-b0fe26bdf422" class=""><strong>@Cinderella Intro: </strong>Ομοίως.</p><p id="961c593c-17a9-4f66-ae46-1a7b4fe58f3d" class=""><strong>@Cinderella: </strong>Ομοίως.</p><p id="1cafca27-8842-487b-b09b-a044b060fb3c" class=""><strong>@Sandwich: </strong></p><p id="77ca4cb3-3b70-4d65-a81b-993b9dc951de" class=""><strong>Summing Up: </strong>Καταλαβαίνει. Ζωγραφίζει, βγάζει ήχουν, τραγουδάει και κάνει παντομίμα για να εκφραστή. Λέει ναι ή όχι μόνο. Δεξί χέρι παράλυτο. Φαίνεται να μπορεί να διαβάσει. Δεν μπορεί να σχηματίσει λέξεις και κατ&#x27; επέκταση ούτε προτάσεις. </p><p id="bc8b1df2-c7d0-42c8-88a7-1330871dd188" class="">
</p><h3 id="82cec9f6-9a1b-48be-8b81-41268786514a" class=""><strong>Adler13a [ Broca | AQ: 55.8 | male | age 52 ] </strong></h3><p id="9dccc6ac-ec08-435e-987a-899d98ff0013" class=""><strong>@Speech: </strong>Καταλαβαίνει. Χειρονομία. Δεξί χέρι παράλυτο. Yes, i know, very, good, yes, i don&#x27;t know</p><p id="7eed4ca1-403b-42e6-a011-0a439cd95729" class=""><strong>@Stroke: </strong>Καταλαβαίνει. March, April, two thousand, five με πολλη δυσκολία. Night, damn, ayayay (δυσανασχέτιση), two weeks. Λέει λέξεις αλλά όχι ολοκληρωμένες προτάσεις. Αργός λόγος όπως και στο προηγούμενο. Recovery: Woman, walking (επανάληχη), what, wow, yes, later, speech, yes, arm and leg, trauma. Πολλά νοήματα με το αριστερό χέρι μόνο (δεξί φαίνεται παράλυτο).</p><p id="5b6265d8-15bd-4a84-bace-2a03abaa0b88" class=""><mark class="highlight-red">Note: Φαίνεται και εδώ (λίγο) πως όταν θέλει να μιλήσει για κάποιους μήνες (που υπάρχει μια σειρά, χρειάζεται να αναφερθεί και σε προηγούμενους/επόμενους έτσι ώστε εικάζω μέσω της ακολουθίας/σειράς μηνών να του έρθουν πιο εύκολα στο μυαλό.</mark></p><p id="71f0b15a-6e21-47b6-bacf-4e9cb1e67d1c" class=""><strong>@Important Event: </strong>Καταλαβαίνει. Λέει ονόματα. Twenty two and eighteen, yes. I don&#x27;t know. ex wife. Ex son, oh no, μπερδεύτηκε. </p><p id="fcf8bf39-bba9-4fa9-b116-326d9023c39e" class=""><strong>@Window: </strong>Καταλαβαίνει. Γελάει (κάποιες φορές από αμηχανία, κάποιες φορές από αυτό που βλέπει). boy, is, ball, window, man, what, oh wow (θαυμασμός), i don&#x27;t know. Κάνει νοήματα. </p><p id="4b1a77c8-e513-446c-9f6e-8410bd0a4b2a" class=""><mark class="highlight-red">Note: Λοιπόν παρατηρώ οτι πολλές φορές οταν δεν μπορούν να εκφράσουν με λέξεις αυτό που βλέπουν &quot;υποδύονται&quot; τους ρόλους των ατόμων που δρουν στις ιστορίες των εικόνων. Έτσι, πολλές φορές μπορούν να εκφράσουν ενθουσιασμό/έκπληξη και εν γένει τα συναισθήματα που θα είχαν αν συμμετείχαν στην ιστορία που διαδραματίζεται μέσω των εικόνων. </mark></p><p id="090fca4f-80b2-4471-a125-8a0489ab54e2" class=""><strong>@Umbrella: </strong>Καταλαβαίνει. Woman, said. Δυσκολεύεται. Umbrella, no, yes, bye. Επαναλαμβάνει και αυτός πολλές φορές τις ίδιες λέξεις προκειμένου να εκφράσει το συναίσθημα. Λέει επίσης συνεχώς τις λέξεις i know. boy, bye, and, woman, okay, oh wow. okay. Γελάει, walking, </p><p id="9aa2596f-5e0f-4f93-93f2-154a8e3a8d32" class=""><strong>@Cat: </strong>Καταλαβαίνει. Man and yes, yes, yes. lemon (θέλει να πει ladder). Fair (πάει για target firemen). Firemen. Two firemen. okay. Νευριάζει. </p><p id="30396c78-1cce-4655-a549-69d180421bb0" class=""><mark class="highlight-red">Note: Υπάρχει περίπτωση η επανάληψη των ίδιων λέξεων να συνδέεται με το πλήθος των αντικειμένων; Δλδ Man (βρήκε τη λέξη) and yes, yes, yes (3 objects ίδιου τύπου ???) </mark></p><p id="edb5e6b5-a927-4e4b-8203-ab1ff4654b71" class=""><mark class="highlight-red">Note2: Υπάρχει περίπτωση να προσμετράμε ως κοντινή απάντηση για το ladder, λέξεις οι οποίες ξεκινούν με το ίδιο γράμμα (πχ. lemon, oh no). Λέει lemon, καταλαβαίνει πως δεν είναι η σωστή λέξη. Να δώσουμε δλδ κάποιο notion για το target. </mark></p><p id="1eb18e75-50b4-42d1-a6fe-c113d550e3cd" class=""><strong>@Flood: </strong>Καταλαβαίνει. Woman is trapped and the man is (θελει να πει reaching) woman and, ζορίζεται καο βγάζει ήχους </p><p id="3fd01dd7-063b-45f8-b8d8-32ef0b5238f8" class=""><strong>@Cinderella Intro: </strong>Καταλαβαίνει. Ξέρει το story της cinderella. Wow (οτι το τασκ θα είναι δύσκολο). </p><p id="c0841e3f-9ed9-4988-bf77-121cda3758f0" class=""><strong>@Cinderella: </strong>Woman and two woman and man hello - hello (πάλι σαν acting), I don&#x27;t know. I know. yes. Νοήματα και acting συνεχίζεται με συναίσθημα. Walking. later (έχει επίγνωση χρονικής συνέχειας). Castle. Two woman. Ήχοι, μουσική και παντομίμα με αριστερό χέρι (Δεξί χέρι παράλυτο). Clock. Shoe. Κάνει και τα φιλιά και γελάει. </p><p id="eba5b410-962c-49cc-9fe4-a312801fef02" class=""><strong>@Sandwich: </strong>Καταλαβαίνει. Bread. Κάνει ήχους. Jelly, παντομίμα κινήσεων ιστορίας. Butter. </p><p id="62842c05-da19-4fd0-83d7-7a3b7f038ad9" class=""><strong>Summing Up: </strong>Καταλαβαίνει. Παντομίμα, νοήματα, ήχοι και γενικότερα ένα acting οταν δεν μπορεί να βρει τις λέξεις (μπαίνει στους ρόλους της ιστορίας και κάνει τους ήχους που κάνουν τα αντικείμενα που σχηματίζουν την ιστορία πχ ήχος φιλιών, μουσικής κτλ). Δεξί χέρι παράλυτο. Λέει λέξεις αρκετές. δεν μπορεί να σχηματίσει προτάσεις. </p><h2 id="8d0d6b59-7e7a-46f4-87f6-69120c2f823b" class="">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</h2><p id="6535dad0-83ad-4a8d-bb96-41caaf4d4772" class="">Ο<strong> λόγος</strong> των ασθενών που πάσχουν από αφασία τύπου Broca είναι <strong>αρκετά αργός</strong> και με πολλά προβλήματα. Συγκεκριμένα, μπορούν να σχηματίσουν <strong>με δυσκολία κάποιες λέξεις</strong>, όχι όμως ολοκληρωμένες προτάσεις. Επιπλέον, είναι εμφανές οτι <strong>κατανοούν το περιεχόμενο</strong> των εργασιών/ερωτήσεων που τους ζητούνται να εκτελέσουν. Κάνουν παντομίμα και <strong>χειρονομίες </strong>όταν δεν μπορούν να επικοινωνήσουν τις λέξεις που αναζητούν μόνο με το αριστερό χέρι καθώς το <strong>δεξί παρατηρείται πως δεν το μπορούν να το κινήσουν</strong>. Επίσης, <strong>παράγουν</strong> <strong>πολλές φορές ήχους</strong> που θα παραγόντουσαν στην πραγματικότητα αν εκτυλισσόταν οπτικο-ακουστικά η ιστορία (πχ. ήχος από φιλί (cinderella), άλειμμα peanut butter, άλογα στην άμαξα κοκ). Αυτό συναντάται στις περιπτώσεις ασθενών που κάνουν <strong>acting την ιστορία που δυσκολεύονται να περιγράψουν. </strong>Επίσης, <strong>επαναλαμβάνουν</strong> πολλές φορές τις ίδιες λέξεις (yes/no/i know/ i don&#x27;t know) και<strong> εν γένει τις χρησιμοποιούν και όταν θέλουν να περιγράψουν άλλες λέξεις</strong> (διάφορες οι εικασίες και τα σχόλια που συναντώνται συνήθως με κόκκινο στα παραπάνω κείμενα) πχ. man, <strong>yes, yes, yes θέλοντας να δείξει και άλλα subjects/objects</strong> της ιστορίας (ίδιου τύπου ή διαφορετικά;). </p><p id="f585521f-c2cb-43da-a546-0652faedcbb1" class="">
</p><p id="c1337948-bd83-4cb6-b285-5b68aa8f60d0" class="">[edit]: μαλλον λανθασμένος ισχυρισμός</p><p id="c97bb78b-3bfb-4549-bfad-d7fa74aacdf8" class=""><mark class="highlight-red">SOSOSOS Note: Το WAB-AQ υπολογίζεται και από άλλα τεστ εκτός του Narrative??? An nai,  Φυσικά, στο συνολικό AQ score συνυπολογίζεται και το score του narrative. Το πρόβλημα δημιουργείται όταν ακόμα και να έχουμε δύο παρόμοια narratives από δύο διαφορετικούς ομιλητές, αυτοί μπορεί να έχουν λάβει διαφορετικά AQ scores (γιατί για παράδειγμα μπορεί να έχουν ένα εντελώς διαφορετικό narrative). Παρόλα αυτά, εμείς στην εκπαίδευση αυτούς τους δύο ομιλητές θα τους θεωρήσουμε πάρα πολύ &quot;κοντινούς&quot;/&quot;παρόμοιους&quot; γιατί ακριβώς &quot;παρόμοια&quot; θα είναι και τα δεδομένα εισόδου μας. Για αυτό δύο προτάσεις: </mark></p><p id="9cf7b5de-ebe2-4afd-b257-969018d60934" class=""><mark class="highlight-red">α) ή βρίσκουμε τα υπόλοιπα tasks με τι εγκεφαλική βλάβη συνδέονται και το βάζουμε ως condition στο μοντέλο δεδομένου οτι αυτή η πληροφορία θα μας δίνεται </mark></p><p id="15b372c2-61d6-49aa-aa71-8b14435a26c1" class=""><mark class="highlight-red">β) ή χρησιμοποιύμε άλλο AQ score το οποίο δίνεται στο excel του AphasiaBank με τα results και αφορά μόνο το narrative </mark></p><h2 id="629d03c6-ba38-4188-8b2b-42c1aa1aaa03" class="">Σημειώσεις από το Teaching Section - Students του AphasiaBank - Δεν με αφήνει να μπω !</h2><p id="9dd4fc5f-76cf-4df1-a2db-34b506c82f28" class="">
</p><h1 id="c51eff8a-79bd-4480-a0ee-5168933bea8f" class="">Wernicke</h1><h2 id="d23af953-30e1-4281-a728-5eef6be7b0d7" class="">Εισαγωγή</h2><p id="574b5adc-5891-480d-abc2-447c71d091cc" class="">Ασθενείς με αυτό τον τύπο αφασίας αναμένεται να εκφέρουν συνεχή λόγο ο οποίος να μην βγάζει κανένα απολύτως νόημα. Συναντάται συχνά και με τους όρους fluent aphasia/repetitive aphasia, &quot;word salad&quot;. </p><p id="b3520ffe-a10e-4b07-a21c-1d21d8a2ee50" class="">
</p><h2 id="04a23306-ac4c-4b3e-808e-d085b4e59bea" class="">Προσωπικές Σημειώσεις</h2><p id="8cee48f2-254e-4d10-a214-52eae41c891e" class="">
</p><h3 id="da722cb6-0562-447f-ac95-699b09adf248" class=""><strong>ACWT10a [ Wernicke | AQ: 57.4 | female | age 48 ] - Πρέπει να έχουν λάθος τα scripts !!!!!!!</strong></h3><p id="8e8f7d9a-b740-45e3-89d9-e3889cc0c9cf" class=""><strong>@Speech: </strong></p><p id="d758bca2-f9c0-4c7b-8b31-85b12aa08bc2" class=""><strong>@Stroke: </strong></p><p id="162be6f2-2ffb-4f0e-8eab-ebf3fd7dba44" class=""><strong>@Important Event: </strong></p><p id="a88f6a86-274e-447b-8aee-a183a4c62d0d" class=""><strong>@Window: </strong></p><p id="3c712d2d-70d0-47f9-9b9f-c6ceddfe0291" class=""><strong>@Umbrella: </strong></p><p id="0164ec54-9259-47c6-8a1b-b0d2e1d89d77" class=""><strong>@Cat: </strong></p><p id="ca5c3387-07bd-498b-9d44-51d71c2bb2b6" class=""><strong>@Flood: </strong></p><p id="180e09af-dae5-4526-a91b-f77de517e04d" class=""><strong>@Cinderella Intro: </strong></p><p id="c2153d93-3a2b-4b43-a11b-b9bc489e3ce5" class=""><strong>@Cinderella: </strong></p><p id="d9801c5f-c178-4fe4-a420-18c0be3ed931" class=""><strong>@Sandwich: </strong></p><p id="cd567559-951b-462f-bdbc-02f40e75dd4c" class=""><strong>Summing Up: </strong></p><p id="d4f82759-05f8-4484-89ce-477b7be5d92c" class="">
</p><h3 id="21c8ee0b-afc9-4058-9dea-9fc8aa5668c2" class=""><strong>ACWT11a [ Wernicke | AQ: 48.9 | male | age 61 ] </strong></h3><p id="01c5b94c-e672-4045-88ac-5b2a6754bdfc" class=""><strong>@Speech: </strong>Χρησιμοποιεί λέξεις. (περιέργως) φαίνεται να καταλαβαίνει τι του ζητείται. Μπορεί να δημιουργήσεις προτάσεις και χρησιμοποιεί και ρήματα. </p><p id="d4db714d-2c5d-43ea-bf56-7c3fcf18c5e1" class=""><strong>@Stroke: </strong>Καταλαβαίνει τι του ζητείται. Ο λόγος του είναι συνεχής. Φαίνεται πως έχει κάποια μορφής παραπληγία στο αριστερό χέρι. Παρόλα αυτά κατάφερε να το σηκώσει μολονότι το λέει και ο ίδιος οτι δεν μπορεί να κάνει πράγματα που έκανε (e.g. drums). Παρατηρώ, επίσης, σε σύγκριση με τους ασθενείς τύπου broca πως ο λόγος είναι πολύ πιο γρήγορος. </p><p id="5ef0d577-031a-4240-bd4f-6ba103672099" class=""><mark class="highlight-red">Νote: Μιλάει πολύ και μολονότι σε κάποια σημεία φαίνεται να ξεφεύγει έχω την εντύπωση πως οι προτάσεις μεταξύ τους έχουν κάποια συσχέτιση. Σαν να μπλέκονται &quot;μονοπάτια&quot; σκέψεων, ίσως; </mark></p><p id="0cd184bf-ea50-458c-a909-8fb04820dfdf" class=""><mark class="highlight-red">Σίγουρα προς το παρόν ξεχωρίζεται από τους broca, με την ταχύτητα του λόγου, το πλήθος διαφορετικών λέξεων και τον σχηματισμό προτάσεων. </mark></p><p id="4573ce96-61b7-45ff-b868-b3b23251c86a" class=""><strong>@Important Event: </strong>Φαίνεται οτι καταλαβαίνει και θέλει να εξιστορήσει μια ιστορία από τα παιδικά του χρόνια. </p><p id="ef3f64dc-63fb-4bd5-9124-a0e617a00fc2" class=""><strong>@Window: </strong>Ρήμα, ball, baseball, throw, catch, dad <mark class="highlight-red">(ωπα, του έσκασε ιστορία από τα παιδικά του χρόνια με τον μπαμπά του και το ανέφερε, μου φαίνεται όπως και στο note παραπάνω οτι δεν λένε οτι να ναι αλλά μπερδεύονται συμβάντα στο κεφάλι και τα αναφέρουν ταυτόχρονα). </mark>Παρόλα αυτά δεν είπε πολλά πράγματα για την ιστορία. Απλώς ξέφυγε. </p><p id="616b684e-cf15-47d8-b46c-811612b94820" class=""><strong>@Umbrella: </strong>I loved it was raining in Midland. Παίρνει πάλι incentive και απλά αλλάζει θέμα. Χρησιμοποιεί και δείχνει αλλά το δεξί χέρι δεν χρησιμοποιείται. Κάτι φαίνεται να αλλάζει με το baby, boy, girl. Πάλι έχω το intuition οτι κοντινές λέξεις τις ψιλοαλλάζει κάπως. </p><p id="9b952528-3aa3-444e-ba51-bb48ff69420f" class=""><strong>@Cat: </strong>Λέει αρκετά πράγματα. Κάπου από το truck πήγε στο train (ξεκινούν με &quot;tr&quot; και είναι και τα δύο μεταφορικά μέσα). </p><p id="103b32aa-8e57-4550-a34e-3387909ea23c" class=""><strong>@Cinderella Intro: </strong>Λέει πάλι τα δικά του για έναν καλλιτέχνη που δούλευε και δούλευσε και τον βοηθούσαν οι άνθρωποι και έβγαλε ταινία (είδε οπισθόφυλλο; δεν ξερω). Και του ζητάει η εξετάστρια αρκετές φορές να διαβάσει πρώτα την ιστορία. Νομίζει οτι ολοκήρωσε το task, φαίνεται λίγο μπερδεμένος. </p><p id="d4a628b0-d317-484e-9b82-940f214ba491" class=""><strong>@Cinderella: </strong>Μολονότι δυσκολεύεται μιλάει πολλή ώρα και καταφέρνει κάπως να πεί την ιστορία. Δλδ φαίνεται πως μπορεί να πει τη ιστορία. </p><p id="2b684144-c3cd-4d71-937c-c4f29b35ec1c" class=""><strong>@Sandwich: </strong>Δυσκολεύεται και δεν μπορεί να εκτελέσει το task χωρίς τις εικόνες/ερεθίσματα. Μόλις λάβει τα κατάλληλα ερεθίσματα καθυστερεί ακόμα λίγο αλλά εν τέλει καταφέρνει να ολοκληρώσει το task. </p><p id="f47e7c66-0c2f-4dfb-aa83-5f0109a5a3b6" class="">
</p><p id="351cb174-4f8e-4f2d-bda8-b147ef74a032" class=""><strong>Summing Up: </strong>Γρήγορος, συνεχής λόγος. Χρησιμοποιεί νοήματα και το δεξί του χέρι φαίνεται να έχει κάποια παραπληγία. Παρεκτρέπονται οι σκέψεις του σχετικά εύκολα και αργεί να κατανοήσει κάποια τασκς (αλλά όχι πάντα). Μόλις τα κατανοήσει παράγει προτάσεις (και με ρήματα) και υπάρχει ποικιλία λέξεων σε σύγκριση με ασθενείς τύπου broca. </p><p id="2c504943-9a3b-464f-b4b2-a4cc5a664386" class="">
</p><h3 id="7e1ff665-f116-49e9-b379-c71e54f56bf5" class=""><strong>Adler06a [ Wernicke | AQ: 28.2 | male | age 70 ] </strong></h3><p id="0ed84568-1d5b-48be-9b66-5e4ac9bfa3a0" class=""><strong>@Speech: </strong>Φαίνεται να κατανοεί μολονότι λέει μια λέξη begger αντί για better και δείχνει επίσης με τα δύο του χέρια (δεν υπάρχει παράλυση) το διάστημα (υπονοώντας bigger). </p><p id="89107bb0-8cb1-46b4-9ae2-9dbe8cd727b7" class=""><strong>@Stroke: </strong>Δυσκολεύεται πολύ να μιλήσει. Παρόλα αυτά φαίνεται να κατανοεί τι του ζητείται να πει. Ο λόγος του φαίνεται να μην βγάζει νόημα. Επίσης, είναι συνεχής μολονότι δυσνόητος/ακατανόητος. </p><p id="31e99b23-86e7-4b69-a774-a356de0282a7" class=""><strong>@Important Event: </strong>Καταλαβαίνει τι του ζητείται. Ο λόγος του είναι δυσνόητος αλλά συνεχείς. Επίσης, κατανοεί οτι δεν μπορεί να αντεπεξέλθει και αγανακτεί.</p><p id="ee0c82bd-2c40-475c-8bd7-dea8bea3dfda" class=""><strong>@Window: </strong><mark class="highlight-red">Κατανοεί. Δυσνόητος μα συνεχής λόγος χωρίς παύσεις</mark>. </p><p id="cca95cd5-ecbe-42ae-ba99-5bffd92e0583" class=""><strong>@Umbrella: </strong>Ομοίως. </p><p id="0066521f-d5f4-4034-9e7c-f6640332b962" class=""><strong>@Cat: </strong>Ομοίως.</p><p id="20a60cc7-4ca9-4855-9df3-7a9d856a3e75" class=""><strong>@Flood: </strong>Ομοίως</p><p id="cb7b946c-035a-45e2-acf2-16f91a82ab12" class=""><strong>@Cinderella Intro: - </strong></p><p id="176abff6-0951-4404-9b44-f9519881355f" class=""><strong>@Cinderella: </strong>Φαίνεται να έχει πρόβλημα στην επανάληψη των λέξεων. Ρωτάει πως είναι το όνομα, του απαντάει cinderella και λέει σακάιντα. Μιλάει αλλά φαίνεται σαν να λέει μια άλλη ιστορία ή να έχει μπερδευτεί. Αποκλίνει λοιπόν πάρα πολύ από το λεξιλόγιο που θα χρησιμοποιούσε κανείς για το story της cinderella. </p><p id="765f13b2-c590-476d-a2ea-a24603a3db58" class=""><strong>@Sandwich: </strong>Κάνει κινήσεις και νοήματα. Βέβαια η εξετάστρια δεν του δίνει εικόνες όπως στους προηγούμενους. </p><p id="21208c2f-1600-4e3a-b84a-06049acc5159" class=""><strong>Summing Up: </strong>Συνεχής λόγος, ακατάληπτος, δυσνότητος χωρίς παύσεις. Υπάρχουν κάποια νοήματα και χειρονομίες και με τα δύο χέρια, δεν φαίνεται να υπάρχει κάποια παράλυση. Έχω την εντύπωση πως καταλαβαίνει τι του ζητείται. Στην ιστορία της cinderella παρόλα αυτά φαίνεται να έχει ξεφύγει. </p><p id="ad4a5850-c461-44bc-bc88-356cbdf02e89" class="">
</p><h3 id="de971270-b536-41c1-8872-b7cfbd2cd78d" class=""><strong>Adler23a [ Wernicke | AQ: 46.8 | male | age 81 ] </strong></h3><p id="ebe864f8-57e7-40d0-ac8c-536657d58356" class=""><strong>@Speech: </strong>Καταλαβαίνει και απαντάει. Φαίνεται πως οι προτάσεις του δεν είναι καλές και πρέπει να έχει και κάποιο πρόβλημα με την άρθρωσή του. Γρήγορος λόγος. </p><p id="482eafa0-1506-4db3-9c36-3149c89726b8" class=""><strong>@Stroke: </strong>Ο άνθρωπος καταλαβαίνει, θυμάται και παθαίνει σε κάποια στιγμή έναν πανικό για ό,τι του συνέβη. Οι προτάσεις του δεν βγάζουν εύκολα νόημα αλλά φαίνεται να αναβιώνει την κατάσταση και το άγχος (αντιλαμβάνεται δλδ την ερώτηση πλήρως). Για αποκατάση λέει πως διαβάζει αλλά παλιότερα ήταν πολύ καλύτερος και πιο γρήγορος. Ο λόγος συνεχίζει να είναι πολύ γρήγορος. </p><p id="284e8ca0-3389-49d6-9d79-67be4c299676" class=""><strong>@Important Event: </strong>Δυσκολεύεται. Από ό,τι φαίνεται πιάνει μια λέξη (trip) από την εξετάστρια και μετά αναζητά την ίδια λέξη για να πει μια ιστορία γιατί δεν την συγκράτησε. </p><p id="bfd9c59e-e077-4703-b431-66d0245fd834" class=""><strong>@Window: </strong>Δεν χρησιμοποιεί πολλές λέξεις. Δεν περιγράφει τις εικόνες αλλά αντιθέτως αναπαράγει την ιστορία σαν να κάνει acting της σκηνής.</p><p id="2ce321d4-4f87-48b4-9cff-438a628f72f9" class=""><strong>@Umbrella: </strong>Ομοίως. Acting της σκηνής. </p><p id="5b5e7a30-638f-414a-be87-781db885ebd6" class=""><strong>@Cat: </strong>Ομοίως. </p><p id="572c7a6e-d79c-4dc3-8b9b-b0c55fa2d3ed" class=""><strong>@Flood: </strong>Ομοίως. </p><p id="b065ff36-c9a5-483c-93b6-9187f857ecb9" class=""><strong>@Cinderella Intro: -</strong></p><p id="7baefd56-2fc1-4042-b61d-89e8727daecc" class=""><strong>@Cinderella: </strong>Ομοίως. </p><p id="45c74ad4-4fae-48d3-b8f6-005b5e238f6a" class=""><strong>@Sandwich: </strong>Κινεί δεξί χέρι αν και φαίνεται να έχει κάποιο θέμα στα δάχτυλα. Δέχεται τα ερεθίσματα, παρόλα αυτά ξεφεύγει από το task που του ζητείται. Λέει τις λέξεις που θέλουμε αλλά μιλάει για το τι του αρέσει και πως έκανε όταν έφαγε ενα peanut butter sandwich. </p><p id="26607d68-9d8a-44ed-8569-024bd05a3087" class=""><strong>Summing Up: </strong>Συνεχής λόγος. Σε κάποιες περιστάσεις καταλαβαίνει τα tasks, σε κάποιες άλλες όχι. Πολλές φορές δεν περιγράφει αυτό που του ζητείται αλλά είναι σαν να σκηνοθετεί τη στιγμή και να παίρνει τους ρόλους των &quot;αντικειμένων&quot;/&quot;οντοτήτων&quot; των εικόνων. </p><h2 id="20b02f32-8474-452b-8b00-96ad3783e549" class="">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</h2><p id="8a0a5405-7510-42f9-9b0d-0ec37831e188" class="">Συνεχείς λόγος χωρίς ιδιαίτερα πολλές παύσεις. Χρησιμοποιούν αρκετές λέξεις. Κάποιες φορές κατανοούν τα tasks, κάποιες άλλες όχι. Φαίνεται να μιλούν και να ξεφεύγουν από αυτό που τους ζητείται με το που τους δοθεί κάποιο ερέθισμα. Κάποιοι κάνουν acting αντί να περιγράψουν με λέξεις τις εικόνες. </p><h2 id="14581265-d5d5-48a5-8de4-9a86f02bc320" class="">Σημειώσεις από το Teaching Section - Students του AphasiaBank - Δεν με αφήνει να μπω !</h2><p id="451abfa9-c407-4617-8563-a8b46ff8ed4a" class="">
</p><h1 id="6351bc58-0dd9-4856-b499-be36018c5b23" class="">TransMotor </h1><h2 id="d0168527-2c9f-4410-805a-95002e4d3ded" class="">Εισαγωγή </h2><h2 id="7aef74d4-67aa-4bd4-9ec6-8f4d58a1f12f" class="">Προσωπικές Σημειώσεις</h2><p id="1d1b504b-3b58-4ac6-a0cb-ba6fbdb224eb" class="">
</p><h3 id="5886770b-8e6b-434c-af52-183cb8c91bb5" class="">ACWT02a [ TransMotor | <strong>AQ: 74.6 | female | age 53 ] </strong></h3><p id="d668737a-0cb4-40f0-ab14-ee14abc09fd5" class="">
</p><p id="d7108778-a278-4043-9cad-5fed117011d0" class=""><strong>@Speech: </strong>Λέει κάποιες λέξεις χωρίς ιδιαίτερη καθυστέρηση. </p><p id="80955b80-771b-4c3e-b8f8-bc3f4c91cb1d" class=""><strong>@Stroke: </strong>Καταλαβαίνει. Σχηματίζει προτάσεις. Φαίνεται να υπάρχει κάποιο πρόβλημα με την άρθρωσή της. Χρησιμοποιεί το ένα χέρι για κινήσεις. Το δεξί ακόμα δεν το έχει μετακινήσει καθόλου. Οι λέξεις που λέει σχηματίζονται αργά αργά. Ο λόγος άρχισε να γίνεται ακαταληπτος. Φαίνεται σαν να μην μπορεί να εκφέρει κάποιες λέξεις. Λέει force αντί για voice. </p><p id="2c31afb4-fe76-40d9-a763-f7a03c525d06" class=""><strong>@Important Event: </strong>Λέει μια ιστορία για την οικογένεια της. Καταλαβαίνει. Χρησιμοποιεί πολλές λέξεις αλλά ο λόγος είναι slurred. </p><p id="b05eba5f-5be8-411c-b102-8cb86e4c3283" class=""><strong>@Window: </strong>Λέει κάποιες λέξεις, αργά και σταθερά με slurred λέξεις. Father (?). </p><p id="963b0b51-31ce-49e2-bb6f-3f66e7ab0ebd" class=""><strong>@Umbrella: </strong>Ομοίως. </p><p id="8d958336-5ed0-4147-90e8-627cc35920c4" class=""><strong>@Cat: </strong>Ομοίως. </p><p id="ca4d9865-e125-4eec-82a5-43ece207590d" class=""><strong>@Cinderella Intro: </strong></p><p id="558ef17d-50a3-4023-989a-809b5dc3ca3f" class=""><strong>@Cinderella: </strong>Ξέρει την ιστορία και την αφηγείται με αργό ρυθμό. Κάποιες λέξεις δυσκολεύεται πολύ να τις προφέρει. Δλδ μπορεί να έχει ποικιλομορφία αλλά δεν μπορεί να πει πλήρως o&#x27;clock (οπότε ίσως και το ASR μας να το αγνοήσει). </p><p id="569f9666-21e1-4af6-9681-3df8effbe70d" class=""><strong>@Sandwich: </strong>Ομοίως </p><p id="a4bef568-f937-46c7-afb6-f224cd45f18e" class=""><strong>Summing Up: </strong>Καταλαβαίνει. Το δεξί της χέρι φαίνεται να έχει πρόβλημα. Χρησιμοποιεί χειρονομίες. Ο λόγος είναι σχετικά άργος γιατί φαίνεται να δυσκολεύεται να εκφέρει τις λέξεις όπως πρέπει. Παρόλα αυτά, κατανοεί τα tasks και παράγει αρκετές λέξεις οι οποίες φαίνεται να είναι εντός περιεχομένου της ιστορίας που καλείται σε κάθε συνθήκη να περιγράψει. <strong> </strong></p><p id="c63c9099-6bc3-4d13-9d2d-fb2afe193d94" class="">
</p><h3 id="1cef5fb7-b5ab-4370-9595-01ccf6533eb9" class="">ACWT03a [ TransMotor | <strong>AQ: 69.3 | male | age 68] </strong></h3><p id="037a5088-4632-419e-acf0-496472b5abed" class=""><strong>@Speech: </strong>Λέει μόνο yes και no.</p><p id="0f6285a6-2b77-4d77-95ac-cf30818c4b10" class=""><strong>@Stroke: </strong>Yes, No πάλι μόνο.</p><p id="50223040-9678-47be-8c6a-2cfcca245d46" class=""><strong>@Important Event: </strong>Ομοίως </p><p id="0edf1f03-935b-43b3-9e2a-9dc7f43d78bf" class=""><strong>@Window: </strong>Soccer ball, basketball, cat με δυσκολία στην εκφορά. Καμία άλλη λέξη και καμία πρόταση. Δεξί χέρι φαίνεται να μην το χρησιμοποιεί όταν δείχνει. </p><p id="ceb01669-4436-4471-ba6e-07deebfed154" class=""><strong>@Umbrella: </strong>Βοηθά η εξετάστρια. It rained, got wet, he went home, </p><p id="e9774c66-0dd6-4df6-b8a7-f507305c6980" class=""><strong>@Cat: </strong>man&#x27;s up a tree (είναι γάτα στο δεντρο), ladder&#x27;s there, the dog is there, the girl, the truck/tricycle is there. Χρησιμοποιεί λοιπόν ρήμα, the girl went to him, the dog</p><p id="c5922e39-5b93-4a19-94ac-5afbef05fa13" class=""><strong>@Cinderella Intro: </strong>Κάτι λέει για τη γάτα από το προηγούμενο story. βέβαια τώρα το πετυχαίνει, δεν λέει man was up the tree. </p><p id="1f82083a-f9c8-4f68-a712-a5f815a8f23e" class=""><strong>@Cinderella: </strong>Δυσκολεύεται πάρα πολύ. married, yes, no. δεν λέει πολλά.</p><p id="b532d6a2-cab5-4ef7-b597-d7aed7ecc385" class=""><strong>@Sandwich: </strong>peanut butter and bread and jelly, δεν μπορεί</p><p id="09150927-3b7e-4393-9b20-7c3f946848bb" class=""><strong>Summing Up: </strong>Δυσκολεύεται πάρα πολύ. λέει διαρκώς, yes, no ή δεν μιλάει. η εξετάστρια βοηθάει με ερωτήσεις. όταν λέει κάποιες λέξεις ο λόγος του φαίνεται slurred. Δεν μπορώ να κατανοήσω σε καμία περίπτωση τη πολύ μικρή απόκλιση AQ score συγκριτικά με την προηγούμενη ομιλήτρια δεδομένου του narrative. </p><h3 id="31cc86a5-4f78-4eef-b17c-f47fa2f4766d" class="">Adler04a [ TransMotor | <strong>AQ: 72.6 | male | age 75] </strong></h3><p id="23b5ea0a-fef5-4b36-bfef-7c02233fa84a" class=""><strong>@Speech: </strong>Γρήγορος λόγος. Δεν βρίσκει ακριβώς τις λέξεις που θέλει αλλά σχηματίζει ικανοποιητικά προτάσεις/λέξεις που φαίνεται να έχουν κάποια συνοχή. </p><p id="a841ff7c-1923-4f1d-884e-29dbde114b02" class=""><strong>@Stroke: </strong>Κατανοητή. Εξιστορεί το τι συνέβει. Μολονότι υπάρχει συνοχή, συντακτικά φαίνεται να υστερεί (καμία σύγκριση με προηγούμενο ομιλητή-τρομερή διαφορά). Μιλάει αρκετή ώρα. </p><p id="867aec52-2b11-451c-98ee-60171d8be4a4" class=""><strong>@Important Event: </strong>Μιλάει για τον χωρισμό της κτλ, χρησιμοποιεί λέξεις και επικοινωνει το μηνυμα της</p><p id="0b85e577-bce7-4a70-8244-758251f192d9" class=""><strong>@Window: </strong>Χρησιμοποιεί σημαντικές λέξεις για την ιστορία </p><p id="b17d7d31-64f9-49b2-b614-9201ea065709" class=""><strong>@Umbrella: </strong>Εξιστορεί την ιστορία με την ομπρέλα και περιγράφει πλήρως τις εικόνες</p><p id="c61207b3-1c39-4952-a4cb-c3b900257774" class=""><strong>@Cat: </strong>Ομοίως</p><p id="89490c26-2596-4b27-910c-e66cb9d2371c" class=""><strong>@Flood: </strong>Δυσκολεύεται αλλά χρησιμοποιεί κάποιες λέξεις</p><p id="826b08ac-e4b6-4bb1-97ed-ab52ecf4c976" class=""><strong>@Cinderella Intro: </strong>-</p><p id="da3d7208-f9fb-45fc-97d4-fcbfe2cd4eea" class=""><strong>@Cinderella:</strong> Ομοίως χρησιμοποεί αρκετές λέξεις και έχει πλούσιο λεξιλόγιο. </p><p id="3f06426d-5a0b-40cc-a838-6e434d1cb1da" class=""><strong>@Sandwich: </strong>Δεν της δίνει η εξετάστρια κάποιο ερέθισμα όπως στους υπόλοιπους, χρησιμοποιεί τις λέξεις που πρέπει αλλα σύντομη είναι η περιγραφή της (δεν αναφέρει τη λέξη μαχαίρι πχ όπως άλλοι ομιλητές αλλά ίσως σε αυτό φταίει οτι δεν της δόθηκαν οι εικόνες)</p><p id="d2134941-eba7-469a-8f97-d9b02ea0b0ae" class=""><strong>Summing Up: </strong>Κατανοεί τα ζητούμενα και χρησιμοποιεί το λεξιλόγιο που απαιτείται σε κάθε ιστορία. Ο λόγος της είναι αργός αλλά καλύτερος από τους προηγούμενους ομιλητές που πάσχουν από τον ίδιο τύπο αφασίας. Φαίνεται να έχει κάποιες ελλείψεις σε γραμματική και τη σύνδεση προτάσεων (χωρίς να είμαι σίγουρος). </p><h2 id="13c121d5-0178-49a8-8131-f74c52115ba6" class="">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</h2><h2 id="884b2fb2-597a-4524-af0b-33e84c9639f4" class="">Σημειώσεις από το Teaching Section - Students του AphasiaBank - Δεν με αφήνει να μπω !</h2><p id="76039f0e-cfa0-4857-bb9d-16ba43d714a5" class="">
</p><h1 id="2f790d2a-9eda-4c58-a9c9-c4c40d8cb90e" class="">Anomic </h1><h2 id="e299db5d-f94d-4ee1-8760-097f699068d6" class="">Εισαγωγή </h2><p id="08d48422-867d-4d50-9745-58ab93484f98" class="">Anomia: A problem with word finding. Impaired recall of words with no impairment of comprehension or the capacity to repeat the words</p><h2 id="d5036375-f3cb-4c9a-ab89-eceeee92366f" class="">Προσωπικές Σημειώσεις</h2><p id="9c425df3-e89e-4835-bbb3-49ef67504a72" class="">
</p><h3 id="fcf45484-6661-444b-a4df-f52567017268" class="">Adler01a [ Anomic | <strong>AQ: 86.8 | male | age 58 ] </strong></h3><p id="c5acd013-087d-4d5d-92b1-1c2981e6e19f" class="">
</p><p id="96f27891-7645-4b56-a5d1-6f32659b9003" class=""><strong>@Speech: </strong>Αργός λόγος </p><p id="15f2075c-7c3a-4bb6-97fa-97fbee15d54d" class=""><strong>@Stroke: </strong>Κάποιες στιγμές μιλάει σχετικά γρήγορα ενώ κάποιες άλλες αργά και κολλάει σε κάποιες λέξεις. Μιλάει αρκετά. Υπάρχουν πολλά fillers, human unintelligible speech, το οποίο μετατρέπεται σε &quot;νορμάλ&quot; ομιλία. Πλούσιο λεξιλόγιο και αρκετή διάρκεια. Λέει επίσης πως δεν μπορεί να διαβάσει. Μου βγάζει νόημα το υψηλό ποσοστό του (διαρκεί το stroke περιπου 10+ λεπτά που σε άλλους ασθενείς τόσο διαρκούσε όλη η χορήγηση)</p><p id="45b177b0-b6bf-4fcf-8acf-af4613d1ec17" class=""><strong>@Important Event: </strong>Κολλάει σε πολλές λέξεις. Φαίνεται πως στις προτάσεις υπάρχει structure και χρησιμοποιεί και ποικιλία χρόνων. </p><p id="6b02f0e4-7d25-42ae-aea2-8a0670de4205" class=""><strong>@Window: </strong>Περιγράφει πιο συνοπτικά την ιστορία. Επίσης, πριν την περιγραφή μιλάει στην εξετάστρια ή σε κάποια άλλη (αυτό θα είναι θόρυβος στα δεδομένα μας). Επίσης, αντί για τη λέξη kick λέει tick (μήπως να χρησιμοποιούσαμε κάποιο edit distance).</p><p id="f2aa10f2-8b73-4339-a605-65cf6873f081" class=""><strong>@Umbrella: </strong>Ομοίως </p><p id="5fca1c86-e086-43df-b36d-fe47db4a834d" class=""><strong>@Cat: </strong>Ομοίως </p><p id="d3c45dc5-773d-498e-9e37-b386f6a9282d" class=""><strong>@Flood: </strong>Ομοίως </p><p id="e937e32b-bede-4f7f-a56b-5ff468498430" class=""><strong>@Cinderella Intro: - </strong></p><p id="2314d31e-5238-4b77-a1f2-eedbfd9b2c72" class=""><strong>@Cinderella: </strong>Ομοίως </p><p id="2dbeedd1-ea1b-4864-b1c4-92ce5c780ffd" class=""><strong>@Sandwich: </strong>Δεν του δίνεται κάποιο ερέθισμα (εικόνα). Λέει τις λέξεις που χρειάζονται με αργή ομιλία. </p><p id="a805a761-fac5-40b1-9bdf-2f14cd739a87" class=""><strong>Summing Up: </strong>Πολλά φιλλερς σε λέξεις που δεν μπορεί να πει αλλά πολλές προτάσεις τις λέει πολύ γρήγορα. Οπότε υπάρχουν κάποια fluctuations ως προς τον ρυθμό του λόγου του. Στο stroke story μιλάει πάρα πολύ ώρα ενώ στα stories που περιέχουν εικόνες, οι περιγραφές του είναι πιο συνοπτικές.  </p><p id="a580237c-b1ce-4fc7-a671-c9953066f593" class="">
</p><h3 id="4e86f86d-7ced-44d8-8c4a-b79d1fb89bda" class="">Adler02a [ Anomic | xx<strong> | xx | xx ]  → SOS εχουν κάνει λάθος, έχουν άλλα transcripts </strong></h3><p id="85cdee1c-6447-458d-854b-3622e6789bf9" class="">
</p><h3 id="3a82304b-a258-4aa1-9386-a0dcc9925ab6" class="">Adler08a [ Anomic | <strong>AQ: 78.5 | male | age 56 ] </strong></h3><p id="65582d49-defc-4eed-b375-afecef0d387d" class=""><strong>@Speech: </strong>Αναφέρει πως όταν μιλάει αργά, καταλαβαίνει καλύτερα. </p><p id="7fbbdb7b-99e0-4de7-9bbb-d7dab63389cf" class=""><strong>@Stroke: </strong>Σχηματίζει προτάσεις. Δυσκολεύεται σε κάποιες λέξεις. Πιο σύνθετες προτάσεις σε σύγκριση με άλλους ασθενείς άλλων τύπων αφασίας αλλά μου κάνει εντύπωση που δεν χρησιμοποιεί το σωστό υποκείμενο (e.g. It said &quot;yes no yes no&quot;, θέλοντας να πει πως στις αρχές που είχε το εγκεφαλικό χρησιμοποιούσε μόνο yes/no). Νομίζω δεν κατάλαβε μετά την ερώτηση &quot;τι έκανε για να καλυτερεύσει&quot; και απαντάει σχετικά με το &quot;τι μπορεί να κάνει&quot;.</p><p id="22c3f6cd-d8bc-4afe-9d27-eefce66d3e4d" class=""><strong>@Important Event: </strong>Σχηματίζει προτάσεις και λέει κάποια γεγονότα.</p><p id="fe3e6a33-5874-4664-9885-de4a85861aef" class=""><strong>@Window: </strong>Δεν χρησιμοποιεί πολλές λέξεις για την περιγραφή. Κάτι γίνεται με τους χρόνους των ρημάτων;</p><p id="66607790-fa29-4c6c-8364-29da7995891b" class=""><strong>@Umbrella: </strong>Νομίζω περιγράφει συνοπτικά την ιστορία.Υπάρχουν fillers και κάπως χαλάει η σύνταξη των προτάσεων. </p><p id="f85cc2b9-572b-432e-ba98-d20a7923223a" class=""><strong>@Cat: </strong>Νομίζω μπερδεύεται κάπως, χρησιμοποιεί πρόσωπα για την εξιστόρηση της εικόνας (I, he). Δεν χρησιμοποιεί καθόλου λέξεις όπως firemen, fire truck κλπ  </p><p id="44803161-b6fd-4d8c-a878-3e2ab71e6c26" class=""><strong>@Flood: </strong>&quot;it&#x27;s his professionally&quot; θελοντας να πει πως είναι professional, ή οτι αυτό είναι το profession που έχει. Δυσκολέυεται σε κάποιες λέξεις. </p><p id="3791653d-1919-4c97-b9f1-df8467a7f6f1" class=""><strong>@Cinderella Intro: - </strong></p><p id="53e1a41d-a509-4422-a6ea-2cd1412be789" class=""><strong>@Cinderella: </strong>Δυσκολεύεται να βρει τις απαραίτητες λέξεις</p><p id="42939a6e-6de5-43e8-8ee3-ccaaf6a6ced7" class=""><strong>@Sandwich: </strong>Δυσκολεύεται να πει peanut butter. Εμείς στα transcripts θα συναντήσουμε τις λέξεις ball, peter, pitα. Συνήθως, όταν δυσκολεύονται να εκφέρουν μια λέξη νομίζω σχηματίζουν λέξεις οι οποίες φωνολογικά είναι πολύ κοντινές οπότε ίσως <mark class="highlight-red">θα μπορούσαμε να υπολογίσουμε το distance που έχουν από κάποια target words</mark>. </p><p id="2eaacc74-f0ad-45ac-9d00-1e8ea5b0b7bd" class=""><strong>Summing Up: </strong>Υπάρχουν fillers. Δυσκολεύεται να βρει κάποιες λέξεις και αυτό είναι πιο εμφανές στις ιστορίες/εικόνες που απαιτούν την εύρεση συγκεκριμένων λέξεων στόχων. Νομίζω σε κάποια σημεία μολονότι οι προτάσεις είναι κάπως σύνθετες γραμματικά έμφανίζονται κάποια σφάλματα. Η περίπτωσή του είναι εμφανώς χειρότερη από αυτή του προηγούμενου ομιλητή. </p><h2 id="787260a4-5319-49af-8fc3-ebae632b4b42" class="">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</h2><p id="4a94cbd7-1c7b-4432-8be6-b41ad45a8c0d" class="">Ο ρυθμός του λόγου των ομιλητών αυτών δεν είναι σταθερός αλλά τουναντίον παρατηρούνται fluctuations τα οποία οφείλονται κυρίως στην αδυναμία τους να εκφέρουν κάποιες συγκεκριμένες λέξεις. Δυσκολεύονται συγκεκριμένα να κάνουν recall. Επίσης, στην προσπάθειά τους να της πουν, εκφέρουν λέξεις οι οποίες είναι σχετικά κοντά φωνολογικά, οπότε ίσως να είχε κάποιο νόημα να υπολογίζαμε ένα distance από αυτές τις λέξεις και τις target word λέξεις το οποίο να αυξάνει έναν μετρητή αν ικανοποιείται κάποιο manually επιλεγμένο threshold. </p><h1 id="4888fd41-08fc-4fe2-bac6-c55ab61cdd94" class="">Conduction</h1><h2 id="ba413b08-b358-4e61-b116-1dc0a64a1314" class="">Εισαγωγή </h2><p id="2a2b2b9e-fb23-42aa-8911-0de7e3f0fb81" class="">impairment is in the inability to repeat words or phrases</p><h2 id="1dddfb0a-9cc9-481f-a225-45b15a6c6611" class="">Προσωπικές Σημειώσεις</h2><p id="ebe902dc-89f7-42b2-b568-1bd9daa279f1" class="">
</p><h3 id="f2494332-0a41-46f3-bbdd-da35e452a9e3" class="">ACWT09a [ Conduction | <strong>AQ: 80.1 | female | age 56 ] </strong></h3><p id="f32efd74-e12f-497a-a7ad-b66223424766" class="">
</p><p id="198d71d9-549e-4ac6-a40f-efb85ac9af0d" class=""><strong>@Speech: </strong>Κάποιες λέξεις δεν είναι (εύκολα) κατανοητές. Ο ρυθμός του λόγου φαίνεται να είναι κανονικος. </p><p id="bcb4c4c3-4e69-49e2-98b0-b044dd4a7e83" class=""><strong>@Stroke: </strong>Καταλαβαίνει. Επανάληψη &#x27;is uh&#x27; δεν υπάρχει καθόλου δομή σε κάποια σημεία. Λείπουν ρήματα και κάποιες λέξεις είναι unintelligible. Μιλάει αρκετή ώρα. Ο λόγος της είναι συνεχής. Το δεξί της χέρι δεν το έχει σηκώσει ως τώρα ενώ κάνει χειρονομίες (ίσως να έχει κάποια παραπληγία). </p><p id="97c1274b-64b0-4d4e-8205-544cb5532a61" class=""><strong>@Important Event: </strong>Στην αρίθμηση, κάνει κάτι που έχω συναντήσει και σε ασθενείς άλλου τύπου αφασίας. Αν θέλει να πει έναν αριθμό, κοιτάζει τα χέρια και λέει κι άλλους αριθμούς (προηγούμενους σε σειρά) έως ότου φτάσει στον επιθυμητό αριθμό. </p><p id="43d6805b-793b-4be7-bb7f-90ee28d3933d" class=""><strong>@Window: </strong>Απλές προτάσεις. Χρησιμοποιεί κάποιες λέξεις που απαιτούνται για την ιστορία. Γρήγορος/Κανονικός ρυθμός. </p><p id="1488feb0-6856-4332-b74d-1dc461340a80" class=""><strong>@Umbrella: </strong>Γενικά παραλείπει κάποιες λέξεις οι οποίες μπορεί να αντιθίστανται από χειρονομίες. Για παράδειγμα, mama is &amp;=ges:umbrella umbrella (mama is [gesture=holding, χέρι που είναι κλεισμένο σαν να κρατάει κάτι]). Κι αυτή κάνει acting. </p><p id="882cb71b-014a-4f51-8468-5a8fcacda90c" class=""><strong>@Cat: </strong>Επανάληψη is-uh </p><p id="f3225f6d-1a55-4d60-af82-c0c385db08db" class=""><strong>@Cinderella Intro: -</strong></p><p id="c61f065f-d225-435c-b829-45c19ba0f3a4" class=""><strong>@Cinderella: </strong>Πρόβλημα με τις προτάσεις, πρόβλημα με τα ρήματα. Επανάληψη is-uh. Κάποιες λέξεις δεν προφέρονται σωστά.</p><p id="f3652acf-7783-4803-a49c-7ba26421655c" class=""><strong>@Sandwich: </strong>Χρησιμοποιεί κάποιες από τις λέξεις. Βέβαια στην αρχή δεν είμαι σίγουρος αν κατάλαβε ακριβώς τι της ζητήθηκε. </p><p id="81c03998-6783-4210-8284-c400b21d80db" class=""><strong>Summing Up:</strong> Κανονικός ρυθμός λόγου. Σταματάει σε κάποια σημεία και σε ορισμένες λέξεις ενώ κάποιες άλλες δεν είναι καταναοητές. Χρησιμοποιεί νομίζω λίγα ρήματα και επαναλαμβάνει διαρκώς το &#x27;is uh&#x27;. </p><p id="b762398d-e8fb-4247-ae6f-0dcf3aeb7238" class="">
</p><h3 id="74dd0848-50fe-4a9a-a285-58a36f3d0736" class="">Adler05a [ Conduction | <strong>AQ: 65.5 | female | age 68 ] </strong></h3><p id="9d07119c-75bf-4114-bf56-b4a46edd95e3" class=""><strong>@Speech: </strong>Έχει πρόβλημα στη φωνή το οποίο και αναφέρει. Δυσκολεύεται να πει κάποιες λέξεις αλλά δεν σταματάει σε αυτές. </p><p id="d560d2bf-4b9c-4459-8c12-0a0c83169c5f" class=""><strong>@Stroke: </strong>Καταλαβαίνει. Δυσκολεύεται σε κάποιες λέξεις αλλά και δημιουργεί προτάσεις οι οποίες συχνά έχουν πρόβλημα στη σύνταξη. </p><p id="044d7386-ca1a-4b8b-a54f-cf1860f47451" class=""><strong>@Important Event: </strong>Ομοίως</p><p id="75a9e5ed-3f28-4ddd-aefd-e60e45992ddf" class=""><strong>@Window: </strong>hick [: kick], Δυσκολεύεται και δεν μπορεί να πει αρκετές λέξεις.</p><p id="373a126d-a9dc-41b8-af8f-8964c10a9bd7" class=""><strong>@Umbrella: </strong>Δυσκολεύεται ομοίως.</p><p id="6e1845e1-6379-43aa-9cad-3ba1a9b6b9c2" class=""><strong>@Cat: </strong>ομοίως</p><p id="c246a1ef-6fb0-4374-aaef-39e843359dd0" class=""><strong>@Flood: </strong>ομοίως</p><p id="900cd2b7-a0af-4149-9b1b-3bc6a90a48e3" class=""><strong>@Cinderella Intro: - </strong></p><p id="dacc4d2e-8cda-446f-b5c9-85950094c71c" class=""><strong>@Cinderella: </strong>Καταλαβαίνει και εξιστορεί την ιστορία. Νομίζω επίσης μπερδεύει και κάποιες λέξεις (ίσως ακόμα και να τις αντικαθιστά με άλλες) e.g. animals </p><p id="f91cd753-283b-4743-94d1-49fd26aea695" class=""><strong>@Sandwich: </strong>Πρέπει όντως να υπάρχιε paraphasia γιατί λέει επίσης &quot;on a piece of picture&quot;</p><p id="2259d111-aa2b-4589-a0f2-da2e417c6b9c" class=""><strong>Summing Up: </strong>Έχει ένα εμφανές πρόβλημα στην φωνή. Καταλαβαίνει και εκφέρει συνεχή λόγο, μολονότι πολλές φορές χρησιμοποιεί λέξεις που δεν είναι κατανοητές.<mark class="highlight-red"> Επίσης, υπάρχει μια ένδειξη να υπάρχει paraphasia, να μπερδεύει και να αντικαθιστά λέξεις με άλλες</mark>.</p><h2 id="2a5d649f-2d36-47d3-9ed2-c2f33b57b20a" class="">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</h2><p id="bf396e95-7ef8-4ea4-9caf-d6f371c75db2" class="">Σχετικά συνεχής λόγος και κάποιες λέξεις δεν είναι κατανοητές. Υπάρχουν επαναλήψεις στην πρώτη ασθενή και στην δεύτερη υπάρχει μια υποψία για paraphasia. </p><p id="bf3ba7f0-1e99-4308-9f59-23ca7bb660db" class="">
</p><h1 id="30bdc22d-3fa2-42b4-84ae-0acdbbd9271e" class="">Global Aphasia</h1><h2 id="52bda80b-90f6-4d90-91de-c8bd3780bf37" class="">Εισαγωγή </h2><p id="c4af4058-dc98-4dc3-a1ec-418814ecc457" class="">
</p><p id="f8a2c6c3-0715-4d85-bf9c-74b499d1a8e0" class="">Global Aphasia is the most severe form of aphasia and is applied to patients who can produce few recognizable words and understand little or no spoken language. Persons with Global Aphasia can neither read nor write. Like in other milder forms of aphasia, individuals can have fully preserved intellectual and cognitive capabilities unrelated to language and speech.</p><h2 id="73b880bc-3def-4522-a20f-0ac04c5597f6" class="">Προσωπικές Σημειώσεις</h2><p id="5b8d4de9-f5d0-4a1c-9ff1-688637a82955" class="">
</p><h3 id="d0ac51a1-3ea9-427e-b4a7-425eef2a61d6" class="">tap09a [ Global | <strong>AQ: 20.5 | male | age 70 ] </strong></h3><p id="2457c597-42e0-44fb-b58e-49bf666686e7" class=""><strong>@Speech: </strong>Σχηματίζει κάποιες λέξεις αλλά με πάρα πολύ δυσκολία. Δεν είναι οι περισσότερες καταληπτές. Χρησιμοποιεί και τα δύο χέρια οπότε φαίνεται να μην έχει παραπληγία στο δεξί χέρι όπως κάποιοι ασθενείς που έχω συναντήσει. </p><p id="61cf1a90-9b78-4b9b-a61b-bb6ab9e5e9d5" class=""><strong>@Stroke: </strong>Μετράει κι αυτός με τα δάκτυλα όταν θέλει να αναφερθεί σε κάποιο μήνα ή νούμερο που δεν μπορεί να πει. Φαίνεται να κατανοεί τις ερωτήσεις που το γίνονται ως τώρα. Επαναλαμβάνει τις ίδιες λέξεις (i know) από σύγχυση που δεν μπορεί να το πει. <mark class="highlight-red">Οπότε οι επαναλήψεις τέτοιου τύπου ίσως να μπορούν να συνδεθούν με ένα γενικό frustration και αδυναμία του ασθενούς να εκφραστεί έτσι όπως πραγματικά θα ήθελε. </mark>Χρησιμοποεί ρήματα και σε κάποια σημεία ο ρυθμός εκφοράς είναι κάπως χειρότερος από το κανονικό αλλά οι λέξεις που προφέρει είναι ακατάληπτες.</p><p id="87ea7198-c18b-49d3-99fc-6c273c4ac057" class=""><strong>@Important Event: </strong>Ακατάληπτος λόγος.</p><p id="1801b09e-0884-4522-97ff-15d8b213ae0b" class=""><strong>@Window: </strong>Δείχνει τις εικόνες. Ακατάληπτος λόγος. Κάπως λέει λέξεις με πάρα πολλή δυσκολία (ball, window). Πάρα πολλά jargons</p><p id="7c952c9c-0a73-4a7f-a664-66c016667007" class=""><strong>@Umbrella: </strong>Χρησιμοποιεί λέξεις όπως ball, window από το προηγούμενο ενώ δείχνει τις εικόνες. &quot;I don&#x27;t what the hell I&#x27;m doing&quot; crystal clear. </p><p id="34fa1e72-3be1-4609-bd00-bb7b3ae6b769" class=""><strong>@Cat: </strong>Δυσκολεύεται πολύ. Ακατάληπτος λόγος.</p><p id="7f83a455-ae55-4ecc-92bd-e6e6153f5c43" class=""><strong>@Flood: </strong>Ομοίως. </p><p id="f0d85ca2-8308-49aa-9859-b580c27485df" class=""><strong>@Cinderella Intro: </strong>Ομοίως</p><p id="06aab284-afe9-4e6c-a2af-bf1f8a456fda" class=""><strong>@Sandwich:  - </strong></p><p id="8cfdcac0-f768-4e06-8314-41ebb0a1c76b" class=""><strong>@Sandwich_Picture: </strong>Χειρονομίες</p><p id="5d2db327-4e59-424b-9093-b866a4eb6160" class=""><strong>Summing Up: </strong>Δυσκολέυεται πολύ. Ακατάληπτος λόγος γεμάτος από jargons. </p><p id="19a0c619-fa85-48b1-998e-802932c05b97" class="">
</p><h2 id="7ad86873-9be9-4add-8e92-f631ea1a95c9" class="">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</h2><p id="f13fbce7-15a7-45d5-a1fd-17113b615d70" class="">Αρκετές δυσκολίες στο λόγο, ακατάληπτες λέξεις. </p><h1 id="5bb54e40-b1e0-4c2c-8210-e5ea5c100508" class="">TransSensory </h1><h2 id="aabc7e7e-2d7f-49df-8ad2-6aba2dfa4f6e" class="">Εισαγωγή </h2><p id="7ebc87ab-b0d3-469d-8ea2-ab1ec1785af4" class="">Transcortical Sensory Aphasia (TSA) has a lot in common with Wernicke’s aphasia. People with TSA produce connected, flowing speech. However, that speech is likely to lack meaning due to word errors and invented words.</p><p id="5d77a984-8676-453b-8282-8d7ed7619fb6" class="">TSA is less common than other types of aphasia, including the similar Wernicke’s aphasia. TSA is similar to Wernicke’s aphasia because TSA is due to damage in the brain that occurs close to Wernicke’s area. Wernicke’s area is the part of the brain that is responsible for language comprehension. However, in TSA there is no damage to Wernicke’s area itself.</p><h2 id="ce8995cd-8052-4c30-99ba-5d477a57c753" class="">Προσωπικές Σημειώσεις</h2><p id="a015ed8b-64ed-4658-bc52-2c157fed8f69" class="">
</p><h3 id="37766be7-ecff-4427-9f6e-0613e04a090f" class="">scale12a [ TransSensory | <strong>AQ: 54.1 | male | age 57 ] </strong></h3><p id="74ef5ed7-4987-41b0-b61a-49d7d9fbc6f5" class=""><strong>@Speech: </strong>Απαντάει μονολεκτικά. </p><p id="8d7002d6-236e-49f3-a5a9-92ecf6ba36b7" class=""><strong>@Stroke: </strong>Ψάχνει για στυλό και χαρτί να μιλήσει. Προς το παρόν δυσκολεύεται. Έχει επαναλάβει αρκετές φορές το awful, terrible. </p><p id="eda16f17-1f36-458d-b956-3b423a8078fb" class=""><strong>@Important Event: </strong>Φαίνεται να δυσκολεύεται να κατανοήσει τις ερωτήσεις. Χρησιμοποιεί εν τέλει χαρτί. Λέει κάποια πρόταση ενώ δεν μιλούσε καθόλου που είναι συντακτικά ορθή. Ξαφνικά αρχίζει να μιλάει περισσότερο. </p><p id="a733b917-3f82-4b78-8726-31eef5d58231" class=""><strong>@Window: </strong>Λέει λέξεις αλλά χρειάζεται βοήθεια από την εξετάστρια. </p><p id="23464915-874f-4e46-80ef-b3bc1ad1f058" class=""><strong>@Umbrella: </strong>Προσπερνάει μια λέξη που δεν μπορούσε να πει ύστερα από το σχόλιο της εξετάστριας και μετά καταφέρνει να βρει τη λέξη που δεν μπορούσε να πει (umbrella) </p><p id="351f4898-b729-4141-9b7c-c8288b89727d" class=""><strong>@Cat: </strong>Δεν μπορεί να πει/βρει κάποιες λέξεις. </p><p id="b51a017b-488d-4edc-aba4-f64799664175" class=""><strong>@Flood: </strong>Γενικά, υπάρχουν αρκετά jargons. Δυσκολεύεται πάρα πολύ.</p><p id="db9403f1-2eee-461f-8ef2-98c9444f0140" class=""><strong>@Cinderella Intro: </strong>Φαίνεται να έχει πρόβλημα το χέρι της. Δυσκολεύεται πολύ και η εξετάστρια της κάνει ερωτήσεις προκειμένου να την βοηθήσει. </p><p id="790b05bc-7954-4d2d-b8cd-4ecafc0bfee1" class=""><strong>@Sandwich: </strong>Γράφει peanut butter. Λέει κάποιες βασικές λέξεις αλλά με πολύ βοήθεια από την εξετάστρια. </p><p id="fd15b786-951b-4f7a-b212-41a981257862" class=""><strong>Summing Up: </strong>Δυσκολεύεται αρκετά και χρειάζεται τη βοήθεια της εξετάστριας. Το ένα χέρι (δεξί) φαίνεται να έχει πρόβλημα αλλά μπορεί να γράψει με το άλλο γεγονός που φαίνεται να τη βοηθά στην εκφορά του λόγου.</p><h2 id="c41bcd8a-4bc9-4a56-890f-4de66b13246e" class="">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</h2><p id="28c9336c-ee0f-495a-889f-472575d5e781" class="">
</p><h1 id="546b9321-dd72-4e9a-a6ce-fc326d729270" class="">Not Aphasic </h1><h2 id="dee7ffbe-1565-4e52-8822-996d8652a3fa" class="">Εισαγωγή </h2><p id="fd916158-9bd7-4e14-a895-36e7d02b283c" class="">
</p><h2 id="0705224a-69d3-4ca3-8944-7ecd4a8b8693" class="">Προσωπικές Σημειώσεις</h2><p id="17717f22-16a6-4304-8baa-5bc495b08257" class="">
</p><h3 id="6293c64d-782f-44ce-8bc6-6f9ae1265ed6" class="">ACWT04a [ NotAphasicByWAB | <strong>AQ: 96.0 | female | age 26 ] </strong></h3><p id="74b0d868-4fe8-4194-aecd-2633a19c3108" class=""><strong>Summing Up: </strong>Φαίνεται ο λόγος της να είναι σε πολύ καλό επίπεδο. Με εξαίρεση κάποια μικρά λαθάκια ακούγεται φυσιολογικό. Στη cinderella υπάρχουν κάποιες παύσεις ίσως επειδή προσπαθεί να θυμηθεί την ιστορία. </p><p id="c0b82cf9-af5f-43ab-9d1e-7f52260922a1" class="">
</p><h2 id="5cc385cd-5d42-4ec0-a470-a9de11d8f731" class="">Γενικό Συμπέρασμα από Προσωπικές Σημειώσεις</h2><p id="f32ae5c7-cc14-4c9c-8fb1-5e820fa35e59" class="">
</p><p id="0713a61c-d4d7-4599-bd52-cb0fd04da2ba" class=""><strong>@Speech: </strong></p><p id="fd169ae1-f9b5-4971-b884-42eecc70e2e5" class=""><strong>@Stroke: </strong></p><p id="d092569a-e7a4-4cee-8339-ed532437ad65" class=""><strong>@Important Event: </strong></p><p id="5f92607d-2435-489e-8371-4d504fb499c4" class=""><strong>@Window: </strong></p><p id="2c84395c-c0a7-4d88-a196-d3e6b90a5726" class=""><strong>@Umbrella: </strong></p><p id="159f3027-e6c5-4bd0-b3b7-b25323c70209" class=""><strong>@Cat: </strong></p><p id="c7e5bf0f-9644-4d13-97d7-287a95c109d6" class=""><strong>@Flood: </strong></p><p id="c976f839-5e73-46da-9f7b-eb0791a44fb8" class=""><strong>@Cinderella Intro: </strong></p><p id="4c053efa-3f07-4ece-9d4e-79ac2d39690b" class=""><strong>@Cinderella: </strong></p><p id="25e38926-2e86-4594-8772-d4c5e322d584" class=""><strong>@Sandwich: </strong></p><p id="c9c49646-4649-4329-941b-7c975cd45677" class=""><strong>Summing Up: </strong></p></div></article></body></html>