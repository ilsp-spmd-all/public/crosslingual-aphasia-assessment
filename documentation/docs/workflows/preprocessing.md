# Data Creation and Preprocessing steps

In this section, we emphasize on the preprocessing steps used. The preprocessing step is used under the hood while creating the dataset running 

```python3
create_initial_data_dict_aphasiabank(
        language="english",
        transcripts_root_path="/data/projects/jerry/crosslingual-data",
        wavs_root_path="/data/projects/planv/media.talkbank.org/aphasia/English/",
        save_dict_name="english_data_v2.json",
    )
```

A more precise inspection of the Processors are shown below

<!-- ::: workflows.data -->
