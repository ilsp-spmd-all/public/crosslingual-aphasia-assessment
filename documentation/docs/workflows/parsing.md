# Dependency Parsing

There are several dependency parsers available in each language. We choose to use Spacy's Dependency parsers for English and French while in Greek, we utilize ILPS tools. 

| Language      | Description                          |
| ----------- | ------------------------------------ |
| `english`       | [Spacy model](https://spacy.io/models/en)  |
| `french`       | [Spacy model](https://spacy.io/models/fr) |
| `greek`    | [ILSP Tool](http://nlp.ilsp.gr/nws/) |

```
calculate_conllu_french(
    data_dict=load_dict("english_data.json"),
    save_dict_name="french_data.json",
    key_target="processed_transcript",
    conllu_name="conllu",
)
```
