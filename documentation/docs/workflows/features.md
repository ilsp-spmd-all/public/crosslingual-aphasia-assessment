# Feature Extraction 

We currently support a wide set of language-agnostic features as presented in the table below

|Language ability           |Feature                                | 
|----------                 |-------------                          |
|`Linguistic Productivity`    |Mean Length of Utterance               |  
|`Content Richness`           |Verb/Word Ratio                        |
|                           |Noun/Word Ratio                        |
|                           |Adjective/Word Ratio                   |
|                           |Adverb/Word Ratio                      |
|                           |Preposition/Word Ratio                 |
|                           |Propositional density                  |
|`Fluency`                   |Words per Minute                       |
|`Syntactic Complexity`       |Verbs per Utterance                    |
|                           |Noun Verb Ratio                        |
|                           |Open-closed class words                |
|                           |Conjunction/Word Ratio                 |
|                           |Mean Clauses per utterance             |
|                           |Mean dependent clauses                 |
|                           |Mean independent clauses               |
|                           |Dependent to all clauses ratio         |
|                           |Mean Tree height                       |
|                           |Max Tree depth                         |
|                           |Number of independent clauses          |
|                           |Number of dependent clauses            |
|`Lexical Diversity`          |Lemma/Token Ratio                      |
|                           |Words in Vocabulary per Words          |
|                           |Unique words in vocabulary per Words   |
|`Gross output`               |Number of words                        |



To extract the corresponding features, run

```
create_features_dict(
    data_dict=load_dict("english_data.json"),
    use_asr_transcripts=False,
    save_dict_name="english_data_features.json",
    language="english",
    )
```