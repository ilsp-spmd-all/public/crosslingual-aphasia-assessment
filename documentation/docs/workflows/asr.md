# ASR 

| Language      | Description                          |
| ----------- | ------------------------------------ |
| `english`       | [jonatasgrosman/wav2vec2-large-xlsr-53-english](https://huggingface.co/jonatasgrosman/wav2vec2-large-xlsr-53-english)  |
| `french`       | [jonatasgrosman/wav2vec2-large-xlsr-53-french](https://huggingface.co/jonatasgrosman/wav2vec2-large-xlsr-53-french) |
| `greek`    | [jonatasgrosman/wav2vec2-large-xlsr-53-greek](https://huggingface.co/jonatasgrosman/wav2vec2-large-xlsr-53-greek) |

```
get_english_aphasiabank_asr_transcriptions(
    data_dict,
    save_dict_name="english_data.json",
    add_language_model=False,
    kenlm_model_path=None,
    )
```