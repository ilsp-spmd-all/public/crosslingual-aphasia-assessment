import csv
from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor
from transformers import Wav2Vec2ProcessorWithLM
import re
import pandas as pd
import evaluate

# from datasets import Dataset
from utils import csv_to_dataset, get_kenlm_decoder, df_to_dataset
from transformers import WhisperProcessor, WhisperForConditionalGeneration
from transcript_cleaning import clean_f_l
import torch


def run_ds_whisper(dataset, model, processor, save_to_path):
    wer_metric = evaluate.load("wer")

    def map_to_pred(batch):
        batch["Utterance ID"] = batch["Utterance ID"]

        input_features = processor.feature_extractor(
            batch["audio"]["array"],
            sampling_rate=batch["audio"]["sampling_rate"],
            return_tensors="pt",
        ).input_features.to("cuda")

        predicted_ids = model.generate(input_features)

        # transcription = processor.batch_decode(predicted_ids, normalize = True)
        transcription = processor.tokenizer.batch_decode(
            predicted_ids, skip_special_tokens=True, normalize=True
        )[0]
        batch["text"] = processor.tokenizer._normalize(batch["sentence"])
        batch["text"] = clean_f_l(batch["text"])
        batch["transcription"] = transcription

        return batch

    results = dataset.map(map_to_pred, remove_columns=["audio", "sentence"])
    # results.save_to_disk(save_to_path)
    df = results.to_pandas()

    # df.to_json("whisper.json")
    df = df.dropna()
    df = df.drop(df[df["text"] == ""].index)  # remove empty transcripts
    df = df.drop(df[df["transcription"] == ""].index)  # remove empty transcripts
    print(
        "Test WER: {:.3f}".format(
            wer_metric.compute(predictions=df["transcription"], references=df["text"])
        )
    )
    return df


# asr_transcript_column="asr_transcription_whisper"
# dataset_path='datasets/ab_english'
# csv_path=f'{dataset_path}/control.csv'
# df=pd.read_csv(csv_path)
# dataset=df_to_dataset(df,new_dataset_path=dataset_path)
# MODEL_ID = "openai/whisper-small"
# processor = WhisperProcessor.from_pretrained(MODEL_ID)
# model = WhisperForConditionalGeneration.from_pretrained(MODEL_ID).to("cuda")
# save_to_path="./test_whisper"

# df_asr=run_ds_whisper(dataset,model,processor,save_to_path)
# df_merged = pd.merge(df, df_asr, on=["Utterance ID"])
# df_merged=df_merged.rename({"transcription":asr_transcript_column},axis=1)
# df_merged=df_merged.rename({"text":asr_transcript_column+"_target"},axis=1)
# df_merged.to_csv(csv_path,index=False)


def evaluate_whisper(
    dataset,
    model_id,
    language,
    task="transcribe",
    remove_large_transcripts=True,
    return_ground_truth=True,
):
    # language: e.g. "el","en","fr"
    # task: either "transcribe" or "translate"
    wer_metric = evaluate.load("wer")
    processor = WhisperProcessor.from_pretrained(model_id)
    model = WhisperForConditionalGeneration.from_pretrained(model_id).to("cuda")
    model.config.forced_decoder_ids = processor.get_decoder_prompt_ids(
        language=language, task=task
    )

    def map_to_pred(batch):
        input_features = processor.feature_extractor(
            batch["audio"]["array"],
            sampling_rate=batch["audio"]["sampling_rate"],
            return_tensors="pt",
        ).input_features.to("cuda")
        predicted_ids = model.generate(input_features)

        # transcription = processor.batch_decode(predicted_ids, normalize = True)
        transcription = processor.tokenizer.batch_decode(
            predicted_ids, skip_special_tokens=True, normalize=True
        )[0]

        batch["text"] = processor.tokenizer._normalize(batch["sentence"])
        batch["text"] = clean_f_l(batch["text"])
        batch["transcription"] = processor.tokenizer._normalize(transcription)

        return batch

    results = dataset.map(map_to_pred, remove_columns=["audio", "sentence"])
    df = results.to_pandas()

    # df.to_json("whisper.json")
    df = df.dropna()
    # print( f"found {len(df[df["text"] == ""])} empty utterance ground truths, removing them")
    df = df.drop(df[df["text"] == ""].index)  # remove empty transcripts
    df = df.drop(df[df["transcription"] == ""].index)  # remove empty transcripts
    if remove_large_transcripts:
        df["text_len"] = df["text"].str.len()
        max_len = max(df["text_len"]) + 50
        df["length"] = df["transcription"].str.len()
        # print(f"Large transcripts dropped: {len(df[df["length"] > max_len])}")
        df = df.drop(df[df["length"] > max_len].index)  # remove large transcripts
        df = df.drop(["length", "text_len"], axis=1)
    print(
        "Test WER: {:.3f}".format(
            wer_metric.compute(predictions=df["transcription"], references=df["text"])
        )
    )
    if not return_ground_truth:
        df = df.drop(["text"], axis=1)
    return df


def evaluate_test_set(dataset, model, processor, normalize="whisper"):
    wer_metric = evaluate.load("wer")

    if normalize == "whisper":
        whisper_processor = WhisperProcessor.from_pretrained("openai/whisper-large")

    def map_to_result(batch):
        try:
            input_values = processor(
                batch["audio"]["array"],
                sampling_rate=batch["audio"]["sampling_rate"],
                return_tensors="pt",
            ).input_values.to("cuda")
            with torch.no_grad():
                # input_values = torch.tensor(batch["input_values"], device="cuda")
                
                logits = model(input_values).logits
                

            pred_ids = torch.argmax(logits, dim=-1)
            batch["transcription"] = processor.batch_decode(pred_ids)[0]
            labels = processor(text=batch["sentence"]).input_ids

        except:
            batch["transcription"] = ""
            labels = processor(text="").input_ids

        if normalize == "whisper":
            batch["transcription"] = whisper_processor.tokenizer._normalize(
                batch["transcription"]
            )
            batch["text"] = whisper_processor.tokenizer._normalize(batch["sentence"])
        elif normalize == "w2v2":
            batch["text"] = batch["sentence"].upper()
            batch["transcription"] = batch["transcription"].upper()

        return batch

    results = dataset.map(map_to_result, remove_columns=["audio", "sentence"])
    df = results.to_pandas()

    # df.to_json("whisper.json")
    df = df.dropna()
    # print( f"found {len(df[df["text"] == ""])} empty utterance ground truths, removing them")
    df = df.drop(df[df["text"] == ""].index)  # remove empty transcripts
    df = df.drop(df[df["transcription"] == ""].index)  # remove empty transcripts

    print(
        "Test WER: {:.3f}".format(
            wer_metric.compute(predictions=df["transcription"], references=df["text"])
        )
    )

    return df


def evaluate_test_set_with_lm(dataset, model, processor_with_lm):
    wer_metric = evaluate.load("wer")

    def map_to_result(batch):
        input_values = processor_with_lm(
            batch["audio"]["array"],
            sampling_rate=batch["audio"]["sampling_rate"],
            return_tensors="pt",
        ).input_values.to("cuda")

        with torch.no_grad():
            # input_values = torch.tensor(batch["input_values"], device="cuda")
            logits = model(input_values).logits

        logits = logits.detach().cpu().numpy().squeeze()
        batch["transcription"] = processor_with_lm.decode(logits).text
        # labels=processor(text=batch["sentence"]).input_ids
        batch["text"] = processor_with_lm.tokenizer.decode(batch["labels"])

        return batch

    results = dataset.map(map_to_result, remove_columns=dataset.column_names)
    df = results.to_pandas()

    # df.to_json("whisper.json")
    df = df.dropna()
    # print( f"found {len(df[df["text"] == ""])} empty utterance ground truths, removing them")
    df = df.drop(df[df["text"] == ""].index)  # remove empty transcripts
    df = df.drop(df[df["transcription"] == ""].index)  # remove empty transcripts

    print(
        "Test WER: {:.3f}".format(
            wer_metric.compute(predictions=df["transcription"], references=df["text"])
        )
    )

    return df


def run_whisper_csv(
    csv_path,
    task,
    language,
    model_id="openai/whisper-large",
    asr_transcription_column_name="asr_transcription_whisper",
):
    df = pd.read_csv(csv_path)
    dataset = df_to_dataset(
        df,
        new_dataset_path="/home/plitsis/planv-new/planv/datasets/ab_french/",
        old_dataset_path="/home/plitsis/planv/datasets/ab_french/",
    )
    df_asr = evaluate_whisper(dataset, model_id, language, task)
    df_merged = pd.merge(df, df_asr, on=["Utterance ID"])
    df_merged = df_merged.rename(
        {"transcription": asr_transcription_column_name}, axis=1
    )
    df_merged = df_merged.rename(
        {"text": asr_transcription_column_name + "_target"}, axis=1
    )
    df_merged.to_csv(csv_path, index=False)


def run_whisper_df(
    df,
    task,
    language,
    model_id="openai/whisper-large",
    asr_transcription_column_name="asr_transcription_whisper",
    create_target_column=True,
    remove_large_transcripts=True,
):
    dataset = df_to_dataset(df)
    df_asr = evaluate_whisper(
        dataset,
        model_id,
        language,
        task,
        remove_large_transcripts,
        return_ground_truth=create_target_column,
    )
    df_merged = pd.merge(df, df_asr, on=["Utterance ID"])
    df_merged = df_merged.rename(
        {"transcription": asr_transcription_column_name}, axis=1
    )
    if create_target_column:
        df_merged = df_merged.rename(
            {"text": asr_transcription_column_name + "_target"}, axis=1
        )

    return df_merged


def run_w2v2_csv(
    csv_path,
    model_id,
    use_lm=False,
    asr_transcription_column_name="asr_transcription_w2v2",
    normalize="w2v2",  # choose "w2v2" or "whisper" normalization
):
    processor = Wav2Vec2Processor.from_pretrained(model_id)
    model = Wav2Vec2ForCTC.from_pretrained(model_id).to("cuda")
    df = pd.read_csv(csv_path)
    dataset = df_to_dataset(df)

    if use_lm:
        vocab_dict = processor.tokenizer.get_vocab()
        kenlm_model_path = "5gram_correct.arpa"
        kenlm_decoder = get_kenlm_decoder(
            vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path
        )
        processor_with_lm = Wav2Vec2ProcessorWithLM(
            feature_extractor=processor.feature_extractor,
            tokenizer=processor.tokenizer,
            decoder=kenlm_decoder,
        )

        df_asr = evaluate_test_set_with_lm(dataset, model, processor_with_lm)
    else:
        df_asr = evaluate_test_set(dataset, model, processor, normalize=normalize)

    df_merged = pd.merge(df, df_asr, on=["Utterance ID"])
    df_merged = df_merged.rename(
        {"transcription": asr_transcription_column_name}, axis=1
    )
    df_merged = df_merged.rename(
        {"text": asr_transcription_column_name + "_target"}, axis=1
    )
    df_merged.to_csv(csv_path, index=False)


def greek_to_english_whisper(
    df,
    model_id="openai/whisper-large",
    asr_transcription_column_name="translation_whisper",
    remove_large_transcripts=True,
):
    dataset = df_to_dataset(df)
    wer_metric = evaluate.load("wer")
    processor = WhisperProcessor.from_pretrained(model_id)
    model = WhisperForConditionalGeneration.from_pretrained(model_id).to("cuda")
    model.config.forced_decoder_ids = processor.get_decoder_prompt_ids(
        language="el", task="translate"
    )

    def map_to_pred(batch):
        input_features = processor.feature_extractor(
            batch["audio"]["array"],
            sampling_rate=batch["audio"]["sampling_rate"],
            return_tensors="pt",
        ).input_features.to("cuda")
        predicted_ids = model.generate(input_features)

        # transcription = processor.batch_decode(predicted_ids, normalize = True)
        transcription = processor.tokenizer.batch_decode(
            predicted_ids, skip_special_tokens=True, normalize=True
        )[0]

        batch["text"] = processor.tokenizer._normalize(batch["sentence"])
        batch["text"] = clean_f_l(batch["text"])
        batch["transcription"] = processor.tokenizer._normalize(transcription)

        return batch

    results = dataset.map(map_to_pred, remove_columns=["audio", "sentence"])
    df_asr = results.to_pandas()

    # df_asr.to_json("whisper.json")
    df_asr = df_asr.dropna()
    df_asr = df_asr.drop(df_asr[df_asr["text"] == ""].index)  # remove empty transcripts
    df_asr = df_asr.drop(
        df_asr[df_asr["transcription"] == ""].index
    )  # remove empty transcripts
    if remove_large_transcripts:
        df_asr["text_len"] = df_asr["text"].str.len()
        max_len = max(df_asr["text_len"]) + 50
        df_asr["length"] = df_asr["transcription"].str.len()
        df_asr = df_asr.drop(
            df_asr[df_asr["length"] > max_len].index
        )  # remove large transcripts
        df_asr.drop(["length", "text_len"], axis=1)

    df_merged = pd.merge(df, df_asr, on=["Utterance ID"])
    df_merged = df_merged.rename(
        {"transcription": asr_transcription_column_name}, axis=1
    )
    df_merged = df_merged.rename(
        {"text": asr_transcription_column_name + "_target"}, axis=1
    )
    df_merged.to_csv(csv_path, index=False)


if __name__ == "__main__":
    # csv_path = "/home/plitsis/planv/datasets/planv-frontiers/all_data.csv"
    # csv_path = "/home/plitsis/planv/datasets/planv_gr/planv_all.csv"
    csv_path = "/home/plitsis/planv/datasets/ab_english/all_data.csv"
    # csv_path = "/home/plitsis/planv/datasets/ab_french/all_data.csv"
    # csv_path="/home/plitsis/planv/datasets/planv-aphasia/all_data.csv"
    # csv_path="/home/plitsis/planv-new/crosslingual-aphasia-assessment/data_csv/all_data_fr.csv"

    # MODEL_ID = "Charalampos/whisper-large-el"
    MODEL_ID = "openai/whisper-large"
    MODEL_ID_w2v2 = "jonatasgrosman/wav2vec2-large-xlsr-53-english"
    # run_whisper_csv(
    #     csv_path,
    #     "translate",
    #     "en",
    #     MODEL_ID,
    #     asr_transcription_column_name="asr_translation_whisper_large",
    # )

    # run_whisper_csv(
    #     csv_path,
    #     "translate",
    #     "fr",
    #     MODEL_ID,
    #     asr_transcription_column_name="asr_translation_whisper",
    # )

    run_w2v2_csv(
        csv_path,
        MODEL_ID_w2v2,
        asr_transcription_column_name="w2v2_whisper_norm",
        normalize="whisper"
        )

    # greek_to_english_whisper(df)
