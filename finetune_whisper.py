# from transformers import WhisperProcessor, WhisperForConditionalGeneration
# from transformers import WhisperFeatureExtractor, WhisperTokenizer
# from utils import csv_to_dataset,df_to_dataset,get_kenlm_decoder
# import evaluate
from datasets import load_from_disk
# from transcript_cleaning import clean_f_l
# from typing import Any, Dict, List, Union
# import torch
# from dataclasses import dataclass
# import os
# import pandas as pd

import os

# from transformers import Wav2Vec2CTCTokenizer,Wav2Vec2FeatureExtractor,Wav2Vec2ForCTC
from transformers import WhisperProcessor, WhisperForConditionalGeneration
from transformers import WhisperFeatureExtractor, WhisperTokenizer
from transformers import Seq2SeqTrainer
from transformers import Seq2SeqTrainingArguments
from transformers.integrations import WandbCallback
from transformers import EarlyStoppingCallback
from transformers import TrainingArguments, Trainer, logging
from datasets import load_dataset, load_metric, Audio, Dataset

import torch
# from data import DataCollatorCTCWithPadding
import pandas as pd
# import torchaudio
import numpy as np
from utils import csv_to_dataset,df_to_dataset,get_kenlm_decoder
from transcript_cleaning import clean_f_l
import wandb

# from pynvml import *
import re

from dataclasses import dataclass
from typing import Any, Dict, List, Union
import evaluate
import panel as pn
# import holoviews as hv
from io import StringIO
import jiwer
from pathlib import Path
import tempfile
import numbers
# from bokeh.resources import INLINE
# hv.extension("bokeh", logo=False)

# del get_wer_from_df
def get_wer_from_df(df,transcript_column="asr_transcription_whisper_target", asr_column="asr_transcription_whisper_small"):
    metric = evaluate.load("wer")
    print(
            "Test WER: {:.3f}".format(
                metric.compute(predictions=list(df[asr_column]), references=list(df[transcript_column]))
            )
        )
    
def prepare_dataset(batch,normalize_whisper=True):
    audio = batch["audio"] 
    
    # compute log-Mel input features from input audio array 
    batch["input_features"] = feature_extractor(audio["array"], sampling_rate=audio["sampling_rate"]).input_features[0]

    if normalize_whisper:
        batch["sentence"] = processor.tokenizer._normalize(batch["sentence"])
        batch["sentence"] = clean_f_l(batch["sentence"])

    # encode target text to label ids 
    batch["labels"] = tokenizer(batch["sentence"]).input_ids
    return batch


RANDOM_SEED=42
# np.random.seed(RANDOM_SEED)
language="el"
task="transcribe"
whisper_model_size="small"

csv_train="data_csv/finetune_whisper/train.csv"
csv_test="data_csv/finetune_whisper/test.csv"
csv_val="data_csv/finetune_whisper/val.csv"
df_train=pd.read_csv(csv_train)
df_test=pd.read_csv(csv_test)
df_val=pd.read_csv(csv_val)


use_local_checkpoint=False
if use_local_checkpoint:
    model_name="./whisper-small-ab_eng/checkpoint-1000"
    processor_dir="whisper-small-ab_eng"
else:
    model_name="openai/whisper-small"

run = wandb.init(project="whisper_finetuning_gr", job_type="fine-tuning", group="small" )
os.environ['WANDB_LOG_MODEL']='true'
os.environ['WANDB_WATCH']='all'

feature_extractor = WhisperFeatureExtractor.from_pretrained(model_name)
if use_local_checkpoint:
    tokenizer = WhisperTokenizer.from_pretrained(processor_dir, language=language, task=task)

    processor = WhisperProcessor.from_pretrained(processor_dir, language=language, task=task)
else:
    tokenizer = WhisperTokenizer.from_pretrained(model_name, language=language, task=task)

    processor = WhisperProcessor.from_pretrained(model_name, language=language, task=task)


@dataclass
class DataCollatorSpeechSeq2SeqWithPadding:
    processor: Any

    def __call__(self, features: List[Dict[str, Union[List[int], torch.Tensor]]]) -> Dict[str, torch.Tensor]:
        # split inputs and labels since they have to be of different lengths and need different padding methods
        # first treat the audio inputs by simply returning torch tensors
        input_features = [{"input_features": feature["input_features"]} for feature in features]
        batch = self.processor.feature_extractor.pad(input_features, return_tensors="pt")

        # get the tokenized label sequences
        label_features = [{"input_ids": feature["labels"]} for feature in features]
        # pad the labels to max length
        labels_batch = self.processor.tokenizer.pad(label_features, return_tensors="pt")

        # replace padding with -100 to ignore loss correctly
        labels = labels_batch["input_ids"].masked_fill(labels_batch.attention_mask.ne(1), -100)

        # if bos token is appended in previous tokenization step,
        # cut bos token here as it's append later anyways
        if (labels[:, 0] == self.processor.tokenizer.bos_token_id).all().cpu().item():
            labels = labels[:, 1:]

        batch["labels"] = labels

        return batch


data_collator = DataCollatorSpeechSeq2SeqWithPadding(processor=processor)


val_dataset=df_to_dataset(df_val,percent_to_use=1.,random_seed=RANDOM_SEED)
train_dataset=df_to_dataset(df_train,percent_to_use=1.,random_seed=RANDOM_SEED)
test_dataset=df_to_dataset(df_test,percent_to_use=1.,random_seed=RANDOM_SEED)

val_data_path="hf_datasets/greek/val.hf"
train_data_path="hf_datasets/greek/train.hf"
test_data_path="hf_datasets/greek/test.hf"

if not os.path.exists(val_data_path):

    val_data = val_dataset.map(prepare_dataset)
    val_data.save_to_disk(val_data_path)
else:
    val_data = load_from_disk(val_data_path)

if not os.path.exists(train_data_path):
    train_data = train_dataset.map(prepare_dataset)
    train_data.save_to_disk(train_data_path)
else:
    train_data = load_from_disk(train_data_path)

if not os.path.exists(test_data_path):
    test_data = test_dataset.map(prepare_dataset)
    test_data.save_to_disk(test_data_path)
else:
    test_data = load_from_disk(test_data_path)
    

metric = evaluate.load("wer")
def compute_metrics(pred):
    pred_ids = pred.predictions
    label_ids = pred.label_ids

    # replace -100 with the pad_token_id
    label_ids[label_ids == -100] = tokenizer.pad_token_id

    # we do not want to group tokens when computing the metrics
    pred_str = tokenizer.batch_decode(pred_ids, skip_special_tokens=True,normalize=True)
    label_str = tokenizer.batch_decode(label_ids, skip_special_tokens=True,normalize=True)

    wer = 100 * metric.compute(predictions=pred_str, references=label_str)

    return {"wer": wer}


model = WhisperForConditionalGeneration.from_pretrained(model_name)
model.config.forced_decoder_ids = None
model.config.suppress_tokens = []
model.config.use_cache = False


training_args = Seq2SeqTrainingArguments(
    output_dir=f"./whisper-{wandb.run.name}",  # change to a repo name of your choice
    per_device_train_batch_size=8,
    gradient_accumulation_steps=8,  # increase by 2x for every 2x decrease in batch size
    learning_rate=1e-5,
    save_total_limit=4,
    warmup_steps=100,
    max_steps=500,
    gradient_checkpointing=True,
    fp16=True,
#     fp16_full_eval=True,
    # optim="adamw_bnb_8bit",
    evaluation_strategy="steps",
    per_device_eval_batch_size=8,
    predict_with_generate=True,
    generation_max_length=225,
    save_steps=200,
    eval_steps=100,
    logging_steps=50,
    report_to="none",
    load_best_model_at_end=True,
    metric_for_best_model="wer",
    greater_is_better=False,
    push_to_hub=False,
    # remove_unused_columns=False, 
    # ignore_data_skip=True
)

trainer = Seq2SeqTrainer(
    args=training_args,
    model=model,
    train_dataset=train_data,
    eval_dataset=val_data,
    data_collator=data_collator,
    compute_metrics=compute_metrics,
    tokenizer=processor,
)
# print(torch.cuda.is_available())
processor.save_pretrained(training_args.output_dir)


def dataset_to_records(dataset):
    records = []
    for item in dataset:
        record = {}
        record["audio"] = wandb.Audio(item["audio"]["array"], 16000)
        record["sentence"] = item["sentence"]
        # normalize sentence with whisper normalizer and then remove F and L tokens
        record["sentence"] = processor.tokenizer._normalize(record["sentence"])
        record["sentence"] = clean_f_l(record["sentence"])
        records.append(record)
    records = pd.DataFrame(records)
    return records

def decode_predictions(trainer, predictions):
    pred_ids = predictions.predictions
    pred_str = trainer.tokenizer.batch_decode(pred_ids, skip_special_tokens=True, normalize=True)
    return pred_str

def compute_measures(predictions, labels):
    labels_nonempty=[]
    for label in labels:
        if label:
            labels_nonempty.append(label)
    measures = [jiwer.compute_measures(ls, ps,) for ps, ls in zip(predictions, labels_nonempty)]
    measures_df = pd.DataFrame(measures)[["wer", "hits", "substitutions", "deletions", "insertions"]]
    return measures_df

class WandbProgressResultsCallback(WandbCallback):
    def __init__(self, trainer, sample_dataset): 
        super().__init__()
        self.trainer = trainer
        self.sample_dataset = sample_dataset
        self.records_df = dataset_to_records(sample_dataset)
        
    def on_log(self, args, state, control, model=None, logs=None, **kwargs):
        super().on_log(args, state, control, model, logs)
        predictions = trainer.predict(self.sample_dataset)
        predictions = decode_predictions(self.trainer, predictions)
        measures_df = compute_measures(predictions, self.records_df["sentence"].tolist())
        records_df = pd.concat([self.records_df, measures_df], axis=1)
        records_df["prediction"] = predictions
        records_df["step"] = state.global_step
        records_table = self._wandb.Table(dataframe=records_df)
        self._wandb.log({"sample_predictions": records_table})
        
    def on_save(self, args, state, control, model=None, tokenizer=None, **kwargs):
        if self._wandb is None:
            return
        if self._log_model and self._initialized and state.is_world_process_zero:
            with tempfile.TemporaryDirectory() as temp_dir:
                self.trainer.save_model(temp_dir)
                metadata = (
                    {
                        k: v
                        for k, v in dict(self._wandb.summary).items()
                        if isinstance(v, numbers.Number) and not k.startswith("_")
                    }
                    if not args.load_best_model_at_end
                    else {
                        f"eval/{args.metric_for_best_model}": state.best_metric,
                        "train/total_floss": state.total_flos,
                    }
                )
                artifact = self._wandb.Artifact(
                    name=f"model-{self._wandb.run.id}",
                    type="model", metadata=metadata)
                for f in Path(temp_dir).glob("*"):
                    if f.is_file():
                        with artifact.new_file(f.name, mode="wb") as fa:
                            fa.write(f.read_bytes())
                self._wandb.run.log_artifact(artifact)


progress_callback = WandbProgressResultsCallback(trainer, val_data.select(np.random.randint(0,len(val_data)-1,size=50)))

trainer.add_callback(progress_callback)

trainer.add_callback(EarlyStoppingCallback(early_stopping_patience=2))

trainer.train()

wandb.run.finish()

