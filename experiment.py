import pandas as pd
from feature_names import FEATURES_NAMES, FEATURES_NAMES_SMALL, FEATURES_NAMES_SMALLER
import numpy as np
from sklearn.preprocessing import StandardScaler, RobustScaler
from xgboost.sklearn import XGBClassifier
from sklearn.svm import SVC
from sklearn import tree
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from feature_extraction import DatasetFeatureExtractor
from sklearn.model_selection import (
    KFold,
    StratifiedKFold,
    LeaveOneOut,
    RepeatedStratifiedKFold,
)
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from adaptation import TEDxMultilingualDatasetEnEl, OptimalAdaptation
from sklearn.metrics import (
    classification_report,
    confusion_matrix,
    roc_curve,
    roc_auc_score,
    # plot_roc_curve,
    auc,
    balanced_accuracy_score,
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
)
from imblearn.over_sampling import SMOTE, ADASYN
from tabpfn import TabPFNClassifier
from sklearn.inspection import permutation_importance
# def label_from_aq(aq, no_of_labels=4):
#     bin_length = int(100 / no_of_labels)
#     try:
#         label = int(aq / bin_length)
#     except:
#         return no_of_labels
#     return label
import mord

def label_from_aq(aq):
    if aq > 75:
        return 3
    elif aq > 50 and aq <= 75:
        return 2
    elif aq > 25 and aq <= 50:
        return 1
    elif aq > 0 and aq <= 25:
        return 0


def get_dataset(features_csv, label):
    df_feats = pd.read_csv(features_csv)
    X = df_feats[FEATURES_NAMES]
    if label == "aq":
        y = df_feats["WAB_AQ"].map(label_from_aq)
    else:
        y = np.ones(len(X)) * label

    return X, y


def get_label_names(label_type):
    if label_type == "aq":
        label_names = [0, 1, 2, 3]
    if label_type == "aq_gr":
        label_names = [0, 1]
    if label_type == "control":
        label_names = [0, 1]
    if label_type == "aq_fr":
        label_names = [0, 1, 2, 3, 4]
    return label_names


def get_dataset_from_df(df_feats, label, features):
    # df_feats=pd.read_csv(features_csv)
    X = df_feats[features]
    if label == "aq" or label == "aq_gr":
        y = df_feats["WAB_AQ"].map(label_from_aq)

    elif label == "aq_fr":
        y = df_feats["BDAE_score"].astype(int)

    elif label == "control":
        y = df_feats["control"].map(lambda x: 1 if x == True else 0)

    elif label == "aphasia_type":
        y = df_feats["aphasia_type"].map(lambda x: 1 if x == True else 0)

    return X, y


def loso_english_experiment(data_dict, label_aphasia=True):
    X, y = get_dataset_from_df(data_dict, label_aphasia)

    kf = LeaveOneOut()
    nan_ids_from_X = np.any(np.isnan(X), axis=0)
    X = X[:, ~nan_ids_from_X]
    clf = make_pipeline(StandardScaler(), SVC(gamma="auto", kernel="linear"))

    accuracy_list = []
    f1_list = []
    idx = 0
    kf_split = kf.split(X=X)

    for model_name in ["svm", "xgboost", "tree"]:
        for train, test in kf_split:
            idx += 1
            X_train, y_train = X[train], y[train]
            X_test, y_test = X[test], y[test]
            if model_name == "svm":
                clf = make_pipeline(StandardScaler(), SVC(gamma="auto"))
            elif model_name == "xgboost":
                clf = XGBClassifier()
            elif model_name == "tree":
                clf = tree.DecisionTreeClassifier()
            clf.fit(X_train, y_train)
            y_preds = clf.predict(X_test)
            acc = clf.score(X_test, y_test)
            # f1 = f1_score(y_test, y_preds, zero_division=1)
            accuracy_list.append(acc)
            # f1_list.append(f1)
        accuracy = sum(accuracy_list) / len(accuracy_list)
        # f1 = sum(f1_list) / len(f1_list)
        print(f"Model: {model_name} - Accuracy: {accuracy}")
    return accuracy


def zeroshot_experiment_csv(train_csv, test_csv, model="xgboost"):
    X_train, y_train = get_dataset(train_csv)
    X_test, y_test = get_dataset(test_csv)

    nan_ids_from_X_train = np.any(np.isnan(X_train), axis=0)

    X_train = X_train[:, ~nan_ids_from_X_train]
    nan_ids_from_X_test = np.any(np.isnan(X_test), axis=0)
    X_test = X_test[:, ~nan_ids_from_X_test]
    print(X_train.shape, X_test.shape)

    english_scaler = StandardScaler()
    X_train = english_scaler.fit_transform(X_train, y_train)

    if model == "xgboost":
        xgb_classifier = XGBClassifier()
        xgb_classifier.fit(X_train, y_train)

        X_test = english_scaler.transform(X_test)
        print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = xgb_classifier.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = xgb_classifier.score(X_test, y_test)
        print(acc)

        weights = xgb_classifier.get_booster().get_score(importance_type="weight")
        from feature_names import FEATURES_NAMES

        for idx, weight_key in enumerate(weights):
            print(
                f"Feature {FEATURES_NAMES[idx].ljust(30)} \t weight={weights[weight_key]}"
            )
    elif model == "svm":
        clf = SVC(gamma="auto")
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)
    elif model == "tree":
        clf = tree.DecisionTreeClassifier()
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)
    elif model == "forest":
        clf = RandomForestClassifier(max_depth=2, random_state=0)
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)
    elif model == "adaboost":
        clf = AdaBoostClassifier(n_estimators=100, random_state=0)
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)


def zeroshot_experiment_np(X_train, y_train, X_test, y_test, model="xgboost"):
    # X_train, y_train = get_dataset(train_dict)
    # X_test, y_test = get_dataset(test_dict)

    nan_ids_from_X_train = np.any(np.isnan(X_train), axis=0)

    X_train = X_train[:, ~nan_ids_from_X_train]
    nan_ids_from_X_test = np.any(np.isnan(X_test), axis=0)
    X_test = X_test[:, ~nan_ids_from_X_test]
    print(X_train.shape, X_test.shape)

    english_scaler = StandardScaler()
    X_train = english_scaler.fit_transform(X_train)

    if model == "xgboost":
        xgb_classifier = XGBClassifier()
        xgb_classifier.fit(X_train, y_train)

        X_test = english_scaler.transform(X_test)
        print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = xgb_classifier.predict(X_test)
        print(y_test)
        print(len(y_test))
        print(y_preds)
        print(len(y_preds))
        acc = xgb_classifier.score(X_test, y_test)
        print(acc)

        # weights = xgb_classifier.get_booster().get_score(importance_type="weight")
        # from feature_names import FEATURES_NAMES

        # for idx, weight_key in enumerate(weights):
        #     print(
        #         f"Feature {FEATURES_NAMES[idx].ljust(30)} \t weight={weights[weight_key]}"
        #     )
    elif model == "svm":
        clf = SVC(gamma="auto")
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)
    elif model == "tree":
        clf = tree.DecisionTreeClassifier()
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)
    elif model == "forest":
        clf = RandomForestClassifier(max_depth=2, random_state=0)
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)
    elif model == "adaboost":
        clf = AdaBoostClassifier(n_estimators=100, random_state=0)
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)


def english_experiment(
    df_train,
    df_test,
    transcript_column_name="clean_transcription_v1",
    classes="aq",
    model="svm",
):
    feature_extractor_train = DatasetFeatureExtractor(
        df_train,
        language="english",
        utterance_column=transcript_column_name,
        conllu_column=transcript_column_name + "_conllu",
        extract_conllu=False,
    )
    feature_extractor_test = DatasetFeatureExtractor(
        df_test,
        language="english",
        utterance_column=transcript_column_name,
        conllu_column=transcript_column_name + "_conllu",
        extract_conllu=False,
    )
    df_feats_train = feature_extractor_train.calculate_features_all_speakers()
    df_feats_test = feature_extractor_test.calculate_features_all_speakers()

    df_test, labels_test = get_dataset_from_df(df_feats_test, classes)
    df_train, labels_train = get_dataset_from_df(df_feats_train, classes)
    zeroshot_experiment_np(
        df_train.to_numpy(), labels_train, df_test.to_numpy(), labels_test, model=model
    )


class ExperimentCV(object):
    def __init__(
        self,
        df,
        transcript_column_name,
        language="en",
        random_seed=42,
        features=FEATURES_NAMES_SMALL,
    ):
        self.df = df
        self.feature_extractor = DatasetFeatureExtractor(
            df,
            language=language,
            utterance_column=transcript_column_name,
            conllu_column=transcript_column_name + "_conllu",
            extract_conllu=False,
        )
        self.random_seed = random_seed
        self.features = features

    def conduct_experiment(self, model_name, label_type, folds="loso", upsample=True):
        df_feats = self.feature_extractor.calculate_features_all_speakers()
        df_train, labels_train = get_dataset_from_df(
            df_feats, label_type, self.features
        )
        X = df_train.to_numpy()
        y = labels_train.to_numpy()
        print("Data Length", len(y))
        label_names = get_label_names(label_type)
        if label_type == "aq_gr":
            y = y - 2
        if label_type == "aq_fr":
            y = y - 1
            upsample = False

        if folds == "loso":
            kf = LeaveOneOut()
        elif folds > 1:
            # if model_name=="mlp":
            #     kf = StratifiedKFold(n_splits=folds)
            # else:
            # # kf = KFold(n_splits=folds,shuffle=False, random_state=None)
            kf = RepeatedStratifiedKFold(n_splits=folds, n_repeats=10, random_state=42)
        elif folds <= 1:
            "Need at least folds=2 to work"
            exit()
        kf_split = list(kf.split(X=X, y=y))

        conf_matrix_list_of_arrays = []
        accuracy_list = []
        # bacc_list=[]
        recall_list = []
        precision_list = []
        f1_list = []
        roc_list = []

        for train, test in kf_split:

            X_train, y_train = X[train], y[train]

            # print("positive percent: ",sum(y_train)/len(y_train)*100)
            if upsample:

                X_train, y_train = SMOTE(k_neighbors=3, random_state=42).fit_resample(
                    X_train, y_train
                )
            scaler = RobustScaler()
            # print("positive percent after upsample: ",sum(y_train)/len(y_train)*100)
            # scaler = StandardScaler()
            X_train = scaler.fit_transform(X_train)
            X_test, y_test = X[test], y[test]
            X_test = scaler.transform(X_test)
            if model_name == "svm":
                clf = SVC(gamma="auto", probability=True)
            elif model_name == "xgboost":
                clf = XGBClassifier()
            elif model_name == "tree":
                clf = tree.DecisionTreeClassifier()
            elif model_name == "forest":
                clf = RandomForestClassifier()
            elif model_name == "mlp":
                clf = MLPClassifier(
                    # tune batch size later
                    batch_size=64,
                    # keep random state constant to accurately compare subsequent models
                    random_state=42,
                    activation="logistic",
                    learning_rate="constant",
                    solver="lbfgs",
                    hidden_layer_sizes=(100,),
                    max_iter=1000,
                )
            elif model_name == "mord_logistic_it":
                clf=mord.LogisticIT()
            clf.fit(X_train, y_train)
            clf_preds = clf.predict(X_test)
            clf_probas = clf.predict_proba(X_test)
            acc = clf.score(X_test, y_test)
            accuracy_list.append(acc)
            f1 = f1_score(y_test, clf_preds, zero_division=0, average="weighted")
            f1_list.append(f1)
            precision = precision_score(
                y_test,
                clf_preds,
                labels=label_names,
                average="weighted",
                zero_division=0,
            )
            precision_list.append(precision)
            recall = recall_score(
                y_test,
                clf_preds,
                labels=label_names,
                average="weighted",
                zero_division=0,
            )
            recall_list.append(recall)

            conf_matrix = confusion_matrix(y_test, clf_preds, labels=label_names)
            conf_matrix_list_of_arrays.append(conf_matrix)
            if not folds == "loso":
                if label_type == "aq" or label_type == "aq_fr":
                    roc_au = roc_auc_score(y_test, clf_probas, multi_class="ovo")
                else:
                    roc_au = roc_auc_score(
                        y_test, clf_probas[:, 1], labels=label_names, multi_class="ovr"
                    )
                roc_list.append(roc_au)
        mean_accuracy = np.mean(accuracy_list)
        sd_accuracy = np.std(accuracy_list)
        # balanced_accuracy = sum(bacc_list) / len(bacc_list)
        conf_matrix = np.sum(conf_matrix_list_of_arrays, axis=0) / len(
            conf_matrix_list_of_arrays
        )
        precision = np.mean(precision_list)
        recall = np.mean(recall_list)
        mean_acc = round(mean_accuracy * 100, 2)
        std_acc = round(sd_accuracy * 100, 2)
        mean_f1 = round(np.mean(f1_list) * 100, 2)
        std_f1 = round(np.std(f1_list) * 100, 2)

        print(f"Model: {model_name} - Accuracy: {mean_acc} +- {std_acc}")
        print(f"F1: {mean_f1} +- {std_f1} ")
        if not folds == "loso":
            mean_auc = round(np.mean(roc_list) * 100, 2)
            std_auc = round(np.std(roc_list) * 100, 2)

            print(f"ROC AUC: {mean_auc} +- {std_auc}")
        # print(f"Precision: {precision}, Recall: {recall}")
        # print(f"Balanced accuracy: {balanced_accuracy}")
        print(f"Confusion Matrix:")
        print(conf_matrix)
        return mean_acc, std_acc, mean_f1, std_f1, mean_auc, std_auc


class CrosslingualExperiment(object):
    def __init__(
        self,
        english_df,
        low_res_df,
        low_res_language="el",
        transcript_column_name="asr_transcription_whisper_target",
        transcript_column_name_low_res="",
        features=FEATURES_NAMES,
    ) -> None:

        self.english_df = english_df
        self.low_res_df = low_res_df
        self.transcript_column_name = transcript_column_name
        if not transcript_column_name_low_res:
            transcript_column_name_low_res = transcript_column_name
        self.feature_extractor_train = DatasetFeatureExtractor(
            self.english_df,
            language="en",
            utterance_column=transcript_column_name,
            conllu_column=transcript_column_name + "_conllu",
            extract_conllu=False,
        )
        self.feature_extractor_test = DatasetFeatureExtractor(
            self.low_res_df,
            language=low_res_language,
            utterance_column=transcript_column_name_low_res,
            conllu_column=transcript_column_name_low_res + "_conllu",
            extract_conllu=False,
        )
        self.features = features


    def conduct_experiment(
        self, upsample=True, model_name="xgboost", label_type="control",importance=True,
    ):
        features = self.features
        label_names = get_label_names(label_type)
        df_feats_train = self.feature_extractor_train.calculate_features_all_speakers()
        df_train, labels_train = get_dataset_from_df(
            df_feats_train, label_type, features
        )
        X_train = df_train.to_numpy()
        y_train = labels_train.to_numpy()
        df_feats_test = self.feature_extractor_test.calculate_features_all_speakers()
        df_test, labels_test = get_dataset_from_df(df_feats_test, label_type, features)
        X_test = df_test.to_numpy()
        y_test = labels_test.to_numpy()
        # if label_type == "aq_gr":
        #     y_test = y_test - 2
        # if label_type == "aq_fr":
        #     y_test = y_test - 1
        #     upsample = False

        # X_train, y_train = X[train], y[train]
        if upsample:
            X_train, y_train = SMOTE(k_neighbors=3, random_state=42).fit_resample(
                X_train, y_train
            )
        scaler = StandardScaler()
        if model_name!="tabpfn":
            X_train = scaler.fit_transform(X_train)
            X_test = scaler.transform(X_test)
        if model_name == "svm":
            clf = SVC(gamma="auto", probability=True)
        elif model_name == "xgboost":
            clf = XGBClassifier()
            
        elif model_name == "tree":
            clf = tree.DecisionTreeClassifier()
        elif model_name == "mlp":
            clf = MLPClassifier(
                # tune batch size later
                batch_size="auto",
                # keep random state constant to accurately compare subsequent models
                random_state=42,
                activation="logistic",
                learning_rate="adaptive",
                solver="adam",
                hidden_layer_sizes=(300,),
                max_iter=1000,
            )
        elif model_name=="tabpfn":
            clf=TabPFNClassifier(device='cuda', N_ensemble_configurations=32)
        clf.fit(X_train, y_train)
        # perform permutation importance
        results = permutation_importance(clf, X_train, y_train, scoring='accuracy')
        # get importance
        importance = results.importances_mean
        # summarize feature importance
        # for i,v in enumerate(importance):
        #     print('Feature: %0d, Score: %.5f' % (i,v))
        # importance = clf.feature_importances_
        for i,v in sorted(zip(self.features,importance),key=lambda x: x[1]):
            print('Feature: %s, Score: %.5f' % (i,v))
        clf_preds = clf.predict(X_test)
        clf_probas = clf.predict_proba(X_test)
        acc = clf.score(X_test, y_test)
        f1 = f1_score(y_test, clf_preds, zero_division=0, average="weighted")
        conf_matrix = confusion_matrix(y_test, clf_preds, labels=label_names)
        if label_type == "aq" or label_type == "aq_fr":
            roc_au = roc_auc_score(y_test, clf_probas, multi_class="ovo")
        else:
            roc_au = roc_auc_score(
                y_test, clf_probas[:, 1], labels=label_names, multi_class="ovr"
            )

            fpr, tpr, thresholds = roc_curve(y_test, clf_probas[:, 1])
            roc_auc = auc(fpr, tpr)
            i = np.arange(len(tpr))  # index for df
            roc = pd.DataFrame(
                {
                    "tf": pd.Series(tpr - (1 - fpr), index=i),
                    "threshold": pd.Series(thresholds, index=i),
                }
            )
            roc_t = roc.iloc[(roc.tf - 0).abs().argsort()[:1]]
            threshold = list(roc_t["threshold"])[0]
            print("optimal threshold: ", threshold)
            auc_preds = [
                (lambda x: 1 if x > threshold else 0)(x) for x in clf_probas[:, 1]
            ]
            f1_roc = f1_score(y_test, auc_preds)
            acc_roc = accuracy_score(y_test, auc_preds)
            print("f1 roc: ", f1_roc)
            print("acc roc: ", acc_roc)
            print("")
        acc=round(acc*100,2)
        f1=round(f1*100,2)
        print(f"Accuracy: {acc}")
        print("F1 score: ",f1 )
        print(conf_matrix)
        print("AUROC: ", roc_au)
        print("AUROC2: ", roc_auc)

        return acc, f1, round(roc_au*100,2)

    def conduct_experiment_cv(
        self, label_type, model_name="xgboost", folds=10, upsample=True, ot=False, threshold_tuning=False,covariate_disc=True
    ):
        df_feats_train = self.feature_extractor_train.calculate_features_all_speakers()
        df_train, labels_train = get_dataset_from_df(
            df_feats_train, label_type, self.features
        )
        df_feats_test = self.feature_extractor_test.calculate_features_all_speakers()
        df_test, labels_test = get_dataset_from_df(
            df_feats_test, label_type, self.features
        )
        X_train = df_train.to_numpy()
        y_train = labels_train.to_numpy()
        X_test_all = df_test.to_numpy()
        y_test_all = labels_test.to_numpy()

        if upsample:
            X_train, y_train = SMOTE(k_neighbors=3, random_state=42).fit_resample(
                X_train, y_train
            )

        
        if covariate_disc:
            X=pd.DataFrame(X_train)
            Z=pd.DataFrame(X_test_all)
            X['is_z'] = 0 # 0 means test set
            Z['is_z'] = 1 # 1 means training set
            XZ = pd.concat( [X, Z], ignore_index=True, axis=0 )

            labels = XZ['is_z'].values
            XZ = XZ.drop('is_z', axis=1).values
            X, Z = X.values, Z.values
            clf = RandomForestClassifier(max_depth=1)
            predictions = np.zeros(labels.shape)
            skf = StratifiedKFold(n_splits=20, shuffle=True, random_state=1234)
            for fold, (train_idx, test_idx) in enumerate(skf.split(XZ, labels)):
                print (f'Training discriminator model for fold {str(fold)}')
                X_train_cov, X_test_cov = XZ[train_idx], XZ[test_idx]
                y_train_cov, y_test_cov = labels[train_idx], labels[test_idx]
                    
                clf.fit(X_train_cov, y_train_cov)
                probs = clf.predict_proba(X_test_cov)[:, 1]
                predictions[test_idx] = probs
            print("classifier auc: ", roc_auc_score(labels,predictions))


            # first, isolate the training part (recall we joined them above)
            predictions_Z = predictions[len(X):]
            predictions_X = predictions[:len(X)]
            predictions_X+=10**-6
            weights_X=(1./predictions_X) - 1. 
            weights_X/= np.mean(weights_X) 
            weights = (1./predictions_Z) - 1. 
            weights /= np.mean(weights) # we do this to re-normalize the computed log-loss



        # scaler = RobustScaler()
        scaler = StandardScaler()

        # print("positive percent after upsample: ",sum(y_train)/len(y_train)*100)
        X_train = scaler.fit_transform(X_train)

        # print("Data Length",len(y_test))
        label_names = get_label_names(label_type)
        if label_type == "aq_gr":
            y_train = y_train - 2
            y_test_all = y_test_all - 2
        if label_type == "aq_fr":
            y_train = y_train - 1
            y_test_all = y_test_all - 1
            upsample = False

        if folds == "loso":
            kf = LeaveOneOut()
        elif folds > 1:
            # if model_name=="mlp":
            #     kf = StratifiedKFold(n_splits=folds)
            # else:
            # # kf = KFold(n_splits=folds,shuffle=False, random_state=None)
            kf = RepeatedStratifiedKFold(n_splits=folds, n_repeats=10, random_state=42)
        elif folds <= 1:
            "Need at least folds=2 to work"
            exit()
        kf_split = list(kf.split(X=X_test_all, y=y_test_all))
        print("test length: ", len(kf_split[0][1]))

        conf_matrix_list_of_arrays = []
        accuracy_list = []
        # bacc_list=[]
        recall_list = []
        precision_list = []
        f1_list = []
        roc_list = []
        acc_roc_list = []

        for train, test in kf_split:
            if model_name == "svm":
                clf = SVC(gamma="auto", probability=True)
            elif model_name == "xgboost":
                clf = XGBClassifier()
            elif model_name == "tree":
                clf = tree.DecisionTreeClassifier()
            elif model_name == "random_forest":
                clf = RandomForestClassifier()
            elif model_name == "mlp":
                clf = MLPClassifier(
                    # tune batch size later
                    batch_size=64,
                    # keep random state constant to accurately compare subsequent models
                    random_state=42,
                    activation="logistic",
                    learning_rate="constant",
                    solver="lbfgs",
                    hidden_layer_sizes=(100,),
                    max_iter=1000,
                )
            # X_train, y_train = X[train], y[train]

            # # print("positive percent: ",sum(y_train)/len(y_train)*100)
            # if upsample:

            #     X_train, y_train = SMOTE(k_neighbors=3,random_state=42).fit_resample(X_train, y_train)
            # scaler = RobustScaler()
            # print("positive percent after upsample: ",sum(y_train)/len(y_train)*100)
            # scaler = StandardScaler()
            # X_train = scaler.fit_transform(X_train)

            X_test, y_test = X_test_all[test], y_test_all[test]
            X_test = scaler.transform(X_test)
            if ot:
                X_train_ot, y_train_ot = X_test_all[train], y_test_all[train]
                from adapt.instance_based import KMM

                X_train_ot = scaler.transform(X_train_ot)
                clf = KMM(
                    estimator=clf,
                    Xt=X_train_ot,
                    kernel="rbf",  # Gaussian kernel
                    gamma=1.0,  # Bandwidth of the kernel
                    verbose=0,
                    random_state=0,
                )

                # optimal_transport = OptimalAdaptation(algorithm="emd")

                # X_el_transformed = optimal_transport.fit_and_transform(X_el, X_en)
                # optimal_transport.ot.fit(Xs=X_train_ot, Xt=X_train, ys=y_train_ot, yt=y_train)
                # X_test=optimal_transport.ot.transform(Xs=X_test, ys=y_test)
            if covariate_disc:
                # weights_test=weights[test]
                clf.fit(X_train,y_train,sample_weight=weights_X)
            else:
                clf.fit(X_train, y_train)
            clf_preds = clf.predict(X_test)
            if not ot:
                clf_probas = clf.predict_proba(X_test)
            else:
                clf_probas = clf.estimator_.predict_proba(X_test)
            acc = clf.score(X_test, y_test)
            accuracy_list.append(acc)
            f1 = f1_score(y_test, clf_preds, zero_division=0, average="weighted")
            f1_list.append(f1)
            precision = precision_score(
                y_test,
                clf_preds,
                labels=label_names,
                average="weighted",
                zero_division=0,
            )
            precision_list.append(precision)
            recall = recall_score(
                y_test,
                clf_preds,
                labels=label_names,
                average="weighted",
                zero_division=0,
            )
            recall_list.append(recall)

            conf_matrix = confusion_matrix(y_test, clf_preds, labels=label_names)
            conf_matrix_list_of_arrays.append(conf_matrix)

            if not folds == "loso":
                if label_type == "aq" or label_type == "aq_fr":
                    roc_au = roc_auc_score(y_test, clf_probas, multi_class="ovo")
                else:
                    # roc_au = roc_auc_score(
                    #     y_test, clf_probas[:, 1], labels=label_names, multi_class="ovr"
                    # )
                    # roc_list.append(roc_au)
                    if threshold_tuning:
                        x_train_thres, y_train_thres = X_test_all[train], y_test_all[train]
                        x_train_thres = scaler.transform(x_train_thres)
                        # clf_preds_thres = clf.predict(x_train_thres)
                        if ot:
                            clf_probas_thres = clf.estimator_.predict_proba(x_train_thres)
                        else:
                            clf_probas_thres = clf.predict_proba(x_train_thres)
                        fpr, tpr, thresholds = roc_curve(
                            y_train_thres, clf_probas_thres[:, 1]
                        )
                        i = np.arange(len(tpr))  # index for df
                        roc = pd.DataFrame(
                            {
                                "tf": pd.Series(tpr - (1 - fpr), index=i),
                                "threshold": pd.Series(thresholds, index=i),
                            }
                        )
                        roc_t = roc.iloc[(roc.tf - 0).abs().argsort()[:1]]
                        threshold = list(roc_t["threshold"])[0]
                        # print("optimal threshold: ", threshold)
                        auc_preds = [
                            (lambda x: 1 if x > threshold else 0)(x)
                            for x in clf_probas[:, 1]
                        ]
                        f1_roc = f1_score(y_test, auc_preds)
                        acc_roc = accuracy_score(y_test, auc_preds)
                        acc_roc_list.append(acc_roc)
                        # roc_auc = auc(fpr, tpr)
                    
                    # print("f1 roc: ", f1_roc)
                    # print("acc roc: ", acc_roc)
                    # print("")
        mean_accuracy = np.mean(accuracy_list)
        sd_accuracy = np.std(accuracy_list)
        # balanced_accuracy = sum(bacc_list) / len(bacc_list)
        conf_matrix = np.sum(conf_matrix_list_of_arrays, axis=0) / len(
            conf_matrix_list_of_arrays
        )
        precision = np.mean(precision_list)
        recall = np.mean(recall_list)
        mean_acc = round(mean_accuracy * 100, 2)
        std_acc = round(sd_accuracy * 100, 2)
        mean_f1 = round(np.mean(f1_list) * 100, 2)
        std_f1 = round(np.std(f1_list) * 100, 2)

        print(f"Model: {model_name} - Accuracy: {mean_acc} +- {std_acc}")
        print(f"F1: {mean_f1} +- {std_f1} ")
        print(f"Confusion Matrix:")
        print(conf_matrix)
        if acc_roc_list:
            print("ROC_ACC: ", np.mean(acc_roc_list), " +- ", np.std(acc_roc_list))
        # if (not folds == "loso") or ot:
        #     mean_auc = round(np.mean(roc_list) * 100, 2)
        #     std_auc = round(np.std(roc_list) * 100, 2)

        #     print(f"ROC AUC: {mean_auc} +- {std_auc}")
        #     # print(f"Precision: {precision}, Recall: {recall}")
        #     # print(f"Balanced accuracy: {balanced_accuracy}")

        #     return mean_acc, std_acc, mean_f1, std_f1, mean_auc, std_auc
        # else:
        return mean_acc, std_acc, mean_f1, std_f1

    def conduct_experiment_ot(
        self, upsample=True, model_name="xgboost", label_type="control"
    ):
        data = TEDxMultilingualDatasetEnEl()
        # X_el, X_en = data.get_features()
        feats_names = data.df_feats_el.columns
        X_el = data.df_feats_el.to_numpy()
        X_en = data.df_feats_en.to_numpy()
        optimal_transport = OptimalAdaptation(algorithm="emd")

        # X_el_transformed = optimal_transport.fit_and_transform(X_el, X_en)
        optimal_transport.ot.fit(Xs=X_el, Xt=X_en)

        features = self.features
        label_names = get_label_names(label_type)
        # if label_type == "aq_gr":
        #     y = y - 2
        # if label_type=="aq_fr":
        #     y=y-1
        #     upsample=False

        df_feats_train = self.feature_extractor_train.calculate_features_all_speakers()
        df_train, labels_train = get_dataset_from_df(
            df_feats_train, label_type, features
        )
        X_train = df_train.to_numpy()
        y_train = labels_train.to_numpy()
        df_feats_test = self.feature_extractor_test.calculate_features_all_speakers()
        df_test, labels_test = get_dataset_from_df(df_feats_test, label_type, features)
        X_test = df_test.to_numpy()
        y_test = labels_test.to_numpy()
        X_test = optimal_transport.ot.transform(X_test)
        if label_type == "aq_gr":
            y_test = y_test - 2
        if label_type == "aq_fr":
            y_test = y_test - 1
            upsample = False

        # X_train, y_train = X[train], y[train]
        if upsample:
            X_train, y_train = SMOTE(k_neighbors=3, random_state=42).fit_resample(
                X_train, y_train
            )
        scaler = StandardScaler()
        X_train = scaler.fit_transform(X_train)
        # X_test, y_test = X[test], y[test]
        X_test = scaler.transform(X_test)
        if model_name == "svm":
            clf = SVC(gamma="auto", probability=True)
        elif model_name == "xgboost":
            clf = XGBClassifier()
        elif model_name == "tree":
            clf = tree.DecisionTreeClassifier()
        elif model_name == "mlp":
            clf = MLPClassifier(
                # tune batch size later
                batch_size="auto",
                # keep random state constant to accurately compare subsequent models
                random_state=42,
                activation="logistic",
                learning_rate="adaptive",
                solver="adam",
                hidden_layer_sizes=(300,),
                max_iter=1000,
            )
        clf.fit(X_train, y_train)
        clf_preds = clf.predict(X_test)
        clf_probas = clf.predict_proba(X_test)
        acc = clf.score(X_test, y_test)
        f1 = f1_score(y_test, clf_preds, zero_division=0, average="weighted")
        conf_matrix = confusion_matrix(y_test, clf_preds, labels=label_names)
        if label_type == "aq" or label_type == "aq_fr":
            roc_au = roc_auc_score(y_test, clf_probas, multi_class="ovo")
        else:
            roc_au = roc_auc_score(
                y_test, clf_probas[:, 1], labels=label_names, multi_class="ovr"
            )
        print(f"Accuracy: {round(acc*100,2)}")
        print("F1 score: ", round(f1*100,2))
        print("AUROC: ", roc_au)
        print(conf_matrix)
        return acc, f1, roc_au

    


def unilingual_baselines_feats_smaller(df_greek, df_english, df_french):
    df_results = pd.DataFrame()
    feature_set = FEATURES_NAMES
    folds = 5
    label_type = "aq"
    if label_type == "aq":
        df_greek = df_greek[df_greek["control"] == False]
        df_english = df_english[df_english["control"] == False]
        df_french = df_french[df_french["control"] == False]
        label_type_gr = "aq_gr"
        label_type_fr = "aq_fr"
    else:
        label_type_gr = label_type
        label_type_fr = label_type
    print("Baselines for SMALL feature set, on ORACLE transcripts")
    transcript_column_name = "asr_transcription_whisper_target"

    print("GREEK - HA")
    exp = ExperimentCV(
        df_greek, transcript_column_name, language="el", features=feature_set
    )
    mean_acc, std_acc, mean_f1, std_f1, mean_auc, std_auc = exp.conduct_experiment(
        model_name="svm", label_type=label_type_gr, folds=folds
    )
    df_results["Greek - HA"] = [
        str(mean_acc) + " +- " + str(std_acc),
        str(mean_f1) + " +- " + str(std_f1),
        str(mean_auc) + " +- " + str(std_auc),
    ]

    # mean_acc,std_acc,mean_f1,std_f1,mean_auc,std_auc=exp.conduct_experiment(model_name="random_forest",label_type=label_type,folds=folds)
    # mean_acc,std_acc,mean_f1,std_f1,mean_auc,std_auc=exp.conduct_experiment(model_name="mlp",label_type=label_type,folds=folds)
    print("FRENCH - HA")

    exp = ExperimentCV(
        df_french, transcript_column_name, language="fr", features=feature_set
    )
    mean_acc, std_acc, mean_f1, std_f1, mean_auc, std_auc = exp.conduct_experiment(
        model_name="svm", label_type=label_type_fr, folds=folds
    )
    df_results["French - HA"] = [
        str(mean_acc) + " +- " + str(std_acc),
        str(mean_f1) + " +- " + str(std_f1),
        str(mean_auc) + " +- " + str(std_auc),
    ]
    # # exp.conduct_experiment(model_name="random_forest",label_type=label_type,folds=folds)
    # # exp.conduct_experiment(model_name="mlp",label_type=label_type,folds=folds)
    # print("ENGLISH - HA")

    exp = ExperimentCV(
        df_english, transcript_column_name, language="en", features=feature_set
    )
    mean_acc, std_acc, mean_f1, std_f1, mean_auc, std_auc = exp.conduct_experiment(
        model_name="svm", label_type=label_type, folds=folds
    )
    df_results["English - HA"] = [
        str(mean_acc) + " +- " + str(std_acc),
        str(mean_f1) + " +- " + str(std_f1),
        str(mean_auc) + " +- " + str(std_auc),
    ]
    # exp.conduct_experiment(model_name="random_forest",label_type=label_type,folds=folds)
    # exp.conduct_experiment(model_name="mlp",label_type=label_type,folds=folds)

    transcript_column_name = "asr_transcription_whisper"

    print("GREEK - ASR")
    exp = ExperimentCV(
        df_greek, transcript_column_name, language="el", features=feature_set
    )
    mean_acc, std_acc, mean_f1, std_f1, mean_auc, std_auc = exp.conduct_experiment(
        model_name="svm", label_type=label_type_gr, folds=folds
    )
    df_results["Greek - ASR"] = [
        str(mean_acc) + " +- " + str(std_acc),
        str(mean_f1) + " +- " + str(std_f1),
        str(mean_auc) + " +- " + str(std_auc),
    ]

    # mean_acc,std_acc,mean_f1,std_f1,mean_auc,std_auc=exp.conduct_experiment(model_name="random_forest",label_type=label_type_gr,folds=folds)
    # mean_acc,std_acc,mean_f1,std_f1,mean_auc,std_auc=exp.conduct_experiment(model_name="mlp",label_type=label_type_gr,folds=folds)
    # print("FRENCH - ASR")

    # exp=ExperimentCV(df_french,transcript_column_name,language="fr",features=feature_set)
    # mean_acc,std_acc,mean_f1,std_f1,mean_auc,std_auc=exp.conduct_experiment(model_name="svm",label_type=label_type,folds=folds)
    # df_results["French - ASR"]=[str(mean_acc)+" +- " +str(std_acc),str(mean_f1)+" +- "+ str(std_f1),str(mean_auc)+" +- "+ str(std_auc)]
    # exp.conduct_experiment(model_name="random_forest",label_type=label_type,folds=folds)
    # exp.conduct_experiment(model_name="mlp",label_type=label_type,folds=folds)
    print("ENGLISH - ASR")

    exp = ExperimentCV(
        df_english, transcript_column_name, language="en", features=feature_set
    )
    mean_acc, std_acc, mean_f1, std_f1, mean_auc, std_auc = exp.conduct_experiment(
        model_name="svm", label_type=label_type_fr, folds=folds
    )
    df_results["English - ASR"] = [
        str(mean_acc) + " +- " + str(std_acc),
        str(mean_f1) + " +- " + str(std_f1),
        str(mean_auc) + " +- " + str(std_auc),
    ]
    # exp.conduct_experiment(model_name="random_forest",label_type=label_type,folds=folds)
    # exp.conduct_experiment(model_name="mlp",label_type=label_type,folds=folds)

    df_results = df_results.T
    df_results.columns = ["Accuracy", "F1", "AUROC"]
    print(df_results.to_latex())


def crosslingual_experiments(df_greek, df_english, df_french):
    experiment_basic = True
    experiment_cv = False
    experiment_ot = False

    df_results = pd.DataFrame()
    feature_set = FEATURES_NAMES
    # folds = 5
    label_type = "aq"
    model_name = "xgboost"
    folds = 5
    ot = False
    low_res_language = "el"
    

    if label_type == "aq":
        df_greek = df_greek[df_greek["control"] == False]
        df_english = df_english[df_english["control"] == False]
        df_french = df_french[df_french["control"] == False]
        label_type_gr = "aq_gr"
        label_type_fr = "aq_fr"
    else:
        label_type_gr = label_type
        label_type_fr = label_type

    if low_res_language == "el":
        label_type = label_type_gr
        language_name = "Greek"
        df_to_use = df_greek
    elif low_res_language == "fr":
        label_type=label_type_fr
        language_name = "French"
        df_to_use = df_french
    print("Feature set: ", feature_set)
    print(f"{language_name} to English translation from ASR")

    experiment = CrosslingualExperiment(
        df_english,
        df_to_use,
        low_res_language="en",
        transcript_column_name="asr_transcription_whisper",
        transcript_column_name_low_res="asr_translation_whisper",
        features=feature_set,
    )
    print("Crosslingual Experiment")

    if experiment_basic:
        acc, f1, roc_auc = experiment.conduct_experiment(
            label_type=label_type, model_name=model_name
        )
        df_results[f"{language_name} to English Translation"] = [str(acc), str(f1),str(roc_auc)]
    if experiment_cv:
        print("Experiment CV")
        if ot:
            print("With OT")
        mean_acc, std_acc, mean_f1, std_f1 = experiment.conduct_experiment_cv(
            label_type=label_type, model_name=model_name, folds=folds, ot=ot
        )
        df_results[f"{language_name} to English Translation"] = [
            str(mean_acc) + " +- " + str(std_acc),
            str(mean_f1) + " +- " + str(std_f1),
        ]
    if experiment_ot:
        print("OT Crosslingual Exp")

        acc, f1, roc_auc = experiment.conduct_experiment_ot(
            label_type=label_type, model_name=model_name
        )
        df_results[f"{language_name} to English Translation"] = [str(acc), str(f1),str(roc_auc)]
    print(f"{language_name} to English ASR transcripts")

    experiment = CrosslingualExperiment(
        df_english,
        df_to_use,
        low_res_language=low_res_language,
        transcript_column_name="asr_transcription_whisper",
        transcript_column_name_low_res="asr_transcription_whisper",
        features=feature_set,
    )
    if experiment_basic:
        acc, f1, roc_auc = experiment.conduct_experiment(
            label_type=label_type, model_name=model_name
        )
        df_results[f"{language_name} ASR Transcripts"] = [str(acc), str(f1),str(roc_auc)]

    if experiment_cv:
        print("Experiment CV")
        if ot:
            print("With OT")
        mean_acc, std_acc, mean_f1, std_f1 = experiment.conduct_experiment_cv(
            label_type=label_type, model_name=model_name, folds=folds, ot=ot
        )
        df_results[f"{language_name} ASR Transcripts"] = [
            str(mean_acc) + " +- " + str(std_acc),
            str(mean_f1) + " +- " + str(std_f1),
        ]
    if experiment_ot:
        print("OT Crosslingual Exp")

        acc, f1, roc_auc = experiment.conduct_experiment_ot(
            label_type=label_type, model_name=model_name
        )
        df_results[f"{language_name} ASR Transcripts"] = [str(acc), str(f1),str(roc_auc)]

    print(f"{language_name} to English HA transcripts")

    experiment = CrosslingualExperiment(
        df_english,
        df_to_use,
        low_res_language=low_res_language,
        transcript_column_name="asr_transcription_whisper_target",
        transcript_column_name_low_res="asr_transcription_whisper_target",
        features=feature_set,
    )
    if experiment_basic:
        acc, f1, roc_auc = experiment.conduct_experiment(
            label_type=label_type, model_name=model_name
        )
        df_results[f"{language_name} HA Transcripts"] = [str(acc), str(f1),str(roc_auc)]

    if experiment_cv:
        print("Experiment CV")
        if ot:
            print("With OT")
        mean_acc, std_acc, mean_f1, std_f1 = experiment.conduct_experiment_cv(
            label_type=label_type, model_name=model_name, folds=folds, ot=ot
        )
        df_results[f"{language_name} HA Transcripts"] = [
            str(mean_acc) + " +- " + str(std_acc),
            str(mean_f1) + " +- " + str(std_f1),
        ]
    if experiment_ot:
        print("OT Crosslingual Exp")

        acc, f1, roc_auc = experiment.conduct_experiment_ot(
            label_type=label_type, model_name=model_name
        )
        df_results[f"{language_name} HA Transcripts"] = [str(acc), str(f1),str(roc_auc)]
        
    df_results = df_results.T
    if not experiment_cv:
        df_results.columns = ["Accuracy", "F1", "AUROC"]
    else:
        df_results.columns=["Accuracy", "F1"]
    print(df_results.to_latex())


if __name__ == "__main__":
    # feats_csv_path_test="datasets/ab_english/test_feats.csv"
    # feats_csv_path_train="datasets/ab_english/train_feats.csv"
    # feats_csv_path_val="datasets/ab_english/val_feats.csv"
    # # zeroshot_experiment_csv(feats_csv_path_train,feats_csv_path_test,model="xgboost")
    # feats_csv_path_control="datasets/ab_english/control_feats.csv"

    csv_path_english = "datasets/ab_english/all_data.csv"
    csv_path_greek = "datasets/planv_gr/planv_all.csv"
    csv_path_french = "crosslingual-aphasia-assessment/data_csv/all_data_fr.csv"
    df_english = pd.read_csv(csv_path_english)
    df_greek = pd.read_csv(csv_path_greek)
    df_french = pd.read_csv(csv_path_french)
    # # run_all_experiments(df_greek,df_english)

    csv_path_english="datasets/ab_english/all_data.csv"
    csv_path_greek="datasets/planv_gr/planv_all.csv"
    df_english=pd.read_csv(csv_path_english)
    df_greek=pd.read_csv(csv_path_greek)
    # df_greek=df_greek[df_greek["control"]==False]
    # df_english=df_english[df_english["control"]==False]
    experiment = CrosslingualExperiment(
        df_english,
        df_greek,
        low_res_language="en",
        # transcript_column_name="asr_transcription_whisper",
        # transcript_column_name_low_res="asr_translation_whisper",
        transcript_column_name="clean_transcription_v1",
        transcript_column_name_low_res="clean_transcription_v1",
        features=FEATURES_NAMES
    ).conduct_experiment(label_type="control")

    # transcript_column_name="asr_transcription_whisper_target"
    # transcript_column_name="clean_transcription_v1"
    # df_greek=df_greek[df_greek["control"]==False]
    # df_english=df_english[df_english["control"]==False]
    # exp=ExperimentCV(df_greek,transcript_column_name,language="el",features=FEATURES_NAMES)
    # exp=ExperimentCV(df_french,transcript_column_name,language="fr",features=FEATURES_NAMES_SMALLER)
    # exp=ExperimentCV(df_english,transcript_column_name,language="en",features=FEATURES_NAMES)
    # exp.conduct_experiment(model_name="forest",label_type="control",folds=10)

    # unilingual_baselines_feats_smaller(df_greek, df_english, df_french)

    # crosslingual_experiments(df_greek, df_english, df_french)
