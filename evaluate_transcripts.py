
# import torch
# import torchaudio
from transcript_cleaning import clean_f_l
from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor
from transformers import Wav2Vec2ProcessorWithLM
import re
import pandas as pd
from datasets import Dataset,load_metric
from utils import csv_to_dataset, get_kenlm_decoder, df_to_dataset
from transformers import WhisperProcessor, WhisperForConditionalGeneration

def prepare_dataset(batch):
    audio = batch["audio"]

    # batched output is "un-batched"
    batch["input_values"] = processor(audio["array"], sampling_rate=audio["sampling_rate"]).input_values[0]
    batch["input_length"] = len(batch["input_values"])
    
    with processor.as_target_processor():
        batch["labels"] = processor(batch["sentence"]).input_ids
    return batch

def prepare_dataset_whisper(batch):
    audio = batch["audio"]

    # batched output is "un-batched"
    # batch["input_values"] = processor(audio["array"], sampling_rate=audio["sampling_rate"]).input_values[0]
    batch["input_values"] = processor(audio["array"], sampling_rate=audio["sampling_rate"]).input_features
    batch["input_length"] = len(batch["input_values"])
    
    # with processor.as_target_processor():
        # batch["labels"] = processor(batch["sentence"]).input_ids
    batch["labels"] = processor(text=batch["sentence"]).input_ids
    return batch

def evaluate_sample(sample,model,processor):
    sample_prep=prepare_dataset(sample)

    input_dict = processor(sample_prep["input_values"],sampling_rate=16000, return_tensors="pt", padding=True)

    logits = model(input_dict.input_values.to("cuda")).logits

    pred_ids = torch.argmax(logits, dim=-1)[0]

    print("Prediction:")
    print(processor.decode(pred_ids))

    print("\nReference:")
    print(sample["sentence"].lower())

def evaluate_test_set(dataset,model,processor,save_to_path):

    wer_metric = load_metric("wer")
    # if whisper:    
    #     dataset = dataset.map(prepare_dataset_whisper, remove_columns=dataset.column_names)
    # else:
    #     dataset = dataset.map(prepare_dataset, remove_columns=dataset.column_names)

    def map_to_result(batch):
        input_values = processor(batch["audio"]["array"],sampling_rate=batch["audio"]["sampling_rate"], return_tensors="pt").input_values.to("cuda")

        with torch.no_grad():
            # input_values = torch.tensor(batch["input_values"], device="cuda")
            logits = model(input_values).logits

        pred_ids = torch.argmax(logits, dim=-1)
        batch["pred_str"] = processor.batch_decode(pred_ids)[0]
        labels=processor(text=batch["sentence"]).input_ids
        batch["text"] = processor.decode(labels, group_tokens=False)
        
        return batch
    
    results = dataset.map(map_to_result, remove_columns=dataset.column_names)
    print("Test WER: {:.3f}".format(wer_metric.compute(predictions=results["pred_str"], references=results["text"])))
    results.save_to_disk(save_to_path)
    df=results.to_pandas()
    df.to_json("whisper.json")

def evaluate_test_set_whisper(dataset,model,processor,save_to_path):

    wer_metric = load_metric("wer")

    def map_to_pred(batch):

        input_features = processor.feature_extractor(batch["audio"]["array"],sampling_rate=batch["audio"]["sampling_rate"], return_tensors="pt").input_features.to("cuda")

        predicted_ids=model.generate(input_features)

        # transcription = processor.batch_decode(predicted_ids, normalize = True)
        transcription = processor.tokenizer.batch_decode(predicted_ids, skip_special_tokens=True, normalize=True)[0]
        batch['text'] = processor.tokenizer._normalize(batch['sentence'])

        batch["transcription"] = transcription

        return batch
    
    results = dataset.map(map_to_pred, remove_columns=dataset.column_names)
    results.save_to_disk(save_to_path)
    df=results.to_pandas()
    
    df.to_json("whisper.json")
    df=df.dropna()
    print("Test WER: {:.3f}".format(wer_metric.compute(predictions=df["transcription"], references=df["text"])))
    

def evaluate_test_set_with_lm(dataset,model,processor_with_lm, save_to_path):

    wer_metric = load_metric("wer")
    
    dataset = dataset.map(prepare_dataset, remove_columns=dataset.column_names)
    
    def map_to_result(batch):
        with torch.no_grad():
            # print(batch.keys())
            print(len(batch["input_values"]))
            # print(batch["input_values"])

            # print(torch.tensor(batch["input_values"], device="cuda").shape)
            # exit()
            input_values = torch.tensor(batch["input_values"], device="cuda").unsqueeze(0)
            # print(input_values.shape)
            logits = model(input_values).logits
            print(logits.shape)
        logits=logits.detach().cpu().numpy().squeeze()
        print(logits.shape)
        batch["pred_str"] = processor_with_lm.decode(
                        logits
                    ).text

        # pred_ids = torch.argmax(logits, dim=-1)
        # batch["pred_str"] = processor.batch_decode(pred_ids)[0]
        # batch["text"] = processor_with_lm.decode(batch["labels"])
        # with processor_with_lm.as_target_processor():
        batch['text']=processor_with_lm.tokenizer.decode(batch['labels'])
        return batch
    

    results = dataset.map(map_to_result, remove_columns=dataset.column_names)
    print("Test WER: {:.3f}".format(wer_metric.compute(predictions=results["pred_str"], references=results["text"])))

    results.save_to_disk(save_to_path)

def show_wer_from_results(results_dir):
    from datasets import load_from_disk
    results=load_from_disk(results_dir)
    print("Test WER: {:.3f}".format(wer_metric.compute(predictions=results["pred_str"], references=results["text"])))
    
def save_csv_from_dataset(results_dir):
    from datasets import load_from_disk
    results=load_from_disk(results_dir)
    
    print("Test WER: {:.3f}".format(wer_metric.compute(predictions=results["pred_str"], references=results["text"])))
    
def evaluate_dataset_df(df,target_transcript_column,asr_transcript_column,asr_len_hack=False):
    wer_metric = load_metric("wer")
    MODEL_ID = "openai/whisper-small"
    processor = WhisperProcessor.from_pretrained(MODEL_ID)
    
    df[target_transcript_column] = df[target_transcript_column].map(processor.tokenizer._normalize)
    df[target_transcript_column]=df[target_transcript_column].map(clean_f_l)
    df=df.drop(df[df[target_transcript_column]==""].index)

    if asr_len_hack:
        max_len=max(df[target_transcript_column].map(len))
        df["word_counts"]=df[target_transcript_column].str.split().str.len()
        max_word_count=max(df["word_counts"])
        df["asr_len"]=df[asr_transcript_column].map(len)
        df=df[df["asr_len"]<max_len]
    wer_metric=wer_metric.compute(predictions=df[asr_transcript_column], references=df[target_transcript_column])
    print("Test WER: {:.3f}".format(wer_metric))
    print(df[[asr_transcript_column,target_transcript_column]].tail(200))

def evaluate_dataset_csv_by_participant(csv_path,target_transcript_column,asr_transcript_column):
    MODEL_ID = "openai/whisper-small"
    processor = WhisperProcessor.from_pretrained(MODEL_ID)
    wer_metric = load_metric("wer")
    df=pd.read_csv(csv_path)
    df[target_transcript_column] = df[target_transcript_column].map(processor.tokenizer._normalize)
    df[target_transcript_column]=df[target_transcript_column].map(clean_f_l)
    for participant in df["Participant ID"].unique():
        df_p=df[df["Participant ID"]==participant]
        aq=df_p["WAB_AQ"].iloc[0]
        wer_score=wer_metric.compute(predictions=df_p[asr_transcript_column], references=df_p[target_transcript_column])
        print(f"Test WER for {participant} with AQ {aq}"+": {:.3f}".format(wer_score))
        # print(df[[asr_transcript_column,target_transcript_column]].tail(200))
if __name__=='__main__':
    import argparse
    
    parser = argparse.ArgumentParser(description="ASR Evaluation Arguments")
    parser.add_argument(
        "--dataset_path",
        type=str,
        default="datasets/ab_english",
        help="Path to dataset dir",
    )
    parser.add_argument(
        "--model",
        type=str,
        default="jonatasgrosman/wav2vec2-large-xlsr-53-english",
        help='Huggingface hub model id or local path to model'
    )
    parser.add_argument(
        "--use_lm",
        type=str,
        default=False,
        help='Whether to use a language model for decoding'
    )
    parser.add_argument(
        "--use_whisper",
        type=str,
        default=True,
        help='Whether to use a language model for decoding'
    )
    args=parser.parse_args()

    dataset_path='datasets/ab_english'
    csv_path=f'{dataset_path}/test.csv'

    df=pd.read_csv(csv_path)

    evaluate_dataset_df(df,"clean_transcription_v1","asr_transcription_whisper",asr_len_hack=True)
    exit()
    df=pd.read_csv(csv_path)

    # dataset=csv_to_dataset(csv_path,new_dataset_path=dataset_path)
    # dataset=Dataset(dataset[:50])
    
    # df=df[:50]
    dataset=df_to_dataset(df,new_dataset_path=dataset_path)
    # print(df.head())
    # print(len(df))
    # print(dataset.keys())
    # exit()
    if args.use_whisper:
        MODEL_ID = "openai/whisper-small"
        processor = WhisperProcessor.from_pretrained(MODEL_ID)
        model = WhisperForConditionalGeneration.from_pretrained(MODEL_ID).to("cuda")
        save_to_path="./test_whisper"
        evaluate_test_set_whisper(dataset,model,processor,save_to_path)

    else:
        MODEL_ID = "jonatasgrosman/wav2vec2-large-xlsr-53-english"
        processor = Wav2Vec2Processor.from_pretrained(MODEL_ID)
        model = Wav2Vec2ForCTC.from_pretrained(MODEL_ID).to("cuda")

        # sample=dataset[3]
        if args.use_lm:
            vocab_dict = processor.tokenizer.get_vocab()
            kenlm_model_path='5gram_correct.arpa'
            kenlm_decoder = get_kenlm_decoder(
                vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path
            )
            processor_with_lm = Wav2Vec2ProcessorWithLM(
                feature_extractor=processor.feature_extractor,
                tokenizer=processor.tokenizer,
                decoder=kenlm_decoder,
            )
        

            save_to_path="./test_asr_lm"

            evaluate_test_set_with_lm(dataset,model,processor_with_lm,save_to_path)
        else:
            save_to_path="./test_asr"

            evaluate_test_set(dataset,model,processor,save_to_path)
# exit()
def get_aphasiabank_asr_transcriptions(
    data_dict,
    save_dict_name,
    add_language_model=False,
    kenlm_model_path=None,
):
    

    MODEL_ID = "jonatasgrosman/wav2vec2-large-xlsr-53-english"
    processor = Wav2Vec2Processor.from_pretrained(MODEL_ID)
    model = Wav2Vec2ForCTC.from_pretrained(MODEL_ID).to("cuda")
    BATCH_SIZE = 4
    issue_speakers = []

    if add_language_model:
        from transformers import Wav2Vec2ProcessorWithLM

        vocab_dict = processor.tokenizer.get_vocab()
        kenlm_decoder = get_kenlm_decoder(
            vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path
        )
        processor_with_lm = Wav2Vec2ProcessorWithLM(
            feature_extractor=processor.feature_extractor,
            tokenizer=processor.tokenizer,
            decoder=kenlm_decoder,
        )
    print("Speaker processing started")
    for idx, speaker in enumerate(data_dict.keys()):
        # if idx <= 81:
        #     continue
        print(f"Speaker {speaker} under processing")
        timeit_start = datetime.now()
        audio_path = data_dict[speaker]["user_info"]["audio_path"]
        for story in data_dict[speaker]["stories"]:
            speech_arrays = []
            utterance_names = []

            for utt_name in data_dict[speaker]["stories"][story]["utterances"]:
                utt_dict = data_dict[speaker]["stories"][story]["utterances"][utt_name]
                if "asr_prediction" in utt_dict:
                    continue
                t_start, t_end = timestamp_to_ints(utt_dict["timestamp"])
                offset = t_start / 1000
                duration = (t_end - t_start) / 1000
                if duration > 100:
                    print(utt_name)
                    continue
                speech_array, sampling_rate = librosa.load(
                    audio_path, sr=16_000, offset=offset, duration=duration
                )
                # # TEMP CODE: DELETE AFTER TESTING
                # soundfile.write("temp_audio/"+utt_name+".wav", speech_array, 16000)
                # # END TEMP
                if speech_array.shape[0] == 0:
                    continue
                utterance_names.append(utt_name)
                speech_arrays.append(speech_array)
            if utterance_names == []:
                continue
            batches = [
                speech_arrays[i : i + BATCH_SIZE]
                for i in range(0, len(speech_arrays), BATCH_SIZE)
            ]
            total_predicted_sentences = []
            for batch in batches:
                try:
                    inputs = processor(
                        batch,
                        sampling_rate=16_000,
                        return_tensors="pt",
                        padding=True,
                    ).to("cuda")
                    with torch.no_grad():
                        logits = model(
                            inputs.input_values, attention_mask=inputs.attention_mask
                        ).logits
                except:
                    inputs = processor(
                        batch,
                        sampling_rate=16_000,
                        return_tensors="pt",
                        padding=True,
                    )
                    model.to("cpu")
                    with torch.no_grad():
                        logits = model(
                            inputs.input_values,
                            attention_mask=inputs.attention_mask,
                        ).logits
                    model.to("cuda")
                    # exit("Exception raised")
                if add_language_model:
                    # Decoding using language models (beam search etc)
                    predicted_sentences = processor_with_lm.batch_decode(
                        logits.cpu().numpy()
                    ).text
                else:
                    # Greedy Decoding
                    predicted_ids = torch.argmax(logits, dim=-1)
                    predicted_sentences = processor.batch_decode(predicted_ids)
                total_predicted_sentences += predicted_sentences
            for utterance_name, prediction in zip(
                utterance_names, total_predicted_sentences
            ):
                data_dict[speaker]["stories"][story]["utterances"][utterance_name][
                    "asr_prediction"
                ] = prediction
        timeit_end = datetime.now()
        print(
            f"Speaker {idx}/{len(data_dict)} \t Took time: {timeit_end- timeit_start}"
        )
        save_dict(data_dict, save_dict_name)

