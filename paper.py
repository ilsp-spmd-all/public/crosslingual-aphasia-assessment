from utils import (
    load_dict,
    save_dict,
    create_name_value_dict,
    fetch_transcriptions_from_elan_file,
    timestamp_to_ints,
    normalize_text,
    reformat_sentence,
)
import re
import string
import soundfile as sf
import librosa
import tqdm
from dependency_parser import (
    EnglishConlluParser,
    FrenchConlluParser,
    MandarinConlluParser,
)
from feature_extraction import SpeakerLevelFeatureExtractor
from feature_names import UTTERANCE_LEVEL_FEATURE_NAMES, FEATURES_NAMES, HELPER_NAMES
import numpy as np
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import KFold, StratifiedKFold, LeaveOneOut
from sklearn.utils import shuffle
from xgboost import XGBClassifier
from datetime import datetime
from time import sleep
import os
import glob
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
import librosa
import soundfile

empty_file_speakers = [
    "wright18a-Control",
    "MMA03a-Aphasia",
    "wright14a-Control",
    "wright13a-Control",
    "wright28a-Control",
    "wright15a-Control",
    "MMA21a-Aphasia",
    "wright21a-Control",
    "wright31a-Control",
    "wright49a-Control",
    "wright50a-Control",
    "wright27a-Control",
    "wright30a-Control",
    "MMA20a-Aphasia",
    "adler07a-Aphasia",
    "wright48a-Control",
]


def create_features_dict(data_dict, use_asr_transcripts, save_dict_name, language):
    if use_asr_transcripts:
        transcript_type = "asr_prediction"
        conllu_name = "conllu-asr"
    else:
        transcript_key = "target_transcript"
        conllu_name = "conllu"

    features_dict = data_dict
    speaker_level_feature_extractor = SpeakerLevelFeatureExtractor(
        language=language,
        stack_utterances_durations=False,
        use_asr_transcripts=use_asr_transcripts,
    )
    for speaker in data_dict.keys():
        (
            speaker_features,
            speaker_helpers,
        ) = speaker_level_feature_extractor.feature_extraction_speaker_level(
            data_dict[speaker], conllu_name=conllu_name
        )
        speaker_helpers_dict = create_name_value_dict(speaker_helpers, HELPER_NAMES)
        speaker_features_dict = create_name_value_dict(speaker_features, FEATURES_NAMES)
        features_dict[speaker]["features"] = speaker_features_dict
        features_dict[speaker]["helpers"] = speaker_helpers_dict
    save_dict(features_dict, save_dict_name)
    return features_dict


def delete_conllu(data_dict):
    for speaker in data_dict:
        for story in list(data_dict[speaker]["stories"]):
            for utterance in list(data_dict[speaker]["stories"][story]["utterances"]):
                if "conllu" in list(data_dict[speaker]["stories"][story]["utterances"]):
                    del data_dict[speaker]["stories"][story]["utterances"]["conllu"]
    return data_dict


def calculate_conllu_english(
    data_dict,
    save_dict_name,
    key_target="processed_transcript",
    conllu_name="conllu",
):
    english_parser = EnglishConlluParser()
    for speaker in tqdm.tqdm(data_dict.keys()):
        for story in data_dict[speaker]["stories"].keys():
            print(f"Processing {speaker}-{story}")
            for utterance_name in data_dict[speaker]["stories"][story]["utterances"].keys():
                utterance_dict = data_dict[speaker]["stories"][story]["utterances"][utterance_name]
                if key_target not in utterance_dict.keys():
                    # Some utterances may not have asr transcriptions
                    continue
                if utterance_dict[key_target] == "":
                    # Continue if transcript is empty string
                    continue
                conllu = english_parser.calculate_conllu_from_sentence(
                    utterance_dict[key_target]
                )
                data_dict[speaker]["stories"][story]["utterances"][utterance_name][
                    conllu_name
                ] = conllu
    save_dict(data_dict, save_dict_name)


def calculate_conllu_french(
    data_dict,
    save_dict_name,
    key_target="processed_transcript",
    conllu_name="conllu",
):
    french_parser = FrenchConlluParser()
    for speaker in data_dict:
        for story in data_dict[speaker]["stories"]:
            print(f"Processing {speaker}-{story}")
            for utterance_name in data_dict[speaker]["stories"][story]["utterances"]:
                utterance_dict = data_dict[speaker]["stories"][story]["utterances"][utterance_name]
                if key_target not in utterance_dict:
                    # Some utterances may not have asr transcriptions
                    continue
                if utterance_dict[key_target] == "":
                    # Continue if transcript is empty string
                    continue
                conllu = french_parser.calculate_conllu_from_sentence(
                    utterance_dict[key_target]
                )
                data_dict[speaker]["stories"][story]["utterances"][utterance_name][
                    conllu_name
                ] = conllu
    save_dict(data_dict, save_dict_name)


def calculate_conllu_mandarin(
    data_dict,
    save_dict_name,
    key_target="processed_transcript",
    conllu_name="conllu",
):
    french_parser = MandarinConlluParser()
    for speaker in data_dict:
        for story in data_dict[speaker]["stories"]:
            print(f"Processing {speaker}-{story}")
            for utterance_name in data_dict[speaker]["stories"][story]["utterances"]:
                utterance_dict = data_dict[speaker]["stories"][story]["utterances"][utterance_name]
                if key_target not in utterance_dict:
                    # Some utterances may not have asr transcriptions
                    continue
                if utterance_dict[key_target] == "":
                    # Continue if transcript is empty string
                    continue
                conllu = french_parser.calculate_conllu_from_sentence(
                    utterance_dict[key_target]
                )
                data_dict[speaker]["stories"][story]["utterances"][utterance_name][
                    conllu_name
                ] = conllu
    save_dict(data_dict, save_dict_name)


def get_dataset(data_dict, label_aphasia=True):
    X, y = [], []
    for speaker in data_dict.keys():
        group = data_dict[speaker]["user_info"]["group"]
        if label_aphasia:
            if group == "aphasia":
                label = 1
            elif group in ["control", "controls", "cardiacs"]:
                label = 0
            else:
                exit("Problem with group name - label")
        else:
            if "aq_score" not in data_dict[speaker]["user_info"].keys():
                continue
            aq_score = data_dict[speaker]["user_info"]["aq_score"]
            if not aq_score:
                # If there is no aq score continue
                continue
            if aq_score > 90:
                label = 0
            elif aq_score > 75:
                label = 1
            elif aq_score > 50:
                label = 2
            else:
                label = 3
        y.append(label)
        features = list(data_dict[speaker]["features"].values())
        X.append(features)
    return np.array(X), np.array(y)


def only_english_experiment_k_fold(data_dict, n_splits=5):
    X, y = get_dataset(data_dict)

    kf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=42)
    nan_ids_from_X = np.any(np.isnan(X), axis=0)
    X = X[:, ~nan_ids_from_X]
    clf = make_pipeline(StandardScaler(), SVC(gamma="auto", kernel="linear"))

    accuracy_list = []
    idx = 0
    kf_split = kf.split(X=X, y=y)

    for train, test in kf_split:
        idx += 1
        X_train, y_train = X[train], y[train]
        X_test, y_test = X[test], y[test]
        clf = make_pipeline(StandardScaler(), SVC(gamma="auto", kernel="linear"))
        clf.fit(X_train, y_train)
        y_preds = clf.predict(X_test)
        acc = clf.score(X_test, y_test)
        accuracy_list.append(acc)
    accuracy = sum(accuracy_list) / len(accuracy_list)
    return accuracy


def loso_english_experiment(data_dict, label_aphasia=True):
    X, y = get_dataset(data_dict, label_aphasia)

    kf = LeaveOneOut()
    nan_ids_from_X = np.any(np.isnan(X), axis=0)
    X = X[:, ~nan_ids_from_X]
    clf = make_pipeline(StandardScaler(), SVC(gamma="auto", kernel="linear"))

    accuracy_list = []
    f1_list = []
    idx = 0
    kf_split = kf.split(X=X)

    for model_name in ["svm", "xgboost", "tree"]:
        for train, test in kf_split:
            idx += 1
            X_train, y_train = X[train], y[train]
            X_test, y_test = X[test], y[test]
            if model_name == "svm":
                clf = make_pipeline(StandardScaler(), SVC(gamma="auto"))
            elif model_name == "xgboost":
                clf = XGBClassifier()
            elif model_name == "tree":
                clf = tree.DecisionTreeClassifier()
            clf.fit(X_train, y_train)
            y_preds = clf.predict(X_test)
            acc = clf.score(X_test, y_test)
            # f1 = f1_score(y_test, y_preds, zero_division=1)
            accuracy_list.append(acc)
            # f1_list.append(f1)
        accuracy = sum(accuracy_list) / len(accuracy_list)
        # f1 = sum(f1_list) / len(f1_list)
        print(f"Model: {model_name} - Accuracy: {accuracy}")
    return accuracy


def zeroshot_experiment(train_dict, test_dict, model="xgboost"):
    X_train, y_train = get_dataset(train_dict)
    X_test, y_test = get_dataset(test_dict)

    nan_ids_from_X_train = np.any(np.isnan(X_train), axis=0)

    X_train = X_train[:, ~nan_ids_from_X_train]
    nan_ids_from_X_test = np.any(np.isnan(X_test), axis=0)
    X_test = X_test[:, ~nan_ids_from_X_test]
    print(X_train.shape, X_test.shape)

    english_scaler = StandardScaler()
    X_train = english_scaler.fit_transform(X_train)

    if model == "xgboost":
        xgb_classifier = XGBClassifier()
        xgb_classifier.fit(X_train, y_train)

        X_test = english_scaler.transform(X_test)
        print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = xgb_classifier.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = xgb_classifier.score(X_test, y_test)
        print(acc)

        weights = xgb_classifier.get_booster().get_score(importance_type="weight")
        from feature_names import FEATURES_NAMES

        for idx, weight_key in enumerate(weights):
            print(
                f"Feature {FEATURES_NAMES[idx].ljust(30)} \t weight={weights[weight_key]}"
            )
    elif model == "svm":
        clf = SVC(gamma="auto")
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)
    elif model == "tree":
        clf = tree.DecisionTreeClassifier()
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)
    elif model == "forest":
        clf = RandomForestClassifier(max_depth=2, random_state=0)
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)
    elif model == "adaboost":
        clf = AdaBoostClassifier(n_estimators=100, random_state=0)
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        # print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = clf.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = clf.score(X_test, y_test)
        print(acc)


def create_planv_data(root, save_dict_name="planv_data.json"):
    eaf_files = glob.glob(f"{root}/*/**.eaf")
    data_dict = {}
    for eaf in eaf_files:
        tmp = eaf.split("/")[-1].split("_")
        speaker_id = tmp[0]
        story = tmp[1]
        if speaker_id not in data_dict.keys():
            data_dict[speaker_id] = {"user_info": {"group": "control"}}
        wav_files = [
            f
            for f in os.listdir(os.path.join(root, speaker_id))
            if (
                (speaker_id in f)
                and (story in f)
                and (not ("_aud" in f))
                and (f.endswith(".wav"))
            )
        ]
        wav_file = os.path.join(root, speaker_id, wav_files[0])
        data_dict[speaker_id][story] = {
            "story_info": {"transcription_path": eaf, "audio_path": wav_file},
            "utterances": {},
        }
    save_dict(data_dict, save_dict_name)


def get_planv_oracle_transcriptions(
    data_dict,
    save_dict_name="planv_data.json",
    tier='TIER_ID="*PAR"',
):
    for speaker in data_dict:
        for story in data_dict[speaker]["stories"]:
            eaf_fp = data_dict[speaker]["stories"][story]["story_info"]["transcription_path"]
            (
                utterances,
                t_start,
                t_end,
                durations,
            ) = fetch_transcriptions_from_elan_file(eaf_fp, tier)
            for idx, utterance in enumerate(utterances):
                data_dict[speaker]["stories"][story]["utterances"][f"{speaker}-{story}-{idx}"] = {
                    "processed_transcript": utterance,
                    "timestamp": f"{t_start[idx]}_{t_end[idx]}",
                    "duration": durations[idx],
                }
    save_dict(data_dict, save_dict_name)


def asr_evaluation(
    data_dict,
    target_key="processed_transcript",
    prediction_key="asr_prediction",
    per_category=False,
):
    from datasets import load_metric

    wer = load_metric("wer")
    cer = load_metric("cer")

    if per_category:
        ref_aphasia, pred_aphasia = [], []
        ref_control, pred_control = [], []
    references, predictions = [], []
    for speaker in data_dict.keys():
        for story in data_dict[speaker]["stories"]:
            for utt_name in data_dict[speaker]["stories"][story]["utterances"]:
                utt_dict = data_dict[speaker]["stories"][story]["utterances"][utt_name]
                if prediction_key not in utt_dict.keys():
                    # AphasiaBank: some transcripts are missing
                    # print("ASR transcription not found")
                    continue
                reference = normalize_text(utt_dict[target_key])
                prediction = normalize_text(utt_dict[prediction_key])
                if reference == "":
                    continue
                references.append(reference)
                predictions.append(prediction)
                if per_category:
                    if data_dict[speaker]["user_info"]["group"] in [
                        "Aphasia",
                        "aphasia",
                    ]:
                        ref_aphasia.append(reference)
                        pred_aphasia.append(prediction)
                    else:
                        ref_control.append(reference)
                        pred_control.append(prediction)
    if per_category:
        wer_aphasia = wer.compute(references=ref_aphasia, predictions=pred_aphasia)
        cer_aphasia = cer.compute(references=ref_aphasia, predictions=pred_aphasia)
        wer_control = wer.compute(references=ref_control, predictions=pred_control)
        cer_control = cer.compute(references=ref_control, predictions=pred_control)
        print(f"Aphasia: WER={wer_aphasia} CER={cer_aphasia}")
        print(f"Control: WER={wer_control} CER={cer_control}")
    wer_result = wer.compute(references=references, predictions=predictions)
    cer_result = cer.compute(references=references, predictions=predictions)
    print(f"Total: WER={wer_result} CER={cer_result}")
    return wer_result, cer_result


def calculate_conllus_ilsp(
    data_dict,
    key_target="processed_transcript",
    conllu_name="conllu",
    save_dict_name="planv_data.json",
):
    import requests

    for speaker in data_dict:
        for story in data_dict[speaker]["stories"]:
            utterances, utt_names = [], []

            for utt_name in data_dict[speaker]["stories"][story]["utterances"]:
                if (
                    key_target
                    not in data_dict[speaker]["stories"][story]["utterances"][utt_name]
                ):
                    continue
                t = data_dict[speaker]["stories"][story]["utterances"][utt_name][key_target]
                if t == "":
                    continue

                def preprocess_text(t):
                    t = t.translate(str.maketrans("", "", string.punctuation))
                    t = t.capitalize()
                    return t + "."

                t = preprocess_text(t)
                utt_names.append(utt_name)
                utterances.append(t)
            if utterances == []:
                continue
            text = "\n\n".join(utterances)
            response = requests.post(
                "http://nlp.ilsp.gr/nws/api/", data={"text": text}, timeout=5
            ).json()
            sentences = text.split("\n\n")
            conllus = response["conllu"].split("\n\n")
            if conllus[-1] == "":
                conllus = conllus[0:-1]
            for idx, (utt_name, utterance, conllu) in enumerate(
                zip(utt_names, utterances, conllus)
            ):
                data_dict[speaker]["stories"][story]["utterances"][utt_name][conllu_name] = conllu
    save_dict(data_dict, save_dict_name)


def delete_keys(data_dict, save_dict_name, keys=[]):
    if keys:
        for speaker in data_dict:
            for story in data_dict[speaker]["stories"]:
                if "utterances" not in data_dict[speaker]["stories"][story]:
                    continue
                story_dict = data_dict[speaker]["stories"][story]["utterances"]
                for utt_name in story_dict:
                    utt_dict = story_dict[utt_name]
                    for key in keys:
                        if key in utt_dict:
                            del data_dict[speaker]["stories"][story]["utterances"][utt_name][key]
        save_dict(data_dict, save_dict_name)


# def get_greek_asr_transcriptions(
#     data_dict,
#     save_dict_name,
#     logits_path,
#     model_id,
#     add_language_model=False,
#     kenlm_model_path=None,
# ):
#     import torch
#     import torchaudio
#     from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor
#     import re

#     processor = Wav2Vec2Processor.from_pretrained(model_id)
#     model = Wav2Vec2ForCTC.from_pretrained(model_id).to("cuda")
#     BATCH_SIZE = 4
#     issue_speakers = []

#     if add_language_model:
#         from transformers import Wav2Vec2ProcessorWithLM

#         vocab_dict = processor.tokenizer.get_vocab()
#         kenlm_decoder = get_kenlm_decoder(
#             vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path
#         )
#         processor_with_lm = Wav2Vec2ProcessorWithLM(
#             feature_extractor=processor.feature_extractor,
#             tokenizer=processor.tokenizer,
#             decoder=kenlm_decoder,
#         )
#     print("Speaker processing started")
#     for idx, speaker in enumerate(data_dict.keys()):
#         print(f"Speaker {speaker} under processing")
#         timeit_start = datetime.now()
#         for story in data_dict[speaker].keys():
#             if story in ["user_info", "features", "helpers"]:
#                 continue
#             audio_path = data_dict[speaker][story]["story_info"]["audio_path"]
#             if not audio_path:
#                 # if missing file is missing
#                 continue
#             speech_arrays = []
#             utterance_names = []

#             for utt_name in data_dict[speaker][story]["utterances"]:
#                 utt_dict = data_dict[speaker][story]["utterances"][utt_name]
#                 # if "asr_prediction" in utt_dict.keys():
#                 #     continue
#                 t_start, t_end = timestamp_to_ints(utt_dict["timestamp"])
#                 offset = t_start / 1000
#                 duration = (t_end - t_start) / 1000
#                 # if duration > 100:
#                 #     print(utt_name)
#                 #     continue
#                 speech_array, sampling_rate = librosa.load(
#                     audio_path, sr=16_000, offset=offset, duration=duration
#                 )
#                 if speech_array.shape[0] == 0:
#                     continue
#                 utterance_names.append(utt_name)
#                 speech_arrays.append(speech_array)
#             if utterance_names == []:
#                 continue
#             batches = [
#                 speech_arrays[i : i + BATCH_SIZE]
#                 for i in range(0, len(speech_arrays), BATCH_SIZE)
#             ]
#             batches_names = [
#                 utterance_names[i : i + BATCH_SIZE]
#                 for i in range(0, len(utterance_names), BATCH_SIZE)
#             ]
#             total_predicted_sentences = []
#             for batch, batch_names in zip(batches, batches_names):
#                 try:
#                     inputs = processor(
#                         batch,
#                         sampling_rate=16_000,
#                         return_tensors="pt",
#                         padding=True,
#                     ).to("cuda")
#                     with torch.no_grad():
#                         logits = model(
#                             inputs.input_values, attention_mask=inputs.attention_mask
#                         ).logits
#                 except:
#                     inputs = processor(
#                         batch,
#                         sampling_rate=16_000,
#                         return_tensors="pt",
#                         padding=True,
#                     )
#                     model.to("cpu")
#                     with torch.no_grad():
#                         logits = model(
#                             inputs.input_values,
#                             attention_mask=inputs.attention_mask,
#                         ).logits
#                     model.to("cuda")
#                     # exit("Exception raised")
#                 for j, b_name in enumerate(batch_names):
#                     tmp = logits[j].cpu().numpy()
#                     np.save(f"{logits_path}/{b_name}.npy", tmp)
#             #     if add_language_model:
#             #         # Decoding using language models (beam search etc)
#             #         predicted_sentences = processor_with_lm.batch_decode(
#             #             logits.cpu().numpy()
#             #         ).text
#             #     else:
#             #         # Greedy Decoding
#             #         predicted_ids = torch.argmax(logits, dim=-1)
#             #         predicted_sentences = processor.batch_decode(predicted_ids)
#             #     total_predicted_sentences += predicted_sentences
#             # for utterance_name, prediction in zip(
#             #     utterance_names, total_predicted_sentences
#             # ):
#             #     data_dict[speaker][story]["utterances"][utterance_name][
#             #         "asr_prediction"
#             #     ] = prediction
#         timeit_end = datetime.now()
#         print(
#             f"Speaker {idx}/{len(data_dict)} \t Took time: {timeit_end- timeit_start}"
#         )
#         # save_dict(data_dict, save_dict_name)


def get_aphasiabank_asr_transcriptions(
    data_dict,
    save_dict_name,
    add_language_model=False,
    kenlm_model_path=None,
):
    import torch
    import torchaudio
    from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor
    import re

    MODEL_ID = "jonatasgrosman/wav2vec2-large-xlsr-53-english"
    processor = Wav2Vec2Processor.from_pretrained(MODEL_ID)
    model = Wav2Vec2ForCTC.from_pretrained(MODEL_ID).to("cuda")
    BATCH_SIZE = 4
    issue_speakers = []

    if add_language_model:
        from transformers import Wav2Vec2ProcessorWithLM

        vocab_dict = processor.tokenizer.get_vocab()
        kenlm_decoder = get_kenlm_decoder(
            vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path
        )
        processor_with_lm = Wav2Vec2ProcessorWithLM(
            feature_extractor=processor.feature_extractor,
            tokenizer=processor.tokenizer,
            decoder=kenlm_decoder,
        )
    print("Speaker processing started")
    for idx, speaker in enumerate(data_dict.keys()):
        # if idx <= 81:
        #     continue
        print(f"Speaker {speaker} under processing")
        timeit_start = datetime.now()
        audio_path = data_dict[speaker]["user_info"]["audio_path"]
        for story in data_dict[speaker]["stories"]:
            speech_arrays = []
            utterance_names = []

            for utt_name in data_dict[speaker]["stories"][story]["utterances"]:
                utt_dict = data_dict[speaker]["stories"][story]["utterances"][utt_name]
                if "asr_prediction" in utt_dict:
                    continue
                t_start, t_end = timestamp_to_ints(utt_dict["timestamp"])
                offset = t_start / 1000
                duration = (t_end - t_start) / 1000
                if duration > 100:
                    print(utt_name)
                    continue
                speech_array, sampling_rate = librosa.load(
                    audio_path, sr=16_000, offset=offset, duration=duration
                )
                # # TEMP CODE: DELETE AFTER TESTING
                # soundfile.write("temp_audio/"+utt_name+".wav", speech_array, 16000)
                # # END TEMP
                if speech_array.shape[0] == 0:
                    continue
                utterance_names.append(utt_name)
                speech_arrays.append(speech_array)
            if utterance_names == []:
                continue
            batches = [
                speech_arrays[i : i + BATCH_SIZE]
                for i in range(0, len(speech_arrays), BATCH_SIZE)
            ]
            total_predicted_sentences = []
            for batch in batches:
                try:
                    inputs = processor(
                        batch,
                        sampling_rate=16_000,
                        return_tensors="pt",
                        padding=True,
                    ).to("cuda")
                    with torch.no_grad():
                        logits = model(
                            inputs.input_values, attention_mask=inputs.attention_mask
                        ).logits
                except:
                    inputs = processor(
                        batch,
                        sampling_rate=16_000,
                        return_tensors="pt",
                        padding=True,
                    )
                    model.to("cpu")
                    with torch.no_grad():
                        logits = model(
                            inputs.input_values,
                            attention_mask=inputs.attention_mask,
                        ).logits
                    model.to("cuda")
                    # exit("Exception raised")
                if add_language_model:
                    # Decoding using language models (beam search etc)
                    predicted_sentences = processor_with_lm.batch_decode(
                        logits.cpu().numpy()
                    ).text
                else:
                    # Greedy Decoding
                    predicted_ids = torch.argmax(logits, dim=-1)
                    predicted_sentences = processor.batch_decode(predicted_ids)
                total_predicted_sentences += predicted_sentences
            for utterance_name, prediction in zip(
                utterance_names, total_predicted_sentences
            ):
                data_dict[speaker]["stories"][story]["utterances"][utterance_name][
                    "asr_prediction"
                ] = prediction
        timeit_end = datetime.now()
        print(
            f"Speaker {idx}/{len(data_dict)} \t Took time: {timeit_end- timeit_start}"
        )
        save_dict(data_dict, save_dict_name)


def get_french_aphasiabank_asr_transcriptions(
    data_dict,
    save_dict_name,
    add_language_model=False,
    kenlm_model_path=None,
):
    import torch
    import torchaudio
    from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor
    import re

    MODEL_ID = "jonatasgrosman/wav2vec2-large-xlsr-53-french"
    processor = Wav2Vec2Processor.from_pretrained(MODEL_ID)
    model = Wav2Vec2ForCTC.from_pretrained(MODEL_ID).to("cuda")
    BATCH_SIZE = 4
    issue_speakers = []

    if add_language_model:
        from transformers import Wav2Vec2ProcessorWithLM

        vocab_dict = processor.tokenizer.get_vocab()
        kenlm_decoder = get_kenlm_decoder(
            vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path
        )
        processor_with_lm = Wav2Vec2ProcessorWithLM(
            feature_extractor=processor.feature_extractor,
            tokenizer=processor.tokenizer,
            decoder=kenlm_decoder,
        )
    print("Speaker processing started")
    for idx, speaker in enumerate(data_dict.keys()):
        print(f"Speaker {speaker} under processing")
        timeit_start = datetime.now()
        audio_path = data_dict[speaker]["user_info"]["converted_audio_path"]
        for story in data_dict[speaker]["stories"]:
            speech_arrays = []
            utterance_names = []

            for utt_name in data_dict[speaker]["stories"][story]["utterances"]:
                utt_dict = data_dict[speaker]["stories"][story]["utterances"][utt_name]
                if "asr_prediction" in utt_dict.keys():
                    continue
                t_start, t_end = timestamp_to_ints(utt_dict["timestamp"])
                offset = t_start / 1000
                duration = (t_end - t_start) / 1000
                if duration > 100:
                    print(utt_name)
                    continue
                speech_array, sampling_rate = librosa.load(
                    audio_path, sr=16_000, offset=offset, duration=duration
                )
                if speech_array.shape[0] == 0:
                    continue
                utterance_names.append(utt_name)
                speech_arrays.append(speech_array)
            if utterance_names == []:
                continue
            batches = [
                speech_arrays[i : i + BATCH_SIZE]
                for i in range(0, len(speech_arrays), BATCH_SIZE)
            ]
            batches_names = [
                utterance_names[i : i + BATCH_SIZE]
                for i in range(0, len(utterance_names), BATCH_SIZE)
            ]
            total_predicted_sentences = []
            for batch, batch_names in zip(batches, batches_names):
                try:
                    inputs = processor(
                        batch,
                        sampling_rate=16_000,
                        return_tensors="pt",
                        padding=True,
                    ).to("cuda")
                    with torch.no_grad():
                        logits = model(
                            inputs.input_values, attention_mask=inputs.attention_mask
                        ).logits
                except:
                    inputs = processor(
                        batch,
                        sampling_rate=16_000,
                        return_tensors="pt",
                        padding=True,
                    )
                    model.to("cpu")
                    with torch.no_grad():
                        logits = model(
                            inputs.input_values,
                            attention_mask=inputs.attention_mask,
                        ).logits
                    model.to("cuda")
                    # exit("Exception raised")
                # for j, b_name in enumerate(batch_names):
                #     tmp = logits[j].cpu().numpy()
                #     np.save(f"logits-french/{b_name}.npy", tmp)
                # continue
                if add_language_model:
                    # Decoding using language models (beam search etc)
                    predicted_sentences = processor_with_lm.batch_decode(
                        logits.cpu().numpy()
                    ).text
                else:
                    # Greedy Decoding
                    predicted_ids = torch.argmax(logits, dim=-1)
                    predicted_sentences = processor.batch_decode(predicted_ids)
                total_predicted_sentences += predicted_sentences
            for utterance_name, prediction in zip(
                utterance_names, total_predicted_sentences
            ):
                data_dict[speaker]["stories"][story]["utterances"][utterance_name][
                    "asr_prediction"
                ] = prediction
        timeit_end = datetime.now()
        print(
            f"Speaker {idx}/{len(data_dict)} \t Took time: {timeit_end- timeit_start}"
        )
        save_dict(data_dict, save_dict_name)


def create_kenlm_txt():
    from datasets import load_dataset

    target_lang = "en"
    dataset = load_dataset("europarl_bilingual", lang1="el", lang2="en", split="train")
    chars_to_ignore_regex = '[,?.!\-\;\:"“%‘”�—’…–]'
    import re

    def extract_text(batch, target_lang="en"):
        text = batch["translation"][target_lang]
        batch["text"] = re.sub(chars_to_ignore_regex, "", text.lower())
        return batch

    dataset = dataset.map(extract_text, remove_columns=dataset.column_names)
    with open("text.txt", "w") as f:
        f.write(" ".join(dataset["text"]))


def fix_kenlm_arpa(model_name):
    with open(f"{model_name}.arpa", "r") as read_file, open(
        f"{model_name}_correct.arpa", "w"
    ) as write_file:
        has_added_eos = False
        for line in read_file:
            if not has_added_eos and "ngram 1=" in line:
                count = line.strip().split("=")[-1]
                write_file.write(line.replace(f"{count}", f"{int(count)+1}"))
            elif not has_added_eos and "<s>" in line:
                write_file.write(line)
                write_file.write(line.replace("<s>", "</s>"))
                has_added_eos = True
            else:
                write_file.write(line)


def get_kenlm_decoder(vocab_dict, kenlm_model_path, to_lower=True):
    from pyctcdecode import build_ctcdecoder

    if to_lower:
        sorted_vocab_dict = {
            k.lower(): v
            for k, v in sorted(vocab_dict.items(), key=lambda item: item[1])
        }
    else:
        sorted_vocab_dict = {
            k: v for k, v in sorted(vocab_dict.items(), key=lambda item: item[1])
        }
    decoder = build_ctcdecoder(
        labels=list(sorted_vocab_dict.keys()),
        kenlm_model_path=kenlm_model_path,
    )
    return decoder


def decoding_process(
    data_dict,
    kenlm_model_path,
    save_dict_name,
    add_language_model,
    logits_path="logits",
    beam_width=4,
    num_processes=10,
):
    import torch
    import torchaudio
    from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor
    import re

    MODEL_ID = "jonatasgrosman/wav2vec2-large-xlsr-53-english"
    processor = Wav2Vec2Processor.from_pretrained(MODEL_ID)
    # model = Wav2Vec2ForCTC.from_pretrained(MODEL_ID).to("cuda")
    issue_speakers = []

    if add_language_model:
        from transformers import Wav2Vec2ProcessorWithLM

        vocab_dict = processor.tokenizer.get_vocab()
        kenlm_decoder = get_kenlm_decoder(
            vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path
        )
        processor_with_lm = Wav2Vec2ProcessorWithLM(
            feature_extractor=processor.feature_extractor,
            tokenizer=processor.tokenizer,
            decoder=kenlm_decoder,
        )

    for idx, speaker in enumerate(data_dict.keys()):
        if speaker in empty_file_speakers:
            continue
        print(f"Speaker {speaker} under processing")
        timeit_start = datetime.now()
        audio_path = data_dict[speaker]["user_info"]["converted_audio_path"]
        for story in data_dict[speaker]["stories"]:
            speech_arrays = []
            utterance_names = []
            total_predicted_sentences = []

            tmp_logits_list, dimensions = [], []
            max_dim = 0
            for idx2, utt_name in enumerate(data_dict[speaker]["stories"][story]["utterances"]):
                if not os.path.isfile(f"logits/{utt_name}.npy"):
                    # If numpy array does not exist continue process
                    continue
                tmp_logit = np.load(f"logits/{utt_name}.npy")
                tmp_logits_list.append(tmp_logit)
                utterance_names.append(utt_name)
                dimensions.append(tmp_logit.shape[0])

            if dimensions == []:
                print(f"Issue with story {story}: no dimensions")
                continue
            # zero padding
            max_dim = max(dimensions)
            padded_logits_list = []
            for l in tmp_logits_list:
                extra_pad = np.zeros((max_dim - l.shape[0], l.shape[1]))
                l = np.append(l, extra_pad, axis=0)
                padded_logits_list.append(l)
            logits = np.array(padded_logits_list)

            if add_language_model:
                # Decoding using language models (beam search etc)
                predicted_sentences = processor_with_lm.batch_decode(
                    logits,
                    beam_width=beam_width,
                    num_processes=num_processes,
                ).text
            else:
                # Greedy Decoding
                predicted_ids = torch.argmax(
                    torch.Tensor(logits), dim=-1
                )  # probably needs other dimensions
                predicted_sentences = processor.batch_decode(predicted_ids)
            for idx3, utt_name in enumerate(utterance_names):
                data_dict[speaker]["stories"][story]["utterances"][utt_name][
                    "asr_prediction"
                ] = predicted_sentences[idx3]

        timeit_end = datetime.now()
        time_took = timeit_end - timeit_start
        print(f"Speaker {idx}/{len(data_dict)} \t Took time: {time_took}")
        save_dict(data_dict, save_dict_name)


def decoding_process_greek(
    data_dict,
    kenlm_model_path,
    save_dict_name,
    add_language_model,
    model_id,
    beam_width=4,
    num_processes=10,
    logits_path="logits-planv",
):
    import torch
    import torchaudio
    from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor, Wav2Vec2CTCTokenizer
    import re

    processor = Wav2Vec2Processor.from_pretrained(model_id)
    # model = Wav2Vec2ForCTC.from_pretrained(model_id).to("cuda")
    issue_speakers = []

    if add_language_model:
        from transformers import Wav2Vec2ProcessorWithLM

        vocab_dict = processor.tokenizer.get_vocab()
        kenlm_decoder = get_kenlm_decoder(
            vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path, to_lower=False
        )
        processor_with_lm = Wav2Vec2ProcessorWithLM(
            feature_extractor=processor.feature_extractor,
            tokenizer=processor.tokenizer,
            decoder=kenlm_decoder,
        )

    for idx, speaker in enumerate(data_dict.keys()):
        if speaker in empty_file_speakers:
            continue
        print(f"Speaker {speaker} under processing")
        timeit_start = datetime.now()
        for story in data_dict[speaker]["stories"]:
            speech_arrays = []
            utterance_names = []
            total_predicted_sentences = []
            audio_path = data_dict[speaker]["stories"][story]["story_info"]["audio_path"]

            tmp_logits_list, dimensions = [], []
            max_dim = 0
            for idx2, utt_name in enumerate(data_dict[speaker]["stories"][story]["utterances"]):
                if not os.path.isfile(f"{logits_path}/{utt_name}.npy"):
                    # If numpy array does not exist continue process
                    continue
                tmp_logit = np.load(f"{logits_path}/{utt_name}.npy")
                tmp_logits_list.append(tmp_logit)
                utterance_names.append(utt_name)
                dimensions.append(tmp_logit.shape[0])

            if dimensions == []:
                print(f"Issue with story {story}: no dimensions")
                continue
            # zero padding
            max_dim = max(dimensions)
            padded_logits_list = []
            for l in tmp_logits_list:
                extra_pad = np.zeros((max_dim - l.shape[0], l.shape[1]))
                l = np.append(l, extra_pad, axis=0)
                padded_logits_list.append(l)
            logits = np.array(padded_logits_list)

            if add_language_model:
                # Decoding using language models (beam search etc)
                predicted_sentences = processor_with_lm.batch_decode(
                    logits,
                    beam_width=beam_width,
                    num_processes=num_processes,
                ).text
            else:
                # Greedy Decoding
                predicted_ids = torch.argmax(
                    torch.Tensor(logits), dim=-1
                )  # probably needs other dimensions
                predicted_sentences = processor.batch_decode(predicted_ids)
            for idx3, utt_name in enumerate(utterance_names):
                data_dict[speaker]["stories"][story]["utterances"][utt_name][
                    "asr_prediction"
                ] = predicted_sentences[idx3].lower()

        timeit_end = datetime.now()
        time_took = timeit_end - timeit_start
        print(f"Speaker {idx}/{len(data_dict)} \t Took time: {time_took}")
        save_dict(data_dict, save_dict_name)


def decoding_process_french(
    data_dict,
    kenlm_model_path,
    save_dict_name,
    add_language_model,
    logits_path="logits-french",
    beam_width=4,
    num_processes=10,
):
    import torch
    import torchaudio
    from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor
    import re

    MODEL_ID = "jonatasgrosman/wav2vec2-large-xlsr-53-french"
    processor = Wav2Vec2Processor.from_pretrained(MODEL_ID)
    # model = Wav2Vec2ForCTC.from_pretrained(MODEL_ID).to("cuda")
    issue_speakers = []

    if add_language_model:
        from transformers import Wav2Vec2ProcessorWithLM

        vocab_dict = processor.tokenizer.get_vocab()
        # del vocab_dict["<s>"]
        # del vocab_dict["</s>"]

        kenlm_decoder = get_kenlm_decoder(
            vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path
        )

        processor_with_lm = Wav2Vec2ProcessorWithLM(
            feature_extractor=processor.feature_extractor,
            tokenizer=processor.tokenizer,
            decoder=kenlm_decoder,
        )

    for idx, speaker in enumerate(data_dict.keys()):
        if speaker in empty_file_speakers:
            continue
        print(f"Speaker {speaker} under processing")
        audio_path = data_dict[speaker]["user_info"]["converted_audio_path"]
        timeit_start = datetime.now()
        for story in data_dict[speaker]["stories"]:
            speech_arrays = []
            utterance_names = []
            total_predicted_sentences = []

            tmp_logits_list, dimensions = [], []
            max_dim = 0
            for idx2, utt_name in enumerate(data_dict[speaker]["stories"][story]["utterances"]):
                if not os.path.isfile(f"{logits_path}/{utt_name}.npy"):
                    # If numpy array does not exist continue process
                    continue
                tmp_logit = np.load(f"{logits_path}/{utt_name}.npy")
                tmp_logits_list.append(tmp_logit)
                utterance_names.append(utt_name)
                dimensions.append(tmp_logit.shape[0])

            if dimensions == []:
                print(f"Issue with story {story}: no dimensions")
                continue
            # zero padding
            max_dim = max(dimensions)
            padded_logits_list = []
            for l in tmp_logits_list:
                extra_pad = np.zeros((max_dim - l.shape[0], l.shape[1]))
                l = np.append(l, extra_pad, axis=0)
                padded_logits_list.append(l)
            logits = np.array(padded_logits_list)

            if add_language_model:
                # Decoding using language models (beam search etc)
                predicted_sentences = processor_with_lm.batch_decode(
                    logits,
                    beam_width=beam_width,
                    num_processes=num_processes,
                ).text
            else:
                # Greedy Decoding
                predicted_ids = torch.argmax(
                    torch.Tensor(logits), dim=-1
                )  # probably needs other dimensions
                predicted_sentences = processor.batch_decode(predicted_ids)
            for idx3, utt_name in enumerate(utterance_names):
                data_dict[speaker]["stories"][story]["utterances"][utt_name][
                    "asr_prediction"
                ] = predicted_sentences[idx3]

        timeit_end = datetime.now()
        time_took = timeit_end - timeit_start
        print(f"Speaker {idx}/{len(data_dict)} \t Took time: {time_took}")
        save_dict(data_dict, save_dict_name)


def clean_asr(data_dict, save_dict_name):
    for speaker in data_dict.keys():
        for story in data_dict[speaker]["stories"]:
            for utt_name in data_dict[speaker]["stories"][story]["utterances"]:
                utt_dict = data_dict[speaker]["stories"][story]["utterances"][utt_name]
                if "asr_prediction" not in utt_dict.keys():
                    continue
                asr_prediction = utt_dict["asr_prediction"]
                asr_prediction = asr_prediction.replace("<s>", "")
                data_dict[speaker]["stories"][story]["utterances"][utt_name][
                    "asr_prediction"
                ] = asr_prediction
    save_dict(data_dict, save_dict_name)


def create_initial_data_dict_aphasiabank(
    language,
    transcripts_root_path,
    wavs_root_path,
    save_dict_name,
    # converted_audios=False,
):
    from transcript_processing import TranscriptsProcessorAphasiaBank

    tp = TranscriptsProcessorAphasiaBank(
        language=language,
        transcripts_datapath=transcripts_root_path,
        wavs_root_path=wavs_root_path,
        create_data_dict=True,
        # converted_audios=True,
    )
    data_dict = tp.data_dict
    save_dict(data_dict, save_dict_name)


def feature_robustness(oracle_dict, asr_dict):
    from feature_names import FEATURES_NAMES
    from scipy import stats

    X_oracle, y_oracle = get_dataset(oracle_dict)
    X_asr, y_asr = get_dataset(asr_dict)

    for j in range(X_oracle.shape[1]):
        diff = X_oracle[:, j] - X_asr[:, j]
        # print(stats.shapiro(X_oracle[:, j]))
        # print(stats.shapiro(X_asr[:, j]))
        # exit()
        # t_stats, p_value = stats.ttest_rel(
        #     X_oracle[:, j], X_asr[:, j], alternative="two-sided"
        # )
        try:
            # X_oracle[:, j], X_asr[:, j]
            t_stats, p_value = stats.wilcoxon(diff)
            # \t Mean {np.mean(tmp)} \t Std {np.std(tmp)
            print(
                f"Feature: {FEATURES_NAMES[j].ljust(30)} \t\t  P-value: {str(p_value).ljust(20)}"
            )
        except:
            print(f"Issue with Feature {FEATURES_NAMES[j]}")


def merge_greek_dicts(dict_1, dict_2, save_dict_name):
    dict_1.update(dict_2)
    print(len(dict_1))
    save_dict(dict_1, save_dict_name)


def clean_asr(data_dict, save_dict_name):
    for speaker in data_dict.keys():
        for story in data_dict[speaker]["stories"]:
            for utt_name in data_dict[speaker]["stories"][story]["utterances"]:
                utt_dict = data_dict[speaker]["stories"][story]["utterances"][utt_name]
                if "asr_prediction" not in utt_dict.keys():
                    continue
                asr_prediction = utt_dict["asr_prediction"]
                asr_prediction = asr_prediction.replace("<s>", "")
                asr_prediction = asr_prediction.replace("</s>", "")
                asr_prediction = re.sub(r"</?\[\d+>", "", asr_prediction)
                asr_prediction = asr_prediction.replace("<unk>", "")
                asr_prediction = asr_prediction.replace("<pad>", "")
                data_dict[speaker]["stories"][story]["utterances"][utt_name][
                    "asr_prediction"
                ] = asr_prediction
    save_dict(data_dict, save_dict_name)


def get_huggingface_dataset():
    from datasets import load_dataset

    dataset = load_dataset("common_voice", "fr", split="train+validation")

    with open("commonvoice_text_french.txt", "w") as f:
        f.write(" ".join(dataset["sentence"]))


def clean_json_for_segmentation_experiment(data_dict, save_dict_name):
    new_dict = {}
    for speaker_name in data_dict.keys():
        tmp_json = data_dict[speaker_name]["user_info"]
        new_dict[speaker_name] = {"user_info": tmp_json}
    save_dict(new_dict, save_dict_name)


def create_segments_english(data_dict, save_dict_name):
    new_dict = data_dict
    from pyannote.audio.pipelines import VoiceActivityDetection

    pipeline = VoiceActivityDetection(segmentation="pyannote/segmentation")
    HYPER_PARAMETERS = {
        # onset/offset activation thresholds
        "onset": 0.5,
        "offset": 0.5,
        # remove speech regions shorter than that many seconds.
        "min_duration_on": 0.0,
        # fill non-speech regions shorter than that many seconds.
        "min_duration_off": 0.0,
    }
    pipeline.instantiate(HYPER_PARAMETERS)

    for speaker_name in new_dict:
        audio_file = data_dict[speaker_name]["user_info"]["converted_audio_path"]
        vad = pipeline(audio_file)
        new_dict[speaker_name]["story"] = {"utterances": {}}
        for idx, (turn, speaker_id, type_sound) in enumerate(
            vad.itertracks(yield_label=True)
        ):
            new_dict[speaker_name]["story"]["utterances"][f"{speaker_name}-{idx}"] = {
                "timestamp": f"{int(turn.start * 1000)}_{int(turn.end * 1000)}",
                "duration": int(turn.end * 1000) - int(turn.start * 1000),
            }
    save_dict(new_dict, save_dict_name)


def add_aq_scores_to_existing_json_files(
    data_dict,
    data_dict_with_aq_scores,
    save_dict_name,
    aq_scores_dict=None,
):
    if aq_scores_dict:
        for speaker in data_dict.keys():
            if speaker in aq_scores_dict.keys():
                data_dict[speaker]["user_info"]["aq_score"] = aq_scores_dict[speaker]
        save_dict(data_dict, save_dict_name)
    else:
        for speaker in data_dict.keys():
            data_dict[speaker]["user_info"]["aq_score"] = data_dict_with_aq_scores[
                speaker
            ]["user_info"]["aq_score"]
        save_dict(data_dict, save_dict_name)


greek_AQ_scores_dict = {
    "300": 19.8,
    "302": 85.5,
    "011": 91.8,
    "010": 96.1,
    "015": 47.6,
    "019": 74.8,
    "018": 42.8,
    "004": 78.3,
    "024": 76.2,
    "023": 55.4,
    "505": 60.9,
    "021": 96.8,
    "306": 60,
    "307": 57.1,
    "030": 78.2,
    "031": 79.4,
}


def zeroshot_experiment_aq_scores(train_dict, test_dict, model="xgboost"):
    X_train, y_train = get_dataset(train_dict, label_aphasia=False)
    X_test, y_test = get_dataset(test_dict, label_aphasia=False)

    nan_ids_from_X_train = np.any(np.isnan(X_train), axis=0)

    X_train = X_train[:, ~nan_ids_from_X_train]
    nan_ids_from_X_test = np.any(np.isnan(X_test), axis=0)
    X_test = X_test[:, ~nan_ids_from_X_test]
    print(X_train.shape, X_test.shape)

    english_scaler = StandardScaler()
    X_train = english_scaler.fit_transform(X_train)

    if model == "xgboost":
        xgb_classifier = XGBClassifier()
        xgb_classifier.fit(X_train, y_train)

        X_test = english_scaler.transform(X_test)
        print(xgb_classifier.get_booster().get_score(importance_type="weight"))
        y_preds = xgb_classifier.predict(X_test)
        print(y_test)
        print(y_preds)
        acc = xgb_classifier.score(X_test, y_test)
        print(acc)


def create_segments_greek(datapath, save_dict_name):
    from pyannote.audio.pipelines import VoiceActivityDetection

    pipeline = VoiceActivityDetection(segmentation="pyannote/segmentation")
    HYPER_PARAMETERS = {
        # onset/offset activation thresholds
        "onset": 0.5,
        "offset": 0.5,
        # remove speech regions shorter than that many seconds.
        "min_duration_on": 0.0,
        # fill non-speech regions shorter than that many seconds.
        "min_duration_off": 0.0,
    }
    pipeline.instantiate(HYPER_PARAMETERS)
    new_dict = {}
    for speaker in os.listdir(datapath):
        new_dict[speaker] = {"user_info": {"group": "aphasia", "aq_score": None}}
        for audio_fn in os.listdir(os.path.join(datapath, speaker)):
            full_audio_fn = os.path.join(datapath, speaker, audio_fn)
            story_name = audio_fn.split("_")[1]
            new_dict[speaker][story_name] = {
                "story_info": {"audio_path": full_audio_fn},
                "utterances": {},
            }

            vad = pipeline(full_audio_fn)
            for idx, (turn, speaker_id, type_sound) in enumerate(
                vad.itertracks(yield_label=True)
            ):
                new_dict[speaker][story_name]["utterances"][f"{speaker}-{idx}"] = {
                    "timestamp": f"{int(turn.start * 1000)}_{int(turn.end * 1000)}",
                    "duration": int(turn.end * 1000) - int(turn.start * 1000),
                }
    save_dict(new_dict, save_dict_name)


def get_greek_asr_transcriptions(
    data_dict,
    save_dict_name,
    logits_path,
    model_id,
    add_language_model=False,
    kenlm_model_path=None,
):
    import torch
    import torchaudio
    from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor
    import re

    processor = Wav2Vec2Processor.from_pretrained(model_id)
    model = Wav2Vec2ForCTC.from_pretrained(model_id).to("cuda")
    BATCH_SIZE = 4
    issue_speakers = []

    if add_language_model:
        from transformers import Wav2Vec2ProcessorWithLM

        vocab_dict = processor.tokenizer.get_vocab()
        kenlm_decoder = get_kenlm_decoder(
            vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path
        )
        processor_with_lm = Wav2Vec2ProcessorWithLM(
            feature_extractor=processor.feature_extractor,
            tokenizer=processor.tokenizer,
            decoder=kenlm_decoder,
        )
    print("Speaker processing started")
    timelist = []
    num_utt_list = []
    for idx, speaker in enumerate(data_dict.keys()):
        num_speaker_utterances = 0
        print(f"Speaker {speaker} under processing")
        timeit_start = datetime.now()
        for story in data_dict[speaker]["stories"]:
            
            audio_path = data_dict[speaker]["stories"][story]["story_info"]["audio_path"]
            if not audio_path:
                # if missing file is missing
                continue
            speech_arrays = []
            utterance_names = []

            for utt_name in data_dict[speaker]["stories"][story]["utterances"]:
                utt_dict = data_dict[speaker]["stories"][story]["utterances"][utt_name]
                # if "asr_prediction" in utt_dict.keys():
                #     continue
                t_start, t_end = timestamp_to_ints(utt_dict["timestamp"])
                offset = t_start / 1000
                duration = (t_end - t_start) / 1000
                # if duration > 100:
                #     print(utt_name)
                #     continue
                speech_array, sampling_rate = librosa.load(
                    audio_path, sr=16_000, offset=offset, duration=duration
                )
                if speech_array.shape[0] == 0:
                    continue
                utterance_names.append(utt_name)
                speech_arrays.append(speech_array)
            if utterance_names == []:
                continue
            batches = [
                speech_arrays[i : i + BATCH_SIZE]
                for i in range(0, len(speech_arrays), BATCH_SIZE)
            ]
            batches_names = [
                utterance_names[i : i + BATCH_SIZE]
                for i in range(0, len(utterance_names), BATCH_SIZE)
            ]
            total_predicted_sentences = []
            for batch, batch_names in zip(batches, batches_names):
                try:
                    inputs = processor(
                        batch,
                        sampling_rate=16_000,
                        return_tensors="pt",
                        padding=True,
                    ).to("cuda")
                    with torch.no_grad():
                        logits = model(
                            inputs.input_values, attention_mask=inputs.attention_mask
                        ).logits
                except:
                    inputs = processor(
                        batch,
                        sampling_rate=16_000,
                        return_tensors="pt",
                        padding=True,
                    )
                    model.to("cpu")
                    with torch.no_grad():
                        logits = model(
                            inputs.input_values,
                            attention_mask=inputs.attention_mask,
                        ).logits
                    # model.to("cuda")
                    # exit("Exception raised")
                # for j, b_name in enumerate(batch_names):
                #     tmp = logits[j].cpu().numpy()
                #     np.save(f"{logits_path}/{b_name}.npy", tmp)
                if add_language_model:
                    # Decoding using language models (beam search etc)
                    predicted_sentences = processor_with_lm.batch_decode(
                        logits.cpu().numpy()
                    ).text
                else:
                    # Greedy Decoding
                    predicted_ids = torch.argmax(logits, dim=-1)
                    predicted_sentences = processor.batch_decode(predicted_ids)
                total_predicted_sentences += predicted_sentences
            for utterance_name, prediction in zip(
                utterance_names, total_predicted_sentences
            ):
                num_speaker_utterances += 1
                data_dict[speaker]["stories"][story]["utterances"][utterance_name][
                    "asr_prediction"
                ] = prediction.lower()
        timeit_end = datetime.now()
        print(
            f"Speaker {idx}/{len(data_dict)} \t Took time: {timeit_end- timeit_start}"
        )
        timelist.append(timeit_end - timeit_start)
        num_utt_list.append(num_speaker_utterances)
    print(timelist)
    print(num_utt_list)
    # timelist = np.array(timelist)
    # num_utt_list = np.array(num_utt_list)
    # print(np.mean(timelist), np.std(timelist))
    # print(np.mean(num_utt_list), np.std(num_utt_list))
    # print(np.mean(timelist / num_utt_list), np.std(timelist / num_utt_list))
    save_dict(data_dict, save_dict_name)


"""
    1. Donwload data and create segments dictionary (greek_data_v3.json)
    2. Pass it through greek asr model
    3. Calculate conllu asr transcripts
    4. Calculate features 
    5. Run AQ experiments 
"""


def merge_chat_files(folder="/data/projects/planv/jerry/elanchat", durations=[0, 0]):
    chat_filenames = [
        os.path.join(folder, fn) for fn in os.listdir(folder) if fn.endswith(".cha")
    ]
    wav_dict = {
        "cat": "/data/projects/planv/jerry/dataset_planv/010/010_cat_1619173048977.wav",
        "stroke": "/data/projects/planv/jerry/dataset_planv/010/010_stroke_1619173048977.wav",
    }
    total_duration = 0
    with open("output.cha", "w") as f_out:
        for idx, chat_filename in enumerate(chat_filenames):
            story_name = chat_filename.split("/")[-1].split(".")[0].split("_")[-1]
            passed_intro_info = False
            previous_line_first_character = ""
            with open(chat_filename, "r") as f_in:
                lines = f_in.readlines()
            for line in lines:
                if line.startswith("@"):
                    if idx == 0 and not passed_intro_info:
                        f_out.write(line)
                    if idx == len(chat_filenames) - 1 and passed_intro_info:
                        f_out.write(line)
                else:
                    if previous_line_first_character == "@":
                        passed_intro_info = True
                        f_out.write(f"@G: {story_name}\n")
                    if idx > 0:
                        time_segment = re.findall("\(.*?)\", line)[0]
                        # re.sub('<.*?>', '', string)
                        t = time_segment.split('"')
                        timestamps = t[2].split("_")[1::]
                        new_segment = f'{t[0]}"{t[1]}"_{int(timestamps[0])+total_duration}_{int(timestamps[1])+total_duration}'
                        line = str.replace(line, time_segment, new_segment)
                    f_out.write(line)
                previous_line_first_character = line[0]
            duration = librosa.get_duration(filename=wav_dict[story_name])
            total_duration += int(duration * 1000)


def create_thales_data_dict(
    data_folder,
    save_dict_name,
    new_format=True,
    tier='TIER_ID="Processed Transcription"',
):
    groups = os.listdir(data_folder)
    data_dict = {}
    eaf_filepaths = []
    t_start, t_end, durations = [], [], []
    for group in groups:
        group_path = os.path.join(data_folder, group)
        group_speakers = os.listdir(group_path)
        for speaker in group_speakers:
            data_dict[speaker] = {"user_info": {"group": group}}
            speaker_path = os.path.join(group_path, speaker)
            speaker_stories = os.listdir(speaker_path)
            for speaker_story in speaker_stories:
                story_path = os.path.join(speaker_path, speaker_story)
                eaf_file = glob.glob(f"{story_path}/*.eaf", recursive=True)[0]
                wav_file = [
                    f for f in glob.glob(f"{story_path}/*") if not f.endswith("eaf")
                ]
                if wav_file:
                    wav_file = wav_file[0]
                else:
                    wav_file = None
                if new_format:
                    data_dict[speaker][speaker_story] = {
                        "story_info": {
                            "transcription_path": eaf_file,
                            "audio_path": wav_file,
                        },
                        "utterances": {},
                    }
                else:
                    data_dict[speaker][speaker_story] = {"raw_transcriptions": {}}

                (
                    transcriptions_list,
                    t_start,
                    t_end,
                    durations,
                ) = fetch_transcriptions_from_elan_file(eaf_file, tier)
                transcriptions_list = reformat_sentence(transcriptions_list)
                eaf_filepaths.append(eaf_file)
                for idx, transcription in enumerate(transcriptions_list):
                    if new_format:
                        data_dict[speaker][speaker_story]["utterances"][
                            f"{speaker}-{speaker_story}-{idx}"
                        ] = {
                            "processed_transcript": transcription,
                            "timestamp": f"{t_start[idx]}_{t_end[idx]}",
                            "duration": durations[idx],
                            "conllu": "",
                        }
                    else:
                        data_dict[speaker][speaker_story]["raw_transcriptions"][
                            transcription
                        ] = {
                            "timestamp": f"{t_start[idx]}_{t_end[idx]}",
                            "duration": durations[idx],
                            "conllu": "",
                        }
    save_dict(data_dict, save_dict_name)
    return data_dict, eaf_filepaths, list(data_dict.keys())


def fix_greek_helper_lowercase():
    data_dict = load_dict("www_greek_data_asr.json")
    for speaker in data_dict.keys():
        for story in data_dict[speaker]["stories"]:
            for utt_id in data_dict[speaker]["stories"][story]["utterances"].keys():
                if (
                    "asr_prediction"
                    not in data_dict[speaker]["stories"][story]["utterances"][utt_id].keys()
                ):
                    continue
                data_dict[speaker]["stories"][story]["utterances"][utt_id][
                    "asr_prediction"
                ] = data_dict[speaker]["stories"][story]["utterances"][utt_id][
                    "asr_prediction"
                ].lower()
        save_dict(data_dict, "www_greek_data_asr.json")


if __name__ == "__main__":
    # zeroshot_experiment_aq_scores(
    #     train_dict=load_dict("english_data_v2_conllu_trf_features.json"),
    #     test_dict=load_dict(
    #         "greek_data_v3_jonatas_no_lm_greedy_conllu_ilsp_features.json"
    #     ),
    #     model="xgboost",
    # )
    # loso_english_experiment(
    #     data_dict=load_dict("english_data_v2_conllu_trf_features.json"),
    #     label_aphasia=False,
    # )
    # merge_chat_files()
    # get_greek_asr_transcriptions(
    #     data_dict=load_dict("greek_data_v3.json"),
    #     save_dict_name="greek_data_v3_time_test.json",
    #     logits_path=None,
    #     model_id="jonatasgrosman/wav2vec2-large-xlsr-53-greek",
    #     add_language_model=False,
    #     kenlm_model_path=None,
    # )

    # Call me mano

    # create_planv_data(
    #     root="/home/jerrychatz/planv-frontiers-data",
    #     save_dict_name="www_planv_data.json",
    # )

    # get_planv_oracle_transcriptions(
    #     data_dict=load_dict("www_planv_data.json"),
    #     save_dict_name="www_planv_data.json",
    #     tier='TIER_ID="*PAR"',
    # )

    # create_segments_greek(
    #     datapath="/home/jerrychatz/projects/aphasia-severity-detection/language-agnostic/dataset_planv",
    #     save_dict_name="www_planv_aphasic_data.json",
    # )

    # get_greek_asr_transcriptions(
    #     data_dict=load_dict("www_planv_aphasic_data.json"),
    #     save_dict_name="www_planv_aphasic_data.json",
    #     logits_path=None,
    #     model_id="jonatasgrosman/wav2vec2-large-xlsr-53-greek",
    #     add_language_model=False,
    #     kenlm_model_path=None,
    # )

    # create_thales_data_dict(
    #     data_folder="/home/jerrychatz/upload/thales-data-with-audio",
    #     save_dict_name="www_thales_data.json",
    #     new_format=True,
    #     tier='TIER_ID="Processed Transcription"',
    # )

    # merge_greek_dicts(
    #     dict_1=load_dict("www_thales_data.json"),
    #     dict_2=load_dict("www_planv_data.json"),
    #     save_dict_name="www_greek_data.json",
    # )
    # merge_greek_dicts(
    #     dict_1=load_dict("www_greek_data.json"),
    #     dict_2=load_dict("www_planv_aphasic_data.json"),
    #     save_dict_name="www_greek_data_extended.json",
    # )

    # calculate_conllus_ilsp(
    #     data_dict=load_dict("www_greek_data.json"),
    #     key_target="processed_transcript",
    #     conllu_name="conllu",
    #     save_dict_name="www_greek_data_conllu.json",
    # )

    # create_features_dict(
    #     data_dict=load_dict("www_greek_data_conllu.json"),
    #     use_asr_transcripts=False,
    #     save_dict_name="www_greek_data_conllu_features.json",
    #     language="greek",
    # )

    # Agglika

    # create_initial_data_dict_aphasiabank(
    #     language="english",
    #     transcripts_root_path="/home/jerrychatz/projects/aphasia-severity-detection/crosslingual-data",
    #     wavs_root_path="/data/projects/planv/aphasiabank_wav/",
    #     save_dict_name="json_files/english_data.json",
    #     # converted_audios=True,
    # )




    # test_dict=load_dict("json_files/ACWT01a_data_ducle.json")
    # get_aphasiabank_asr_transcriptions(
    #     data_dict=test_dict,
    #     save_dict_name="json_files/ACWT01a_data_ducle_asr.json",
    #     add_language_model=False,
    #     kenlm_model_path=None,
    # )


    asr_evaluation(
        data_dict=load_dict("json_files/ACWT01a_data_ducle_asr.json"),
        target_key="duc_le_transcript",
        prediction_key="asr_prediction",
        per_category=False,
    )

    # calculate_conllu_english(
    #     data_dict=load_dict("json_files/ACWT01a_data.json"),
    #     save_dict_name="json_files/ACWT01a_data_conllu.json",
    #     key_target="target_transcript",
    #     conllu_name="conllu",
    # )

    # create_features_dict(
    #     data_dict=load_dict("json_files/ACWT01a_data_conllu.json"),
    #     use_asr_transcripts=False,
    #     save_dict_name="json_files/ACWT01a_data_features.json",
    #     language="english",
    # )

    # loso_english_experiment(
    #     data_dict=load_dict("www_english_data_conllu_features.json"), label_aphasia=True
    # )

    # zeroshot_experiment(train_dict=load_dict("www_english_data_conllu_features.json"), test_dict=load_dict("www_greek_data_conllu_features.json"), model="xgboost")

    # --- ASR gia ellinika ---
    # get_greek_asr_transcriptions(
    #     data_dict=load_dict("www_greek_data.json"),
    #     save_dict_name="www_greek_data_asr.json",
    #     logits_path=None,
    #     model_id="jonatasgrosman/wav2vec2-large-xlsr-53-greek",
    #     add_language_model=False,
    #     kenlm_model_path=None,
    # )

    # asr_evaluation(
    #     data_dict=load_dict("greek_data_jonatas_no_lm_greedy.json"),
    #     target_key="processed_transcript",
    #     prediction_key="asr_prediction",
    #     per_category=True,
    # )

    # calculate_conllus_ilsp(
    #     data_dict=load_dict("www_greek_data_asr.json"),
    #     key_target="asr_prediction",
    #     conllu_name="conllu-asr",
    #     save_dict_name="www_greek_data_asr_conllu.json",
    # )

    # create_features_dict(
    #     data_dict=load_dict("www_greek_data_asr_conllu.json"),
    #     use_asr_transcripts=True,
    #     save_dict_name="www_greek_data_asr_conllu_features.json",
    #     language="greek",
    # )

    # zeroshot_experiment(
    #     train_dict=load_dict("www_english_data_conllu_features.json"),
    #     test_dict=load_dict("www_greek_data_asr_conllu_features.json"),
    #     model="xgboost",
    # )

    # add_aq_scores_to_existing_json_files(
    #     data_dict=load_dict("www_planv_aphasic_data.json"),
    #     data_dict_with_aq_scores=None,
    #     save_dict_name="www_planv_aphasic_data_aq_scores.json",
    #     aq_scores_dict=greek_AQ_scores_dict,
    # )

    # calculate_conllus_ilsp(
    #     data_dict=load_dict("www_planv_aphasic_data_aq_scores.json"),
    #     key_target="asr_prediction",
    #     conllu_name="conllu-asr",
    #     save_dict_name="www_planv_aphasic_data_aq_scores_conllu.json",
    # )

    # create_features_dict(
    #     data_dict=load_dict("www_planv_aphasic_data_aq_scores_conllu.json"),
    #     use_asr_transcripts=True,
    #     save_dict_name="www_planv_aphasic_data_aq_scores_conllu_features.json",
    #     language="greek",
    # )

    # zeroshot_experiment_aq_scores(
    #     train_dict=load_dict("www_english_data_conllu_features.json"),
    #     test_dict=load_dict("www_planv_aphasic_data_aq_scores_conllu_features.json"),
    #     model="xgboost",
    # )

    # loso_english_experiment(
    #     data_dict=load_dict("www_english_data_conllu_features.json"),
    #     label_aphasia=False,
    # )
