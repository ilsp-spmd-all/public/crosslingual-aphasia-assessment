import os
import re
import glob
import string
from utils import (
    # transcript_cleaning_decorator,
    timestamp_to_duration_conversion,
    save_dict,
    clean_transcript_abenglish,
)
from arguments import parse_arguments


class SingleLineProcessor(object):
    def __init__(self):
        self.punctuation = string.punctuation
        self.time_indicator_char = ""

    def remove_punctuation(self, line):
        return line.translate(str.maketrans("", "", self.punctuation))

    def fetch_timestamp(self, line):
        pattern = f"{self.time_indicator_char}(.*?){self.time_indicator_char}"
        time = re.findall(pattern, line)
        return time[0] if time else None

    def fetch_patient_metadata(self, line):
        line_split = line.split("|")
        aq_score = float(line_split[-2]) if line_split[-2] else None
        gender = line_split[4]
        aphasia_type = line_split[5]
        return aq_score, gender, aphasia_type

    def fetch_story(self, line):
        return line.replace("@G:", "").strip()


class DocumentProcessor(object):
    def __init__(self):
        self.slp = SingleLineProcessor()

    def read_cha_file(self, cha_fp):
        with open(cha_fp, "r", encoding="utf8") as fp:
            lines = fp.readlines()
        return lines

    # @transcript_cleaning_decorator
    def process_cha_file(self, cha_fp, language="english", cleaning=None):
        # Read CHA file
        cha_lines = self.read_cha_file(cha_fp)

        # Initializations
        transcripts, stories, timestamps = [], [], []
        aq_score, aphasia_type, gender, story = None, None, None, None
        participant_speaking, previous_utterance = False, ""

        par_count = 0  # count of when new participant utterance begins, useful in numbering utterances
        ducle_counts = []  # duc le numbering
        inv_spoke_last = 0  # for duc le numbering, to make sure we dont count two inv utterances in a row
        for idx, line in enumerate(cha_lines):
            if "@G:" in line:
                story = self.slp.fetch_story(line)
            if "@ID" in line and "PAR" in line:
                aq_score, gender, aphasia_type = self.slp.fetch_patient_metadata(line)
            if "*INV" in line:
                if par_count > 0 and inv_spoke_last == 0:
                    par_count += 1
                    inv_spoke_last = 1
            if "*PAR" in line or participant_speaking:
                # if participant starts or continues speaking
                timestamp = self.slp.fetch_timestamp(line)

                if timestamp:
                    # if timestamp exists, it signifies that
                    # participant ended speaking
                    par_count += 1
                    if previous_utterance == "":
                        ducle_counts.append(par_count)
                    else:
                        ducle_counts.append(par_count)
                    previous_utterance += line
                    timestamps.append(timestamp)
                    # remove participant tag, newline and \t tag before storing raw transcript
                    previous_utterance = (
                        previous_utterance.replace("*PAR:", "")
                        .replace("\n", "")
                        .replace("\t", "")
                    )
                    transcripts.append(previous_utterance)
                    stories.append(story)
                    participant_speaking = False
                    previous_utterance = ""
                    inv_spoke_last = 0
                else:
                    # if timestamp does not exist
                    # participant continues speaking
                    previous_utterance += line
                    # par_count+=1
                    participant_speaking = True
        return (
            transcripts,
            stories,
            timestamps,
            aq_score,
            aphasia_type,
            gender,
            ducle_counts,
        )


class TranscriptsProcessor(object):
    """
    AphasiaBank Transcript Processor Class

    Processes all transcripts (in .cha format, in the AphasiaBank style)
    Currently supports only english AB
    """

    def __init__(
        self,
        language,
        transcripts_datapath,
        group="all",
        create_data_dict=False,
        new_format=False,
    ):
        self.language = language
        self.transcripts_root_path = transcripts_datapath
        self.doc_processor = DocumentProcessor()
        self.group_list = self.find_group(group)
        self.transcripts_paths, self.groups = self.get_transcripts_paths(new_format)
        (
            self.transcripts,
            self.stories,
            self.timestamps,
            self.aq_score,
            self.aphasia_type,
            self.gender,
            self.data_dict,
        ) = self.process_transcripts(create_data_dict)

    def get_transcripts_paths(self, new_format):
        transcript_paths, group_transcripts_paths, groups = [], [], []
        for group in self.group_list:
            if not new_format:
                tmp_language = self.language.capitalize()
                tmp_group = group.capitalize()
            else:
                tmp_language = self.language
                tmp_group = group
            group_transcripts_paths = glob.glob(
                os.path.join(
                    self.transcripts_root_path,
                    tmp_language,
                    tmp_group,
                    "**",
                    "*.cha",
                )
            )
            groups += [group] * len(group_transcripts_paths)
            transcript_paths += group_transcripts_paths
        return transcript_paths, groups

    def find_group(self, group_argument: str):
        group_list = []
        if group_argument == "all":
            group_list = ["aphasia", "control"]
        else:
            group_list.append(group_argument)
        return group_list

    def process_transcripts(self, create_data_dict=False):
        data_dict = {}
        speakers, transcripts, stories, timestamps, aq_scores, aphasia_type, gender = (
            [],
            [],
            [],
            [],
            [],
            [],
            [],
        )
        for transcript_path in self.transcripts_paths:
            (
                cha_transcripts,
                cha_stories,
                cha_timestamps,
                cha_aq,
                cha_aphasia_type,
                cha_gender,
            ) = self.doc_processor.process_cha_file(
                transcript_path, language=self.language, cleaning=None
            )
            # Alternative: cleaning="pylangacq"
            speaker = transcript_path.split("/")[-1].split(".")[0]
            group_indicated_from_path = transcript_path.split("/")[-3]
            # speakers in aphasia/control may have the same name
            # issue arises as one speaker from control group may delete data from aphasia group
            speaker = f"{speaker}-{group_indicated_from_path}"
            speakers.append(speaker)
            transcripts += cha_transcripts
            stories += cha_stories
            timestamps += cha_timestamps
            aq_scores.append(cha_aq)
            aphasia_type.append(cha_aphasia_type)
            gender.append(cha_gender)

            # Dictionary
            # Each file is a story
            if create_data_dict:
                group_indicated_from_path = transcript_path.split("/")[-3]
                group = (
                    "aphasia"
                    if group_indicated_from_path in ["Aphasia", "aphasia"]
                    else "control"
                )
                print(transcript_path, group)
                data_dict[speaker] = {"user_info": {"group": group}}
                for idx, _ in enumerate(cha_transcripts):
                    if cha_transcripts[idx] == "":
                        continue
                    cha_transcripts[idx] = " ".join(cha_transcripts[idx].split())
                    # Capitalize First letter
                    cha_transcripts[idx] = cha_transcripts[idx].capitalize()
                    # Remove punctutation
                    cha_transcripts[idx] = cha_transcripts[idx].translate(
                        str.maketrans("", "", string.punctuation)
                    )
                    # Add . at the end of the transcript
                    cha_transcripts[idx] += "."
                    if cha_stories[idx] in data_dict[speaker]["stories"]:
                        data_dict[speaker]["stories"][cha_stories[idx]][
                            "raw_transcriptions"
                        ][cha_transcripts[idx]] = {
                            "timestamp": cha_timestamps[idx],
                            "duration": timestamp_to_duration_conversion(
                                cha_timestamps[idx]
                            ),
                            "conllu": "",
                        }
                    else:
                        data_dict[speaker]["stories"][cha_stories[idx]] = {
                            "raw_transcriptions": {
                                cha_transcripts[idx]: {
                                    "timestamp": cha_timestamps[idx],
                                    "duration": timestamp_to_duration_conversion(
                                        cha_timestamps[idx]
                                    ),
                                    "conllu": "",
                                }
                            }
                        }
        return (
            transcripts,
            stories,
            timestamps,
            aq_scores,
            aphasia_type,
            gender,
            data_dict,
        )

    def story_level_transcription(self):
        pass


class TranscriptsProcessorAphasiaBank(TranscriptsProcessor):
    def __init__(
        self,
        language,
        transcripts_datapath,
        wavs_root_path,
        create_data_dict,
    ):
        self.wavs_root_path = wavs_root_path
        super().__init__(
            language=language,
            transcripts_datapath=transcripts_datapath,
            create_data_dict=create_data_dict,
        )
        print(self.wavs_root_path)

    def process_transcripts(self, create_data_dict=False):
        data_dict = {}
        speakers, transcripts, stories, timestamps, aq_scores, aphasia_type, gender = (
            [],
            [],
            [],
            [],
            [],
            [],
            [],
        )
        transcriptions_with_no_audios = []
        for transcript_path in self.transcripts_paths:
            (
                cha_transcripts,
                cha_stories,
                cha_timestamps,
                cha_aq,
                cha_aphasia_type,
                cha_gender,
                ducle_counts,
            ) = self.doc_processor.process_cha_file(
                transcript_path, language=self.language, cleaning=None
            )
            # Alternative: cleaning="pylangacq"
            speaker_name = transcript_path.split("/")[-1].split(".")[0]
            group_indicated_from_path = transcript_path.split("/")[-3]
            # speakers in aphasia/control may have the same name
            # issue arises as one speaker from control group may delete data from aphasia group
            speaker = f"{speaker_name}-{group_indicated_from_path}"
            speakers.append(speaker)
            transcripts += cha_transcripts
            stories += cha_stories
            timestamps += cha_timestamps
            aq_scores.append(cha_aq)
            aphasia_type.append(cha_aphasia_type)
            gender.append(cha_gender)

            # Dictionary
            # Each file is a story
            if create_data_dict:
                group = group_indicated_from_path.lower()
                # group = (
                #     "aphasia"
                #     if group_indicated_from_path in ["Aphasia", "aphasia"]
                #     else "control"
                # )
                tmp = transcript_path.split(".")[0].split("/")
                fp = f"{self.wavs_root_path}/{('/').join(tmp[-3::])}"
                for extension in ["wav", "mp4", "mp3"]:
                    wav_file = f"{fp}.{extension}"
                    if os.path.isfile(wav_file):
                        break
                if not os.path.isfile(wav_file):
                    # print("Cannot find audio file", transcript_path)
                    transcriptions_with_no_audios.append(transcript_path)
                    continue
                data_dict[speaker] = {
                    "user_info": {
                        "group": group,
                        "transcript_path": transcript_path,
                        "audio_path": wav_file,
                        "aq_score": cha_aq,
                    },
                    "stories": {},
                }
                for idx, _ in enumerate(cha_transcripts):
                    if cha_transcripts[idx] == "":
                        continue
                    raw_transcript = cha_transcripts[idx]
                    cha_transcripts[idx] = clean_transcript_abenglish(
                        cha_transcripts[idx]
                    )
                    cha_transcripts[idx] = " ".join(cha_transcripts[idx].split())
                    # Capitalize First letter
                    cha_transcripts[idx] = cha_transcripts[idx].capitalize()
                    # Remove punctutation
                    # cha_transcripts[idx] = cha_transcripts[idx].translate(
                    #     str.maketrans("", "", string.punctuation)
                    # )
                    # Remove extra spaces
                    cha_transcripts[idx] = " ".join(cha_transcripts[idx].split())
                    # Add . at the end of the transcript
                    try:
                        if cha_transcripts[idx][-1] not in [".", ",", "!", "?"]:
                            cha_transcripts[idx] += "."
                    except:
                        continue
                    if cha_stories[idx] in data_dict[speaker]["stories"]:
                        speaker_utterance_idx = (
                            len(
                                data_dict[speaker]["stories"][cha_stories[idx]][
                                    "utterances"
                                ]
                            )
                            + 1
                        )
                        data_dict[speaker]["stories"][cha_stories[idx]]["utterances"][
                            f"{speaker}-{cha_stories[idx]}-{speaker_utterance_idx}"
                        ] = {
                            "raw_transcript": raw_transcript,
                            "target_transcript": cha_transcripts[idx],
                            "timestamp": cha_timestamps[idx],
                            "duration": timestamp_to_duration_conversion(
                                cha_timestamps[idx]
                            ),
                            "duc_le_id": speaker_name + "-" + str(ducle_counts[idx]),
                            "conllu": "",
                        }
                    else:
                        data_dict[speaker]["stories"][cha_stories[idx]] = {
                            "utterances": {
                                f"{speaker}-{cha_stories[idx]}-1": {
                                    "raw_transcript": raw_transcript,
                                    "target_transcript": cha_transcripts[idx],
                                    "timestamp": cha_timestamps[idx],
                                    "duration": timestamp_to_duration_conversion(
                                        cha_timestamps[idx]
                                    ),
                                    "duc_le_id": speaker_name
                                    + "-"
                                    + str(ducle_counts[idx]),
                                    "conllu": "",
                                }
                            }
                        }
        print("Transcriptions with no audios", transcriptions_with_no_audios)
        return (
            transcripts,
            stories,
            timestamps,
            aq_scores,
            aphasia_type,
            gender,
            data_dict,
        )


if __name__ == "__main__":

    args = parse_arguments()
    transcript_processor = TranscriptsProcessor(
        args.language, args.transcripts_datapath, args.group
    )
    if args.save_results_json:
        results = transcript_processor.process_transcripts()
        save_dict(results, args.save_results_json)
