import os
from utils import csv_to_dataset
def create_kenlm_txt_from_csv(csv_path,target_txt_path='to_train_lm.txt',transcription_col_name='clean_transcription_v1'):
    dataset=csv_to_dataset(csv_path,transcription_col_name=transcription_col_name)
    with open(target_txt_path, "w") as f:
        f.write(" ".join(dataset['sentence']))

def fix_kenlm_arpa(model_name):
    with open(f"{model_name}.arpa", "r") as read_file, open(
        f"{model_name}_correct.arpa", "w"
    ) as write_file:
        has_added_eos = False
        for line in read_file:
            if not has_added_eos and "ngram 1=" in line:
                count = line.strip().split("=")[-1]
                write_file.write(line.replace(f"{count}", f"{int(count)+1}"))
            elif not has_added_eos and "<s>" in line:
                write_file.write(line)
                write_file.write(line.replace("<s>", "</s>"))
                has_added_eos = True
            else:
                write_file.write(line)

dataset_dir='datasets/ab_english'
csv_train=dataset_dir+'/train.csv'
csv_val=dataset_dir+'/val.csv'
csv_test=dataset_dir+'/test.csv'

# create_kenlm_txt_from_csv(csv_train)
# os.system("../kenlm/build/bin/lmplz -o 3 <\"to_train_lm.txt\" > \"5gram.arpa\"")
# fix_kenlm_arpa("5gram")