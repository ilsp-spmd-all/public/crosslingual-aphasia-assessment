from transformers import Wav2Vec2CTCTokenizer

from transformers import Wav2Vec2FeatureExtractor
import torch
from data import DataCollatorCTCWithPadding
import pandas as pd
import os
import torchaudio
from datasets import load_dataset, load_metric, Audio, Dataset
import numpy as np
from transformers import Wav2Vec2ForCTC
from transformers import TrainingArguments
from transformers import Trainer
from transformers import logging
from utils import csv_to_dataset,df_to_dataset,get_kenlm_decoder
import wandb

# from pynvml import *
import re

use_lm=True

if torch.cuda.is_available():
    print("CUDA available")
else:
    print("CUDA unavailable")

wandb.init(project="train_asr_ab_english")
os.environ['WANDB_LOG_MODEL']='true'
# os.environ['CUDA_VISIBLE_DEVICES']='0'

dataset_dir='datasets/ab_english'
csv_train=dataset_dir+'/train.csv'
csv_val=dataset_dir+'/val.csv'
csv_test=dataset_dir+'/test.csv'
vocab_dict_path=f"{dataset_dir}/vocab.json"
tokenizer = Wav2Vec2CTCTokenizer(vocab_dict_path, unk_token="[UNK]", pad_token="[PAD]", word_delimiter_token="|")

feature_extractor = Wav2Vec2FeatureExtractor(feature_size=1, sampling_rate=16000, padding_value=0.0, do_normalize=True, return_attention_mask=True)

if use_lm:
    from transformers import Wav2Vec2ProcessorWithLM
    kenlm_model_path=""
    kenlm_decoder = get_kenlm_decoder(
            vocab_dict=vocab_dict, kenlm_model_path=kenlm_model_path
        )
    processor_with_lm = Wav2Vec2ProcessorWithLM(
            feature_extractor=feature_extractor,
            tokenizer=tokenizer,
            decoder=kenlm_decoder,
        )

else:
    from transformers import Wav2Vec2Processor
    processor = Wav2Vec2Processor(feature_extractor=feature_extractor, tokenizer=tokenizer)

print('loading data')

df_train=pd.read_csv(csv_train)
df_val=pd.read_csv(csv_val)

df_train=df_train[df_train['duration_seconds']<10]
df_train=df_train[df_train['duration_seconds']>0.5]
# df_test=df_test[df_test['duration_seconds']<11]
# df_test=df_test[df_test['duration_seconds']>0.5]
df_val=df_val[df_val['duration_seconds']<10]
df_val=df_val[df_val['duration_seconds']>0.5]
wandb.config.max_len=10
wandb.config.min_len=0.5

val_data=df_to_dataset(df_val,percent_to_use=0.5)
train_data=df_to_dataset(df_train,percent_to_use=1.)

wandb.config.percent_train=1.
wandb.config.percent_val=0.5

def prepare_dataset(batch):
    audio = batch["audio"]

    # batched output is "un-batched"
    batch["input_values"] = processor(audio["array"], sampling_rate=audio["sampling_rate"]).input_values[0]
    batch["input_length"] = len(batch["input_values"])
    
    with processor.as_target_processor():
        batch["labels"] = processor(batch["sentence"]).input_ids
    return batch


cache_dir="cache"
train_data = train_data.map(prepare_dataset, remove_columns=train_data.column_names,num_proc=10,load_from_cache_file=True)
# test_data = test_data.map(prepare_dataset, remove_columns=test_data.column_names,num_proc=10,load_from_cache_file=True)
val_data = val_data.map(prepare_dataset, remove_columns=val_data.column_names,num_proc=10,load_from_cache_file=True)
# import random

data_collator = DataCollatorCTCWithPadding(processor=processor, padding=True)

wer_metric = load_metric("wer")

def compute_metrics(pred):
    pred_logits = pred.predictions
    pred_ids = np.argmax(pred_logits, axis=-1)

    pred.label_ids[pred.label_ids == -100] = processor.tokenizer.pad_token_id

    pred_str = processor.batch_decode(pred_ids)
    # we do not want to group tokens when computing the metrics
    label_str = processor.batch_decode(pred.label_ids, group_tokens=False)

    wer = wer_metric.compute(predictions=pred_str, references=label_str)

    return {"wer": wer}

model = Wav2Vec2ForCTC.from_pretrained(
    "facebook/wav2vec2-large-xlsr-53", 
    attention_dropout=0.1,
    hidden_dropout=0.1,
    feat_proj_dropout=0.0,
    mask_time_prob=0.05,
    layerdrop=0.1,
    ctc_loss_reduction="mean", 
    pad_token_id=processor.tokenizer.pad_token_id,
    vocab_size=len(processor.tokenizer)
)
model.freeze_feature_extractor()
model.gradient_checkpointing_enable()

training_args = TrainingArguments(
  # output_dir="/content/gdrive/MyDrive/wav2vec2-large-xlsr-turkish-demo",
  output_dir="./wav2vec2-large-xlsr-english-ab-demo_under10sec",
  group_by_length=True,
  per_device_train_batch_size=16,
  gradient_accumulation_steps=2,
  evaluation_strategy="epoch",
  num_train_epochs=30,
  fp16=True,
  save_steps=100,
  eval_steps=100,
  logging_steps=10,
  learning_rate=3e-4,
  warmup_steps=500,
  save_total_limit=2,
  save_strategy='epoch',
  load_best_model_at_end=True,
  report_to='wandb',
  run_name='train_asr_ab_english'
)

trainer = Trainer(
    model=model,
    data_collator=data_collator,
    args=training_args,
    compute_metrics=compute_metrics,
    train_dataset=train_data,
    eval_dataset=val_data,
    tokenizer=processor.feature_extractor,
)
print(torch.cuda.is_available())

logging.set_verbosity_info()
trainer.train()