# CrossLingual Aphasia Assessment

A repository to host the cross-lingual aphasia severity detection code for Plan-V.

## Description

The scripts here are used to run experiments for assessing aphasia severity on low-resource languages, by training the model on another language where more data is available (hence the cross-lingual). To run the code you must at least have access to the [AphasiaBank](https://aphasia.talkbank.org/) dataset  in one or two languages (can be accessed with proper credentials, available upon request). For more information, see our [paper](https://arxiv.org/abs/2204.00448).

## Installation

To run the code, create an Anaconda enviroment with python 3.8 and install requirements.txt with pip.

## Documentation

Right now everything is in the folder named "documentation", any bugs and omissions will be fixed soon, until then browse the markdown files to see how to run the scripts and in which order.

## Download Data
To inspect AphasiaBank dataset, you can have access to [TalkBank](https://sla.talkbank.org/TBB/aphasia). User credentials are required and can be provided upon request.

### English AphasiaBank
```
# Environmental Variables
ab_username="INSERT_USERNAME" 
ab_password="INSERT_PASSWORD"
aphasiabank_url="https://aphasia.talkbank.org/data/English/"
media_url="https://media.talkbank.org/aphasia/English/"
save_folder="/data/projects/planv"
wav_folder="/data/projects/planv/aphasiabank_wav"

# Download Transcripts
wget -r -np -e robots=off --reject "index.html*" -P ${save_folder} --user ${ab_username} --password ${ab_password} ${aphasiabank_url}

# Download Media
wget -r -np -e robots=off --reject "index.html*" -P ${save_folder} --user ${ab_username} --password ${ab_password} ${media_url}
```
### French AphasiaBank 
```
# Environmental Variables
aphasiabank_url="https://aphasia.talkbank.org/data/French/" 
media_url="https://media.talkbank.org/aphasia/French/" 

# Download Transcripts
wget -r -np -e robots=off --reject "index.html*" -P ${save_folder} --user ${ab_username} --password ${ab_password} ${aphasiabank_url}

# Download Media
wget -r -np -e robots=off --reject "index.html*" -P ${save_folder} --user ${ab_username} --password ${ab_password} ${media_url}
```

### Greek Data - The PLANV dataset

PLan-V Data contains both people with aphasia and control speakers and can be found here [insert CLARIN link]

## Data Preprocessing

### Unzip and Downsample Media files
First, we need to unzip the downloaded folder and then we need to convert all media files to wav files. Some media files are in mp4 format and they need to be converted as they cannot be processed with current audio loading libraries we use. We also recommend downsampling all audio files to 16 kHz since the HuggingFace models used require this sampling rate. Finally, we keep only one channel in case there are two.
```
# To unzip all transcripts

cd ${save_folder}/aphasia.talkbank.org/
find . -name "*.zip" | while read filename; do unzip -o -d "$(dirname "$filename")" "$filename"; rm "$filename"; done

# To convert English aphasiabank videos to 16kHz mono wav:

python3 scripts/convert_mp4_to_wav.py --DATA_MP4_DIR ${save_folder}/media.talkbank.org/aphasia/English/Aphasia --DATA_WAV_DIR ${wav_folder}/Aphasia

python3 scripts/convert_mp4_to_wav.py --DATA_MP4_DIR ${save_folder}/media.talkbank.org/aphasia/English/Control --DATA_WAV_DIR ${wav_folder}/Control
```





## Future
We will be updating the code in this repository with improvements and extensions. Some of them are
- Adding speaker diarization to eliminate the need for manual transcriptions altogether.
- Aphasia severity estimation by tier (Mild, Medium, Severe, Very Severe).

