import argparse


def parse_arguments():
    parser = argparse.ArgumentParser(description="Transcript Processing Arguments")
    parser.add_argument(
        "--transcripts_datapath",
        type=str,
        default="/home/jerrychatz/projects/data",
        help="AphasiaBank Datapath with Transcriptions",
    )
    parser.add_argument(
        "--language",
        type=str,
        default="english",
        choices=["english"],
        help="Language of transcripts",
    )
    parser.add_argument("--group", choices=["all", "aphasia", "control"], default="all")
    parser.add_argument(
        "--save-results-json",
        type=str,
        default="results.json",
        help="Save results in json file",
    )
    # Thales Dataset
    parser.add_argument(
        "--thales-data-folder",
        type=str,
        default="/home/jerrychatz/projects/thales-data",
        help="Thales Datapath with Transcriptions",
    )
    parser.add_argument(
        "--thales-tier",
        type=str,
        default='TIER_ID="Processed Transcription"',
        help="Thales TIER in Transcriptions",
    )
    # Experiment Arguments

    # English Experiment
    parser.add_argument(
        "--experiment-only-english",
        action="store_true",
        help="Conduct experiment only in English",
    )
    parser.add_argument(
        "--experiment-crosslingual-zeroshot",
        action="store_true",
        help="Flag to run zero shot experiment",
    )
    parser.add_argument(
        "--experiment-crosslingual-fewshot",
        action="store_true",
        help="Flag to run few shot experiment",
    )
    parser.add_argument(
        "--experiment-crosslingual-add-planv",
        action="store_true",
        help="Flag to add thales data",
    )
    parser.add_argument(
        "--experiment-crosslingual-add-chinese",
        action="store_true",
        help="Add chinese data to test set",
    )
    parser.add_argument(
        "--experiment-crosslingual-only-chinese",
        action="store_true",
        help="Add only chinese data to test set",
    )
    parser.add_argument(
        "--experiment-story-level",
        action="store_true",
        help="Flag to run story level experiment",
    )
    parser.add_argument(
        "--use-optimal-transport",
        action="store_true",
        help="Flag to use optimal transport",
    )

    # Model
    parser.add_argument(
        "--model-type",
        type=str,
        default="xgboost",
        help="Model that will be used for training and inference",
    )
    parser.add_argument(
        "--stack-utterances-durations",
        action="store_true",
        help="If true, total duration corresponds to the sum of utterances durations.",
    )
    parser.add_argument(
        "--use-asr-transcripts",
        action="store_true",
        help="Use ASR transcriptions",
    )
    return parser.parse_args()
